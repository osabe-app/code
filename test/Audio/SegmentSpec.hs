{-# LANGUAGE MultiWayIf #-}

module Audio.SegmentSpec where

import Test.HUnit
import Test.QuickCheck

import Audio.Segment


instance Arbitrary Segment where
  arbitrary = do
    isClip <- arbitrary
    isInsertedGap <- arbitrary
    let geqZero = arbitrary `suchThat` (>= 0)
    if | isClip        -> Clip <$> geqZero <*> geqZero
       | isInsertedGap -> InsertedGap <$> geqZero
       | otherwise     -> ReferenceGap <$> geqZero <*> geqZero <*> geqZero


isClipA :: Gen Segment
isClipA = arbitrary `suchThat` isClip

isGapA :: Gen Segment
isGapA = arbitrary `suchThat` isGap


testSegmentProperties = do
  trimBeginningChecks
  trimEndChecks
  splitSegmentChecks
  expandGapChecks


-- TODO unit tests for isClip?
-- TODO unit tests for isGap?

-- TODO unit tests for offsetOfFirstSampleIn with Clip and Gap, 0 len and non-zero len
-- TODO unit tests for offsetOfLastSampleIn with Clip and Gap, 0 len and non-zero len


--------------------------------
-- trimBeginning
--------------------------------

trimBeginningChecks = do
  quickCheck prop_trimBeginning_lengthChanged
  quickCheck prop_trimBeginning_adjustedStart


prop_trimBeginning_lengthChanged :: Segment -> Int -> Bool
prop_trimBeginning_lengthChanged segment amount =
  let trimmed = trimBeginning amount segment
  in lengthOf trimmed == lengthOf segment - amount
  && case segment of
      ReferenceGap{} -> (originalLengthOf segment <= amount)
        || (originalLengthOf trimmed == originalLengthOf segment - amount)
      _ -> True

prop_trimBeginning_adjustedStart :: Segment -> Int -> Bool
prop_trimBeginning_adjustedStart segment amount =
  let trimmed = trimBeginning amount segment
  in case segment of
      Clip{}         -> startOf trimmed == startOf segment + amount
      InsertedGap{}  -> True
      ReferenceGap{} -> (originalLengthOf segment <= amount)
        || (startOf trimmed == startOf segment + amount)


--------------------------------
-- trimEnd
--------------------------------

trimEndChecks = do
  quickCheck prop_trimEnd_lengthChanged
  quickCheck prop_trimEnd_invariants


prop_trimEnd_lengthChanged :: Segment -> Int -> Bool
prop_trimEnd_lengthChanged segment amount =
  let trimmed = trimEnd amount segment
  in lengthOf trimmed == lengthOf segment - amount

prop_trimEnd_invariants :: Segment -> Int -> Bool
prop_trimEnd_invariants segment amount =
  let trimmed = trimEnd amount segment
  in case segment of
      Clip{}         -> startOf trimmed == startOf segment
      InsertedGap{}  -> True
      ReferenceGap{} -> startOf trimmed == startOf segment
        && originalLengthOf trimmed == originalLengthOf segment


--------------------------------
-- splitSegment
--------------------------------

-- TODO unit tests:
--  - check for bounds checking

splitSegmentChecks = do
  quickCheck prop_splitSegment_lengthInvariant
  quickCheck prop_splitSegment_newLengthsSmaller 
  quickCheck $ forAll isClipA prop_splitSegment_startsLineUp


prop_splitSegment_lengthInvariant :: Segment -> Int -> Bool
prop_splitSegment_lengthInvariant segment offset =
  case splitSegment offset segment of
    Nothing -> True
    Just (a, b) -> lengthOf segment == lengthOf a + lengthOf b

prop_splitSegment_newLengthsSmaller :: Segment -> Int -> Bool
prop_splitSegment_newLengthsSmaller segment offset =
  case splitSegment offset segment of
    Nothing -> True
    Just (a, b) -> lengthOf a < lengthOf segment
                && lengthOf b < lengthOf segment

prop_splitSegment_startsLineUp :: Segment -> Int -> Bool
prop_splitSegment_startsLineUp segment offset =
  case splitSegment offset segment of
    Nothing -> True
    Just (a, b) -> startOf a == startOf segment
                && startOf a + lengthOf a == startOf b

--------------------------------
-- expandGap
--------------------------------

expandGapChecks = do
  quickCheck $ forAll isGapA prop_expandGap_gapExpanded
  quickCheck prop_expandGap_invariants


prop_expandGap_gapExpanded :: Segment -> Int -> Bool
prop_expandGap_gapExpanded segment amount =
  let expanded = expandGap amount segment
  in lengthOf expanded == max 0 (lengthOf segment + amount)

prop_expandGap_invariants :: Segment -> Int -> Bool
prop_expandGap_invariants segment amount =
  let expanded = expandGap amount segment
  in case segment of
      Clip{}         -> segment == expanded
      InsertedGap{}  -> True
      ReferenceGap{} -> startOf expanded == startOf segment
        && originalLengthOf expanded == originalLengthOf segment
