# osabe

**osabe** is the **O**pen-**S**ource **A**udio-**B**ook **E**ditor, a specialized WAV editor for cutting voice clips out of long-form narration audio such as audiobooks or podcasts.

This is the official code repository for this project. For end-user downloads and documentation, see [osabe.app](https://osabe.app/). For information on the development of this application and on its architecture, see the [project blog](https://osabe-app.bitbucket.io/).

Currently I'm not accepting code contributions, but that may change in future.

## About

**osabe** is written in [Haskell](https://www.haskell.org/) and relies on the [SDL2 game development library](https://www.libsdl.org/download-2.0.php). It was created out of a frustration with the feature set of existing free/cheap wave editors in an attempt to ease the burden of editing audio for this fledgling voice artist; I also wanted to try to do a more-complex project in Haskell.

## Dev Space Configuration

This project uses [stack](https://docs.haskellstack.org/en/stable/README/) to manage the Haskell project, and **git** for version control. You'll also need to do some OS-specific configuration mostly involving getting the SDL2 library and Haskell bindings installed for things to work.

Instructions are provided for different OSs in the following sub-sections.

### Configuration on Windows

I found this Reddit post on [installing SDL2 using stack](https://www.reddit.com/r/haskellgamedev/comments/4jpthu/windows_sdl2_is_now_almost_painless_via_stack/) very helpful.

You'll need to run (on x64 machines):

    stack exec -- pacman -Syu
    stack exec -- pacman -S mingw-w64-x86_64-pkg-config mingw-w64-x86_64-SDL2 mingw-w64-x86_64-SDL2_image mingw-w64-x86_64-SDL2_ttf
    stack install sdl2 sdl2-ttf

Things seem to be a bit broken at the moment with SDL2/SDL2_image, so you might need to use particular versions of the base libraries -- thanks to [this comment by jship](https://github.com/haskell-game/sdl2/issues/277#issuecomment-1784282404) we know a working version:

For my install, I used:

```
stack exec -- curl -O  https://repo.msys2.org/mingw/x86_64/mingw-w64-x86_64-SDL2-2.24.1-1-any.pkg.tar.zst
stack exec -- pacman -U mingw-w64-x86_64-SDL2-2.24.1-1-any.pkg.tar.zst
stack exec -- curl -O  https://repo.msys2.org/mingw/x86_64/mingw-w64-x86_64-SDL2_image-2.8.2-2-any.pkg.tar.zst
stack exec -- pacman -U mingw-w64-x86_64-SDL2_image-2.8.2-2-any.pkg.tar.zst
```

You may need to download the `.tar.zst` files manually from [the mingw repo](https://repo.msys2.org/mingw/x86_64/) and put them into this directory for these commands to work -- the `curl` call in jship's comment didn't work for me with one or both of them.

Next, we need to setup the [`sdl2-image` Haskell bindings](https://hackage.haskell.org/package/sdl2-image). Sadly, these don't compile readily on Windows 10 for me at the moment, so I had to make the following adjustments:

Clone the `sdl2-image` repository locally into a directory two up from this project's root (so it should be at `../../sdl2-image`) and checkout `v2.1.0.0`.

Next, edit `src/SDL/Raw/Image.hsc` in the library and add the following code before the `#include` on line 75:

```
#define _SDL_main_h
#define SDL_main_h_
#define SDL_MAIN_HANDLED
```

And after _that_ comment out the `sdl2-iamge-examples` section in the `package.yaml` and the package should build.

Once you have the above set up, this project should build correctly but it won't run because you'll need the DLLs for these libraries as well.

Download the Windows runtime binaries for your processer (likely x64) from:

 - [SDL2](https://www.libsdl.org/download-2.0.php)
 - [SDL_image 2.0](https://www.libsdl.org/projects/SDL_image/)
 - [SDL_ttf 2.0](https://www.libsdl.org/projects/SDL_ttf/)

Then unzip them and copy the DLLs to the root directory of the project. After this the application will run.

### Configuration on OSX

The first thing you'll need to do is get the compiler to work. For some reason the `-no_fixup_chains` flag in the configuration for the compilation setup messes things up, so you'll have to remove it.

Once the compiler has installed during `stack build`, edit the file:

```
~/.stack/programs/x86_64-osx/ghc-custom-native-int-9.4.7/bin/hsc2hs-ghc-9.4.7
```

and remove the `-no_fixup_chains` flag from the line for `HSC2HS_EXTRA`. See [this discussion on Haskell.org](https://discourse.haskell.org/t/solved-trouble-building-unix-package-on-macos-ld-unknown-option-no-fixup-chains/7772/12) for more information.

Next we'll need to get SDL2 and related libraries and their bindings installed.

SDL2 is easily installed on OSX using [Homebrew](https://brew.sh/) (as documented in the [Haskell game dev SDL2 documentation](https://github.com/haskell-game/sdl2))

    brew install pkg-config sdl2 sdl2_image sdl2_ttf

Once you have these installed, you'll need to clone the [`sdl2-image` Haskell bindings](https://hackage.haskell.org/package/sdl2-image) into `../../sdl2-image` and checkout `v2.1.0.0` there. (This step is largely unnecessary for OSX, but _is_ required on Windows so we have to do it here too. Alternatively you could just remove the extra-dep for `../../sdl2-image` from `stack.yaml` and things should work without cloning the repo.)

After this things should compile.

### Configuration on Linux

The configuration on Linux will vary by distribution. You may have to add a separate custom compiler section in `stack.yaml` to select the right compiler version/binary. (I'm hoping to obviate this soon.)

You will also need to install the SDL2 libraries. The naming of the SDL2 packages vary by distro, but you will need the SDL2 library along with SDl2-image and SDL2-ttf, and possibly pkg-config as well. Below are a few examples:

On Arch:

    sudo pacman -S sdl2_image sdl2_ttf

On Ubuntu:

    sudo apt install libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev

Once you have these installed, you'll need to clone the [`sdl2-image` Haskell bindings](https://hackage.haskell.org/package/sdl2-image) into `../../sdl2-image` and checkout `v2.1.0.0` there. (This step is largely unnecessary for Linux, but _is_ required on Windows so we have to do it here too. Alternatively you could just remove the extra-dep `../../sdl2-image` from `stack.yaml` and things should work without cloning the repo.)

## Development

Development of this project is done using a _code-then-compile-in-the-REPL_ approach, but there are a few caveats w.r.t. running the `main` function in the REPL:

 - it is _much_ slower when running through the REPL than when compiled
 - it cannot be run on OSX from the REPL because of an issue with the bindings; apparently there's a solution but I haven't yet bothered to investigate -- see the [Haskell SDL wiki](https://wiki.haskell.org/SDL) for more.

As a result of the above, I find it more convenient to run the program using `stack run`; I just check that it compiles using `stack repl` until I'm ready to run it.

### Playback on Windows

The application defaults to using the `directsound` audio driver when running on Windows.

If you want to change which driver is used for playback, you can change the `audio-driver` configuration setting or run the program using the `SDL_AUDIODRIVER` environment variable, like so:

    SDL_AUDIODRIVER=directsound stack run

# Acknowledgements

- **God** for everything including all the techniques I learned and insights I gained and the resources for troubleshooting / problem-solving I found during development
- [Dan Thau](https://github.com/paradigm) and Dave Householder for testing out Linux and Mac installs, respectively
