{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}

module Config.Settings where

import Control.Lens hiding ((.=))
import Control.Monad.Reader
import Control.Monad.Writer
import Data.Aeson.Key
import Data.Either
import Data.Either.Combinators hiding (fromRight)
import qualified Data.Map as M
import Data.Maybe
import Data.Yaml
import System.Directory
import System.FilePath
import System.Info
import Text.Read


data Settings
  = Settings
    { _audioDriverName :: !(Maybe String)
    , _defaultViewSpanInS :: !(Maybe Int)
    , _fileVersion :: !String
    , _maxIntraclipSilenceInMs :: !Int
    , _playbackBufferSampleCount :: !Int
    , _thresholdIndBDown :: !Float
    }
$(makeLenses ''Settings)

defaultSettings
  = Settings
    { _audioDriverName = if os == "mingw32" then Just "directsound" else Nothing
    , _defaultViewSpanInS = Just 20 -- if Nothing then use full file length
    , _fileVersion = currentFileVersionString
    , _maxIntraclipSilenceInMs = 50
    , _playbackBufferSampleCount = 1024
    , _thresholdIndBDown = -60
    }


--------------------------------
-- JSON
--------------------------------

audioDriverNameField = "audio-driver"
defaultViewSpanInSField = "default-view-span-in-seconds"
fileVersionField = "file-version"
maxIntraclipSilenceInMsField = "max-intraclip-silence-in-ms"
playbackBufferSampleCountField = "playback-buffer-sample-count"
thresholdIndBDownField = "threshold-in-dB-down"


instance FromJSON Settings where
  parseJSON = withObject "Settings" $ \v -> Settings
    <$> v .:? fromString audioDriverNameField
    <*> v .:? fromString defaultViewSpanInSField
    <*> v .: fromString fileVersionField
    <*> v .: fromString maxIntraclipSilenceInMsField
    <*> v .: fromString playbackBufferSampleCountField
    <*> v .: fromString thresholdIndBDownField

instance ToJSON Settings where
  toJSON settings =
    object
      [ fromString audioDriverNameField           .= _audioDriverName settings
      , fromString defaultViewSpanInSField        .= _defaultViewSpanInS settings
      , fromString fileVersionField               .= _fileVersion settings
      , fromString maxIntraclipSilenceInMsField   .= _maxIntraclipSilenceInMs settings
      , fromString playbackBufferSampleCountField .= _playbackBufferSampleCount settings
      , fromString thresholdIndBDownField         .= _thresholdIndBDown settings
      ]


--------------------------------
-- 'Has' pattern
--------------------------------

class HasSettings a where
  getSettings :: a -> Settings


getAudioDriverName :: (MonadReader r m, HasSettings r) => m (Maybe String)
getAudioDriverName = asks (_audioDriverName . getSettings)

getDefaultViewSpanInS :: (MonadReader r m, HasSettings r) => m (Maybe Int)
getDefaultViewSpanInS = asks (_defaultViewSpanInS . getSettings)

getMaxIntraclipSilenceInMs :: (MonadReader r m, HasSettings r) => m Int
getMaxIntraclipSilenceInMs = asks (_maxIntraclipSilenceInMs . getSettings)

getPlaybackBufferSampleCount :: (MonadReader r m, HasSettings r) => m Int
getPlaybackBufferSampleCount = asks (_playbackBufferSampleCount . getSettings)

getThresholdIndBDown :: (MonadReader r m, HasSettings r) => m Float
getThresholdIndBDown = asks (_thresholdIndBDown . getSettings)


--------------------------------
-- Parsing and Validtion
--------------------------------

parseAudioDriverName :: String -> Either String (Maybe String)
parseAudioDriverName input =
  if null input then return Nothing else return (Just input)

validateAudioDriverName :: Maybe String -> Either String (Maybe String)
validateAudioDriverName = Right

--

parseDefaultViewSpanInS :: String -> Either String (Maybe Int)
parseDefaultViewSpanInS input =
  if null input then return Nothing else
    case readMaybe input of
      Just v -> return (Just v)
      _ -> Left $ concat
        [ "Unable to parse '", defaultViewSpanInSField, "' value"]

validateDefaultViewSpanInS :: Maybe Int -> Either String (Maybe Int)
validateDefaultViewSpanInS value =
  if fromMaybe 1 value > 0 then return value else Left $
    defaultViewSpanInSField ++ " setting must be a positive, non-zero integer"

--

beta = "beta"
currentFileVersionString = beta
validFileVersionStrings = [beta]

fileVersionIsValid :: Settings -> Bool
fileVersionIsValid settings = _fileVersion settings `elem` validFileVersionStrings

fileVersionErrorMessage =
  "Unknown or unsupported file version, some settings may not take effect"

--

parseMaxIntraclipSilenceInMs :: String -> Either String Int
parseMaxIntraclipSilenceInMs =
  maybeToRight (concat ["Unable to parse '", maxIntraclipSilenceInMsField, "' value"])
  . readMaybe

validateMaxIntraclipSilenceInMs :: Int -> Either String Int
validateMaxIntraclipSilenceInMs value =
  if value >= 0 then return value else Left $
    maxIntraclipSilenceInMsField ++ " setting must not be a non-negative integer"

--

parsePlaybackBufferSampleCount :: String -> Either String Int
parsePlaybackBufferSampleCount =
  maybeToRight (concat ["Unable to parse '", playbackBufferSampleCountField, "' value"])
  . readMaybe

validatePlaybackBufferSampleCount :: Int -> Either String Int
validatePlaybackBufferSampleCount value =
  if value > 0 && isPowerOf2 value then return value else Left $
    playbackBufferSampleCountField ++ " setting must be an integer that is a power of 2"

isPowerOf2 :: Int -> Bool
isPowerOf2 f = 2 ^ floor (logBase 2 (fromIntegral f)) == f

--

parseThresholdIndBDown :: String -> Either String Float
parseThresholdIndBDown =
  maybeToRight (concat ["Unable to parse '", thresholdIndBDownField, "' value"])
  . readMaybe

validateThresholdIndBDown :: Float -> Either String Float
validateThresholdIndBDown value =
  if value < 0 then return value else Left $
    thresholdIndBDownField ++ " setting should be a negative number"

--

defaultIfInvalid
  :: (Show a)
  => Lens' Settings a -> (a -> Either String a) -> Settings -> Writer [String] Settings
defaultIfInvalid l validate settings = do
  let value = view l settings
  let defaultValue = view l defaultSettings
  let result = validate value
  logErrorAndDefaultMessage result defaultValue
  let value' = fromRight defaultValue result
  return (set l value' settings)

logErrorAndDefaultMessage :: (Show a) => Either String a -> a -> Writer [String] ()
logErrorAndDefaultMessage (Left e) defaultValue =
  tell [concat
    [ e
    , "\n\nUsing default value of "
    , show defaultValue
    , " instead."
    ]]
logErrorAndDefaultMessage _ _ = return ()


--------------------------------
-- File Reading and Writing
--------------------------------

establishSettingsFile :: MonadIO m => FilePath -> m ()
establishSettingsFile path = liftIO $ do
  fileExists <- doesFileExist path
  unless fileExists (writeSettingsFile defaultSettings path)

readSettingsFile :: MonadIO m => FilePath -> m (Settings, [String])
readSettingsFile path = liftIO $ do
  res <- decodeFileEither path
  return $ case res of
    Left e ->
      ( defaultSettings
      , [concat
        [ "Error reading settings file:\n\n"
        , show e
        , "\n\nUsing default settings instead."
        ]]
      )
    Right s ->
      let (settings, messages) = runWriter $
            defaultIfInvalid audioDriverName validateAudioDriverName s
            >>= defaultIfInvalid defaultViewSpanInS validateDefaultViewSpanInS
            >>= defaultIfInvalid maxIntraclipSilenceInMs validateMaxIntraclipSilenceInMs
            >>= defaultIfInvalid playbackBufferSampleCount validatePlaybackBufferSampleCount
            >>= defaultIfInvalid thresholdIndBDown validateThresholdIndBDown
      in if fileVersionIsValid settings
         then (settings, messages)
         else (settings, fileVersionErrorMessage:messages)

writeSettingsFile :: MonadIO m => Settings -> FilePath -> m ()
writeSettingsFile settings path = liftIO $ do
  createDirectoryIfMissing True (takeDirectory path)
  encodeFile path settings


--------------------------------
-- Setting via Command-Line
--------------------------------

type Gettables = M.Map String String
type Settables = M.Map String (String -> Either String Settings)


cmdGettables :: Settings -> Gettables
cmdGettables settings = M.fromList
  [ ( audioDriverNameField
    , getInSettings settings audioDriverName
    )
  , ( defaultViewSpanInSField
    , getInSettings settings defaultViewSpanInS
    )
  , ( maxIntraclipSilenceInMsField
    , getInSettings settings maxIntraclipSilenceInMs
    )
  , ( playbackBufferSampleCountField
    , getInSettings settings playbackBufferSampleCount
    )
  , ( thresholdIndBDownField
    , getInSettings settings thresholdIndBDown
    )
  ]

cmdSettables :: Settings -> Settables
cmdSettables settings = M.fromList
  [ ( audioDriverNameField
    , parseAudioDriverName
      >=> validateAudioDriverName
      >=> setInSettings settings audioDriverName
    )
  , ( defaultViewSpanInSField
    , parseDefaultViewSpanInS
      >=> validateDefaultViewSpanInS
      >=> setInSettings settings defaultViewSpanInS
    )
  , ( maxIntraclipSilenceInMsField
    , parseMaxIntraclipSilenceInMs
      >=> validateMaxIntraclipSilenceInMs
      >=> setInSettings settings maxIntraclipSilenceInMs
    )
  , ( playbackBufferSampleCountField
    , parsePlaybackBufferSampleCount
      >=> validatePlaybackBufferSampleCount
      >=> setInSettings settings playbackBufferSampleCount
    )
  , ( thresholdIndBDownField
    , parseThresholdIndBDown
      >=> validateThresholdIndBDown
      >=> setInSettings settings thresholdIndBDown
    )
  ]

getInSettings :: (Show a) => Settings -> Lens' Settings a -> String
getInSettings settings l = show (view l settings)

setInSettings :: Settings -> Lens' Settings a -> a -> Either String Settings
setInSettings settings l value = Right (set l value settings)

getSetting name path = do
  (settings, errors) <- readSettingsFile path
  if not (null errors)
  then mapM_ putStrLn errors
  else do
    let gettables = cmdGettables settings
    case M.lookup name gettables of
      Nothing -> putStrLn ("Could not find setting named " ++ name)
      Just v -> putStrLn v

listSettings = mapM_ putStrLn
  [ audioDriverNameField
  , defaultViewSpanInSField
  , maxIntraclipSilenceInMsField
  , playbackBufferSampleCountField
  , thresholdIndBDownField
  ]

setSetting name value path = do
  (settings, errors) <- readSettingsFile path
  if not (null errors)
  then mapM_ putStrLn errors
  else do
    let settables = cmdSettables settings
    case M.lookup name settables of
      Nothing -> putStrLn ("Could not find setting named " ++ name)
      Just set ->
        case set value of
          Left e -> putStrLn e
          Right settings' -> do
            writeSettingsFile settings' path
            putStrLn ("Set '" ++ name ++ "' to '" ++ value ++ "'.")
