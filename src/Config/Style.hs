module Config.Style
 ( Style(..)
 , HasStyle(..)
 , defaultStyle
 , establishStyleFile
 , readStyleFile
 --
 ) where

import Control.Monad.Reader
import Data.Yaml
import System.Directory
import System.FilePath

import Config.Style.Canonical
import qualified Config.Style.VBeta as VBeta


class HasStyle a where
  getStyle :: a -> Style
  updateStyle :: Style -> a -> a


defaultStyle = toStyle VBeta.defaultStyle

establishStyleFile :: MonadIO m => FilePath -> m ()
establishStyleFile path = liftIO $ do
  fileExists <- doesFileExist path
  unless fileExists $ do
    createDirectoryIfMissing True (takeDirectory path)
    encodeFile path VBeta.defaultStyle

readStyleFile :: MonadIO m => FilePath -> m (Style, [String])
readStyleFile path = liftIO $ do
  res <- decodeFileEither path :: IO (Either ParseException VBeta.Style)
  return $ case res of
    Left e ->
      ( defaultStyle
      , ["Error reading style file:\n\n" ++ show e ++ "\n\nUsing default style instead"]
      )
    Right s -> (toStyle s, [])

