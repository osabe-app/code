module Config.Style.Canonical where

import Primitive.Color


-- Planning questions:
-- make font size/family configurable?
-- make image sources configurable?

data Style
  = Style
    { versionOf :: !String
    -- instructions screen
    , instructionsScreenBackgroundColorOf :: !Color
    -- load screen
    , loadScreenBackgroundColorOf :: !Color
    , loadScreenTextColorOf :: !Color
    , loadScreenProgressBarActiveBackgroundColorOf :: !Color
    , loadScreenProgressBarActiveBorderColorOf :: !Color
    , loadScreenProgressBarActivePaddingSizeOf :: !Word
    , loadScreenProgressBarCompleteBackgroundColorOf :: !Color
    , loadScreenProgressBarCompleteBorderColorOf :: !Color
    , loadScreenProgressBarCompletePaddingSizeOf :: !Word
    , loadScreenProgressBarInactiveBackgroundColorOf :: !Color
    , loadScreenProgressBarInactiveBorderColorOf :: !Color
    , loadScreenProgressBarInactivePaddingSizeOf :: !Word
    -- edit screen
    , editScreenAxisBackgroundColorOf :: !Color
    , editScreenAxisBorderColorOf :: !Color
    , editScreenAxisTextColorOf :: !Color
    , editScreenClipActiveBackgroundColorOf :: !Color
    , editScreenClipActiveForegroundColorOf :: !Color
    , editScreenClipHoverBackgroundColorOf :: !Color
    , editScreenClipHoverForegroundColorOf :: !Color
    , editScreenClipNeutralBackgroundColorOf :: !Color
    , editScreenClipNeutralForegroundColorOf :: !Color
    , editScreenClipRemovingBackgroundColorOf :: !Color
    , editScreenClipRemovingForegroundColorOf :: !Color
    , editScreenDeadSpaceColorOf :: !Color
    , editScreenGapNeutralColorOf :: !Color
    , editScreenGapRemovingColorOf :: !Color
    -- TODO gap resizing color
    , editScreenMarkingCursorColorOf :: !Color
    , editScreenNavigationBarHeightOf :: !Word
    , editScreenPlaybackCursorActiveColorOf :: !Color
    , editScreenPlaybackCursorIdleColorOf :: !Color
    , editScreenPrecisionCursorActivatingColorOf :: !Color
    , editScreenPrecisionCursorIdleColorOf :: !Color
    , editScreenPrecisionCursorModifyingColorOf :: !Color
    -- TODO toolbar background color
    , editScreenResizeBarBackgroundColorOf :: !Color
    , editScreenResizeBarSizeOf :: !Word
    , editScreenStatusBarBackgroundColorOf :: !Color
    , editScreenStatusBarTextColorOf :: !Color
    , editScreenViewWindowActiveBackgroundColorOf :: !Color
    , editScreenViewWindowActiveBorderColorOf :: !Color
    , editScreenViewWindowHoverBackgroundColorOf :: !Color
    , editScreenViewWindowHoverBorderColorOf :: !Color
    , editScreenViewWindowNeutralBackgroundColorOf :: !Color
    , editScreenViewWindowNeutralBorderColorOf :: !Color
    -- TODO view window zooming/resizing colors
    }


class ToStyle a where
  toStyle :: a -> Style
