{-# LANGUAGE TemplateHaskell #-}

module Config.Style.VBeta.LoadScreen where

import Control.Lens
import Data.Aeson
import Data.Aeson.TH
import Text.Casing

import Primitive.Color
import Config.Style.VBeta.Color


--------------------------------
-- Progress Bar Styles
--------------------------------

data ProgressBarStyle
  = ProgressBarStyle
    { _progressBarBackgroundColor :: !Color
    , _progressBarBorderColor :: !Color
    , _progressBarPaddingSize :: !(Maybe Word)
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 12
  , omitNothingFields = True
  }
  ''ProgressBarStyle)
$(makeLenses ''ProgressBarStyle)

defaultProgressBarStyle =
  ProgressBarStyle
  { _progressBarBackgroundColor = invisible
  , _progressBarBorderColor = white
  , _progressBarPaddingSize = Just 2
  }


data ProgressBarStyles
  = ProgressBarStyles
    { _progressBarActive :: !ProgressBarStyle
    , _progressBarComplete :: !ProgressBarStyle
    , _progressBarInactive :: !ProgressBarStyle
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 12
  , omitNothingFields = True
  }
  ''ProgressBarStyles)
$(makeLenses ''ProgressBarStyles)

defaultProgressBarStyles =
  ProgressBarStyles
  { _progressBarActive =
      ProgressBarStyle
      { _progressBarBackgroundColor = gray 32
      , _progressBarBorderColor = gray 128
      , _progressBarPaddingSize = Just 2
      }
  , _progressBarComplete =
      ProgressBarStyle
      { _progressBarBackgroundColor = gray 128
      , _progressBarBorderColor = white
      , _progressBarPaddingSize = Just 2
      }
  , _progressBarInactive =
      ProgressBarStyle
      { _progressBarBackgroundColor = gray 32
      , _progressBarBorderColor = gray 128
      , _progressBarPaddingSize = Just 2
      }
  }


--------------------------------
-- Load Screen Styles
--------------------------------

data LoadScreenStyle
  = LoadScreenStyle
    { _loadScreenBackgroundColor :: !Color
    , _loadScreenProgressBars :: !ProgressBarStyles
    , _loadScreenTextColor :: !Color
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 11
  , omitNothingFields = True
  }
  ''LoadScreenStyle)
$(makeLenses ''LoadScreenStyle)


defaultLoadScreenStyle =
  LoadScreenStyle
  { _loadScreenBackgroundColor = gray 64
  , _loadScreenProgressBars = defaultProgressBarStyles
  , _loadScreenTextColor = white
  }

