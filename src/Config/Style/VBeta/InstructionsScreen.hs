{-# LANGUAGE TemplateHaskell #-}

module Config.Style.VBeta.InstructionsScreen where

import Control.Lens
import Data.Aeson
import Data.Aeson.TH
import Text.Casing

import Primitive.Color
import Config.Style.VBeta.Color


newtype InstructionsScreenStyle
  = InstructionsScreenStyle
    { _instructionsScreenBackgroundColor :: Color
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 19
  , omitNothingFields = True
  }
  ''InstructionsScreenStyle)
$(makeLenses ''InstructionsScreenStyle)


defaultInstructionsScreenStyle =
  InstructionsScreenStyle
  { _instructionsScreenBackgroundColor = gray 72
  }
