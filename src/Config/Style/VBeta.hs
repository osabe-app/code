{-# LANGUAGE TemplateHaskell #-}

module Config.Style.VBeta
 ( Style
 , defaultStyle
 ) where

import Control.Lens
import Data.Aeson.TH
import Data.Maybe
import Text.Casing

import qualified Config.Style.Canonical as C
import Config.Style.VBeta.EditScreen
import Config.Style.VBeta.InstructionsScreen
import Config.Style.VBeta.LoadScreen


data Style
  = Style
    { _fileVersion :: !String
    , _editScreen :: !EditScreenStyle
    , _instructionsScreen :: !InstructionsScreenStyle
    , _loadScreen :: !LoadScreenStyle
    }
  deriving (Eq)
$(deriveJSON
  defaultOptions
  { fieldLabelModifier = toKebab . fromHumps . drop 1
  , omitNothingFields = True
  }
  ''Style)
$(makeLenses ''Style)


defaultStyle =
  Style
  { _fileVersion = "beta"
  , _editScreen = defaultEditScreenStyle
  , _instructionsScreen = defaultInstructionsScreenStyle
  , _loadScreen = defaultLoadScreenStyle
  }


instance C.ToStyle Style where
  toStyle beta =
    C.Style
    { C.versionOf = _fileVersion beta
    -- instructions screen
    , C.instructionsScreenBackgroundColorOf =
        view (instructionsScreen.instructionsScreenBackgroundColor) beta
    -- load screen
    , C.loadScreenBackgroundColorOf =
        view (loadScreen.loadScreenBackgroundColor) beta
    , C.loadScreenTextColorOf = 
        view (loadScreen.loadScreenTextColor) beta
    , C.loadScreenProgressBarActiveBackgroundColorOf =
        view (loadScreen.loadScreenProgressBars.progressBarActive.progressBarBackgroundColor) beta
    , C.loadScreenProgressBarActiveBorderColorOf =
        view (loadScreen.loadScreenProgressBars.progressBarActive.progressBarBorderColor) beta
    , C.loadScreenProgressBarActivePaddingSizeOf =
        fromMaybe 0 $ view (loadScreen.loadScreenProgressBars.progressBarActive.progressBarPaddingSize) beta
    , C.loadScreenProgressBarCompleteBackgroundColorOf =
        view (loadScreen.loadScreenProgressBars.progressBarComplete.progressBarBackgroundColor) beta
    , C.loadScreenProgressBarCompleteBorderColorOf =
        view (loadScreen.loadScreenProgressBars.progressBarComplete.progressBarBorderColor) beta
    , C.loadScreenProgressBarCompletePaddingSizeOf =
        fromMaybe 0 $ view (loadScreen.loadScreenProgressBars.progressBarComplete.progressBarPaddingSize) beta
    , C.loadScreenProgressBarInactiveBackgroundColorOf =
        view (loadScreen.loadScreenProgressBars.progressBarInactive.progressBarBackgroundColor) beta
    , C.loadScreenProgressBarInactiveBorderColorOf =
        view (loadScreen.loadScreenProgressBars.progressBarInactive.progressBarBorderColor) beta
    , C.loadScreenProgressBarInactivePaddingSizeOf =
        fromMaybe 0 $ view (loadScreen.loadScreenProgressBars.progressBarInactive.progressBarPaddingSize) beta
    -- edit screen
    , C.editScreenAxisBackgroundColorOf =
        view (editScreen.editScreenAxis.axisBackgroundColor) beta
    , C.editScreenAxisBorderColorOf =
        view (editScreen.editScreenAxis.axisBorderColor) beta
    , C.editScreenAxisTextColorOf =
        view (editScreen.editScreenAxis.axisTextColor) beta
    , C.editScreenClipActiveBackgroundColorOf =
        view (editScreen.editScreenClipStyles.clipActive.clipBackgroundColor) beta
    , C.editScreenClipActiveForegroundColorOf =
        view (editScreen.editScreenClipStyles.clipActive.clipForegroundColor) beta
    , C.editScreenClipHoverBackgroundColorOf =
        view (editScreen.editScreenClipStyles.clipHover.clipBackgroundColor) beta
    , C.editScreenClipHoverForegroundColorOf =
        view (editScreen.editScreenClipStyles.clipHover.clipForegroundColor) beta
    , C.editScreenClipNeutralBackgroundColorOf =
        view (editScreen.editScreenClipStyles.clipNeutral.clipBackgroundColor) beta
    , C.editScreenClipNeutralForegroundColorOf =
        view (editScreen.editScreenClipStyles.clipNeutral.clipForegroundColor) beta
    , C.editScreenClipRemovingBackgroundColorOf =
        view (editScreen.editScreenClipStyles.clipRemoving.clipBackgroundColor) beta
    , C.editScreenClipRemovingForegroundColorOf =
        view (editScreen.editScreenClipStyles.clipRemoving.clipForegroundColor) beta
    , C.editScreenDeadSpaceColorOf =
        view (editScreen.editScreenDeadSpaceColor) beta
    , C.editScreenGapNeutralColorOf =
        view (editScreen.editScreenGapStyles.gapNeutral.gapBackgroundColor) beta
    , C.editScreenGapRemovingColorOf =
        view (editScreen.editScreenGapStyles.gapRemoving.gapBackgroundColor) beta
    , C.editScreenMarkingCursorColorOf =
        view (editScreen.editScreenMarkingCursorColor) beta
    , C.editScreenNavigationBarHeightOf =
        view (editScreen.editScreenNavigationBarHeight) beta
    , C.editScreenPlaybackCursorActiveColorOf =
        view (editScreen.editScreenPlaybackCursor.playbackCursorActiveColor) beta
    , C.editScreenPlaybackCursorIdleColorOf =
        view (editScreen.editScreenPlaybackCursor.playbackCursorIdleColor) beta
    , C.editScreenPrecisionCursorActivatingColorOf =
        view (editScreen.editScreenPrecisionCursor.precisionCursorActivatingColor) beta
    , C.editScreenPrecisionCursorIdleColorOf =
        view (editScreen.editScreenPrecisionCursor.precisionCursorIdleColor) beta
    , C.editScreenPrecisionCursorModifyingColorOf =
        view (editScreen.editScreenPrecisionCursor.precisionCursorModifyingColor) beta
    , C.editScreenResizeBarBackgroundColorOf =
        view (editScreen.editScreenResizeBar.resizeBarBackgroundColor) beta
    , C.editScreenResizeBarSizeOf =
        view (editScreen.editScreenResizeBar.resizeBarSize) beta
    , C.editScreenStatusBarBackgroundColorOf =
        view (editScreen.editScreenStatusBar.statusBarBackgroundColor) beta
    , C.editScreenStatusBarTextColorOf =
        view (editScreen.editScreenStatusBar.statusBarTextColor) beta
    , C.editScreenViewWindowActiveBackgroundColorOf =
        view (editScreen.editScreenViewWindow.viewWindowActive.viewWindowBackgroundColor) beta
    , C.editScreenViewWindowActiveBorderColorOf =
        view (editScreen.editScreenViewWindow.viewWindowActive.viewWindowBorderColor) beta
    , C.editScreenViewWindowHoverBackgroundColorOf =
        view (editScreen.editScreenViewWindow.viewWindowHover.viewWindowBackgroundColor) beta
    , C.editScreenViewWindowHoverBorderColorOf =
        view (editScreen.editScreenViewWindow.viewWindowHover.viewWindowBorderColor) beta
    , C.editScreenViewWindowNeutralBackgroundColorOf =
        view (editScreen.editScreenViewWindow.viewWindowNeutral.viewWindowBackgroundColor) beta
    , C.editScreenViewWindowNeutralBorderColorOf =
        view (editScreen.editScreenViewWindow.viewWindowNeutral.viewWindowBorderColor) beta
    }
