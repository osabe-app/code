{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Primitive.Geometry where

import Control.DeepSeq
import Data.Vector.Unboxed
import Data.Vector.Unboxed.Deriving
import Foreign.Ptr
import Foreign.Storable
import FULE
import GHC.Generics


--------------------------------
-- Location & Size
--------------------------------

data Coordinates
  = Coordinates
    { xOf :: !Int
    , yOf :: !Int
    }
  deriving (Eq, Generic, NFData, Ord, Show)

{-# ANN module "HLint: ignore Use uncurry" #-}
derivingUnbox "Coordinates"
  [t| Coordinates -> (Int, Int)   |]
  [| \(Coordinates x y) -> (x, y) |]
  [| \(x, y) -> Coordinates x y   |]

intAlignment = alignment (0::Int)
intSize = sizeOf (0::Int)

instance Storable Coordinates where
  sizeOf _ = 2 * intSize
  alignment _ = intAlignment
  peek ptr = do
    let xptr = castPtr ptr :: Ptr Int
    x <- peek xptr
    let yptr = plusPtr xptr (sizeOf x)
    y <- peek yptr
    return (Coordinates x y)
  poke ptr (Coordinates x y) = do
    let xptr = castPtr ptr :: Ptr Int
    poke xptr x
    let yptr = plusPtr xptr (sizeOf x)
    poke yptr y


origin = Coordinates 0 0


data Deltas
  = Deltas
    { deltaXOf :: !Int
    , deltaYOf :: !Int
    }
  deriving (Eq, Generic, NFData, Ord, Show)


zeroDeltas = Deltas 0 0

-- get how far the second coordinate is from the first
getOffset :: Coordinates -> Coordinates -> Deltas
getOffset (Coordinates fx fy) (Coordinates sx sy) = Deltas (sx-fx) (sy-fy)

addOffset :: Coordinates -> Deltas -> Coordinates
addOffset (Coordinates x y) (Deltas dx dy) = Coordinates (x+dx) (y+dy)

subOffset :: Coordinates -> Deltas -> Coordinates
subOffset (Coordinates x y) (Deltas dx dy) = Coordinates (x-dx) (y-dy)


data Dimensions
  = Dimensions
    { widthOf :: !Int
    , heightOf :: !Int
    }
  deriving (Eq, Generic, NFData, Ord, Show)


--------------------------------
-- Lines
--------------------------------

data Line
  = Line
    { pointAOf :: !Coordinates
    , pointBOf :: !Coordinates
    }
  deriving (Eq, Show)
derivingUnbox "Line"
  [t| Line -> (Int, Int, Int, Int) |]
  [| \(Line (Coordinates x1 y1) (Coordinates x2 y2)) -> (x1, y1, x2, y2) |]
  [| \(x1, y1, x2, y2) -> Line (Coordinates x1 y1) (Coordinates x2 y2) |]

newtype Polyline
  = Polyline
    { pointsOf :: Vector Coordinates
    }
  deriving (Eq, Show)


--------------------------------
-- Rectangle
--------------------------------

data Rectangle
  = Rectangle
    { coordinatesOf :: !Coordinates
    , dimensionsOf :: !Dimensions
    }
  deriving (Eq, Generic, NFData, Show)


zeroRect = Rectangle origin (Dimensions 0 0)

constrictBy :: Int -> Rectangle -> Rectangle
constrictBy n (Rectangle (Coordinates x y) (Dimensions w h)) =
  Rectangle (Coordinates (x+n) (y+n)) (Dimensions (w-2*n) (h-2*n))

doubleRect :: Rectangle -> Rectangle
doubleRect (Rectangle (Coordinates x y) (Dimensions w h)) =
  Rectangle (Coordinates (x*2) (y*2)) (Dimensions (w*2) (h*2))

rectToPoly :: Rectangle -> Polyline
rectToPoly (Rectangle (Coordinates x y) (Dimensions w h)) =
  Polyline $ fromList
    [ Coordinates x y
    , Coordinates (x+w-1) y
    , Coordinates (x+w-1) (y+h-1)
    , Coordinates x (y+h-1)
    , Coordinates x y
    ]

--------------------------------

shrinkBottomSide :: Int -> Rectangle -> Rectangle
shrinkBottomSide size (Rectangle c (Dimensions w h)) =
  Rectangle c (Dimensions w (h-size))

setHeightFromBottom :: Int -> Rectangle -> Rectangle
setHeightFromBottom size (Rectangle (Coordinates x y) (Dimensions w h)) =
  Rectangle (Coordinates x (y+h-size)) (Dimensions w size)

--

shrinkLeftSide :: Int -> Rectangle -> Rectangle
shrinkLeftSide size (Rectangle (Coordinates x y) (Dimensions w h)) =
  Rectangle (Coordinates (x+size) y) (Dimensions (w-size) h)

setWidthFromLeft :: Int -> Rectangle -> Rectangle
setWidthFromLeft size (Rectangle c (Dimensions w h)) =
  Rectangle c (Dimensions size h)

--

shrinkRightSide :: Int -> Rectangle -> Rectangle
shrinkRightSide size (Rectangle c (Dimensions w h)) =
  Rectangle c (Dimensions (w-size) h)

setWidthFromRight :: Int -> Rectangle -> Rectangle
setWidthFromRight size (Rectangle (Coordinates x y) (Dimensions w h)) =
  Rectangle (Coordinates (x+w-size) y) (Dimensions size h)

--

shrinkTopSide :: Int -> Rectangle -> Rectangle
shrinkTopSide size (Rectangle (Coordinates x y) (Dimensions w h)) =
  Rectangle (Coordinates x (y+size)) (Dimensions w (h-size))

setHeightFromTop :: Int -> Rectangle -> Rectangle
setHeightFromTop size (Rectangle c (Dimensions w h)) =
  Rectangle c (Dimensions w size)

--------------------------------

intersection :: Rectangle -> Rectangle -> Maybe Rectangle
intersection r1 r2 =
  if wt < w1+w2 && ht < h1+h2
  then Just (Rectangle (Coordinates x' y') (Dimensions w' h'))
  else Nothing
  where
    Rectangle (Coordinates x1 y1) (Dimensions w1 h1) = r1
    Rectangle (Coordinates x2 y2) (Dimensions w2 h2) = r2
    xt = min x1 x2
    yt = min y1 y2
    xwt = max (x1+w1) (x2+w2)
    yht = max (y1+h1) (y2+h2)
    wt = xwt - xt
    ht = yht - yt
    x' = max x1 x2
    y' = max y1 y2
    w' = min (x1+w1) (x2+w2) - x'
    h' = min (y1+h1) (y2+h2) - y'

totalIntersection :: [Rectangle] -> Maybe Rectangle
totalIntersection [] = Nothing
totalIntersection [r] = Just r
totalIntersection (r1:r2:rs) =
  case intersection r1 r2 of
    Just r -> totalIntersection (r:rs)
    Nothing -> totalIntersection rs

inBounds :: Rectangle -> Coordinates -> Bool
inBounds (Rectangle (Coordinates bx by) (Dimensions bw bh)) (Coordinates cx cy) =
  bx <= cx && cx < bx+bw && by <= cy && cy < by+bh

ratioX :: Coordinates -> Rectangle -> Float
ratioX (Coordinates cx _) (Rectangle (Coordinates bx _) (Dimensions bw _)) =
  fromIntegral (cx - bx) / fromIntegral bw

ratio :: Integral a => a -> a -> Float
ratio num denom = fromIntegral num / fromIntegral denom

toXIn :: Rectangle -> Int -> Int -> Int
toXIn (Rectangle (Coordinates x _) (Dimensions w _)) offset span =
  round (fromIntegral offset / fromIntegral span * fromIntegral w) + x

--------------------------------

makeBoundsRect :: Layout -> Bounds -> Rectangle
makeBoundsRect layout bounds = Rectangle coords dims
  where
    [top, left, right, bottom] = boundingGuidesFor layout bounds
    coords = Coordinates left top
    dims = Dimensions (right - left) (bottom - top)

makeBoundsRects :: Layout -> Bounds -> [Rectangle]
makeBoundsRects layout bounds =
  case clippingOf bounds of
    Just c -> makeBoundsRect layout bounds : makeBoundsRects layout c
    Nothing -> [makeBoundsRect layout bounds]

