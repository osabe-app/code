{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}

module Primitive.Color
 ( Color(..)
 , parseColor
 , invisible
 , black
 , white
 , gray
 , red
 , orange
 , yellow
 , green
 , blue
 , purple
 , cyan
 , magenta
 )  where

import Control.DeepSeq
import Data.Bits
import Data.Char
import Data.Word
import GHC.Generics (Generic)
import Numeric
import Text.Parsec


data Color
  = Color
    { redOf :: !Word8
    , greenOf :: !Word8
    , blueOf :: !Word8
    , alphaOf :: !Word8
    }
  deriving (Eq, Generic, NFData)

instance Show Color where
  show (Color r g b 255) =
    let word8ToHex w8 = [intToDigit (w8 `shift` (-4)), intToDigit (w8 .&. 0xF)]
    in concat
    [ "#"
    , word8ToHex (fromIntegral r)
    , word8ToHex (fromIntegral g)
    , word8ToHex (fromIntegral b)
    ]
  show (Color r g b a) = concat
    [ "rgba("
    , show r, ", "
    , show g, ", "
    , show b, ", "
    , show a, ")"
    ]



invisible = Color 0 0 0 0
black = Color 0 0 0 255
white = Color 255 255 255 255
gray g = Color g g g 255
red = Color 255 0 0 255
orange = Color 255 134 0 255
yellow = Color 255 255 0 255
green = Color 0 255 0 255
blue = Color 0 0 255 255
purple = Color 142 0 255 255
cyan = Color 0 255 255 255
magenta = Color 255 0 255 255


--------------------------------
-- Parsing
--------------------------------

parseColor :: String -> Either ParseError Color
parseColor = parse (try decimalColor <|> hexColor) ""


--------------------------------
-- Decimal
--------------------------------

decimalColor = do
  color <- try rgbaColor <|> rgbColor
  eof
  return color

word8 :: Stream s m Char => ParsecT s u m Word8
word8 = do
  digits <- many1 digit
  let num = read digits
  if num < 256
  then return (fromIntegral num)
  else fail "Number too big"

comma = spaces >> char ',' >> spaces

rgbaColor = do
  string "rgba(" >> spaces
  r <- word8
  comma
  g <- word8
  comma
  b <- word8
  comma
  a <- word8
  spaces >> char ')'
  return (Color r g b a)

rgbColor = do
  string "rgb(" >> spaces
  r <- word8
  comma
  g <- word8
  comma
  b <- word8
  spaces >> char ')'
  return (Color r g b 255)


--------------------------------
-- Hexadecimal
--------------------------------

hexColor = do
  char '#'
  digits <- try (count 8 hexDigit) -- RGBA
        <|> try (count 6 hexDigit) -- RGB
        <|> try (count 4 hexDigit) -- RGBA Shorthand
        <|>      count 3 hexDigit  -- RGB  Shorthand
  eof
  return (hexStringToColor digits)

hex2word8 :: String -> Word8
hex2word8 = fromIntegral . fst . head . readHex

hexStringToColor str =
  case str of
    [ru,rl,gu,gl,bu,bl,au,al] ->
      Color (hex2word8 [ru,rl]) (hex2word8 [gu,gl]) (hex2word8 [bu,bl]) (hex2word8 [au,al])
    [ru,rl,gu,gl,bu,bl] ->
      Color (hex2word8 [ru,rl]) (hex2word8 [gu,gl]) (hex2word8 [bu,bl]) 255
    [r,g,b,a] ->
      Color (hex2word8 [r,r]) (hex2word8 [g,g]) (hex2word8 [b,b]) (hex2word8 [a,a])
    [r,g,b] ->
      Color (hex2word8 [r,r]) (hex2word8 [g,g]) (hex2word8 [b,b]) 255
