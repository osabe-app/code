{-# LANGUAGE BlockArguments #-}

module Audio.WaveFile
 ( WaveFile -- re-export
 , WaveFileException
 , readWaveFile
 , writeWaveFile
 ) where

import Control.Exception
import Data.Maybe

import Audio.Segments
import Audio.Wave.Content
import Audio.Wave.Header


data WaveFileException
  = UnanticipatedErrorReadingWaveFile
  | UnanticipatedErrorWritingWaveFile
  | WaveFileReadContentsException !WaveFileContentsException
  | WaveFileReadHeaderException !WaveFileHeaderException
  | WaveFileWriteContentsException !IOException
  deriving (Show)

instance Exception WaveFileException

fromWaveFileContentsException :: SomeException -> Maybe WaveFileException
fromWaveFileContentsException ex =
  case fromException ex :: Maybe WaveFileContentsException of
    Just e  -> Just (WaveFileReadContentsException e)
    Nothing -> Nothing

fromWaveFileHeaderException :: SomeException -> Maybe WaveFileException
fromWaveFileHeaderException ex =
  case fromException ex :: Maybe WaveFileHeaderException of
    Just e  -> Just (WaveFileReadHeaderException e)
    Nothing -> Nothing

readWaveFile :: FilePath -> Reporter -> IO WaveFile
readWaveFile path report = do
  report 0
  r <- try (readWaveFileContents path report)
  case r of
    Right wf -> return wf
    Left ex -> throwIO . head $
      catMaybes
        [ fromWaveFileContentsException ex
        , fromWaveFileHeaderException ex
        , Just UnanticipatedErrorReadingWaveFile
        ]

writeWaveFile :: FilePath -> WaveFile -> Segments -> Reporter -> IO ()
writeWaveFile path waveFile segments report = do
  report 0
  r <- try (writeWaveFileContents path waveFile segments report)
  case r of
    Right _ -> return ()
    Left ex ->
      case fromException ex :: Maybe IOException of
        Just e -> throwIO (WaveFileWriteContentsException e)
        Nothing -> throwIO UnanticipatedErrorWritingWaveFile
