module Audio.Decibel
 ( Decibel
 , levelOf
 , dBDownFrom
 , scaleForDisplay
 ) where

import Data.Bits
import Data.Word
import GHC.Float


{- HERE BE DRAGONS!!!
 - (a small one tho)
 -
 -
 - The Decibel format used here is not a pure decibel value.
 -
 - According to my research:
 -
 - Wave signals fluxuate between positive and negative values
 - around a zero value -- in some formats (e.g. Word8) the
 - zero value is actually in the middle of the span of values
 - that the data type can take on, but for the calculation of
 - decibels this is adjusted to be zero.
 -
 - A decibel is the ratio of a sample value* as compared to some
 - fixed reference point, and in the data formats used for wave
 - files the maximum positive value of the format is used as the
 - fixed value[1] -- this is known as "Decibels relative to
 - Full-Scale" (dBFS).
 -
 - *We can either use the RMS value of a signal or the peak level
 - of a signal when calculating decibel value; in this program
 - the peak level is used.
 -
 - The base formula for computing a decibel (from a signal level) is:
 -
 -   20 * log_10 (sample / reference)
 -
 - Since we can't take the logarithm of a negative number, samples
 - from the negative part of a waveform need to be adjusted to be
 - the magnitude (i.e. absolute value) of the sample in order to
 - take the ratio and compute the decibel. All decibels computed
 - relative to full-scale then should have values that are <= 0.0f
 -
 - However:
 -
 - Since signed integer types are not symmetric, there is one more
 - possible negative value than the number of positive values.
 - According to [1], this means that when computing the decibel of
 - the greatest negative number, the resulting value will actually
 - exceed the full-scale maximum decibel of 0.0f and be a positive
 - decibel value.
 -
 - [1] https://en.wikipedia.org/wiki/Full_scale
 -
 -
 - For the encoding used in this program, two things are changed from
 - the above:
 -  - The sign of the original signal value is encoded in the decibel value
 -    for easier display of the signal
 -  - To facilitate this encoding, the magnitude of the max negative sample
 -    is reduced to match the magnitude of the max positive value
 -
 -
 - The encoding used here is as follows:
 -
 -
 - With the full-scale truncation of the maximum negative value, the
 - max positive signal value and new max negative signal value will
 - both yield decibel values of 0.0f; the zero signal value will be
 - a -Inf decibel value:
 -
 - Signal value:	max-     0      max+
 - Decibel value:	0.0     -Inf     0.0
 -
 - Thus the sign of the decibel value will always be negative, except
 - when the value is zero, so the sign itself is superfluous.
 -
 - The range for floating-point numbers is:
 -
 - 			-Inf --  0.0  -- +Inf
 -
 - Since the IEEE Floating-Point standard supports _negative
 - zero values_, we can use the now-unused sign bit to designate
 - which part of the waveform the original signal value was on,
 - and encode things thus:
 -
 - Signal range:	 neg signal  /  pos signal
 - Decibel encoding:	-0.0 -- -Inf / +Inf -- +0.0
 -
 - This leads to an easier time displaying the decibel representation
 - of the waveform.
 -
 -}

newtype Decibel = Decibel Float
  deriving (Eq, Show)


signMask :: Word32
signMask  = 0x80000000

valueMask :: Word32
valueMask = 0x7FFFFFFF

levelOf :: Decibel -> Float
levelOf (Decibel f) = castWord32ToFloat (signMask .|. castFloatToWord32 f)

dBCoeff :: Float
dBCoeff = 20 / log 10

dBDownFrom :: Float -> Float -> Decibel
dBDownFrom ref sample =
  let sample' = min ref (abs sample) -- limit to full-scale values
      dB = dBCoeff * log (sample' / ref)
      sign = signMask .&. castFloatToWord32 sample -- encode sign of original sample
      value = valueMask .&. castFloatToWord32 dB
  in Decibel (castWord32ToFloat (sign .|. value))

one = castFloatToWord32 (1.0::Float)

-- Because I'm forgetful:
--
-- This function sets things up for display, moving from a scale of:
--
--     -0 -- -Inf / +Inf -- +0
--
-- to:
--
--     -1    --   0   --    +1
--
-- It does so as follows:
--
-- First, we cut off the value range making the `threshold` the minimum value.
-- Next, we divide by the threshold (a negative number), which does two things:
--  - it scales the values down to the 0--1 range (on either side of 0)
--  - it reverses the direction of the values' ranges by rotating around 0
-- After this, we add +/-1 to slide the ranges back to the proper side of 0
-- for what the original sample was.
--
scaleForDisplay :: Decibel -> Float -> Float
scaleForDisplay (Decibel f) threshold =
  let at = abs threshold
      f' | f < threshold = threshold
         | f > at        = at
         | otherwise     = f
      sign = signMask .&. castFloatToWord32 f
      slide = castWord32ToFloat (sign .|. one)
  in f' / threshold + slide
