{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

module Audio.Wave.Content
 ( WaveFile
 , WaveFileContentsException
 , Reporter
 , readWaveFileContents
 , writeWaveFileContents
 , writeSegments
 ) where

import Control.Exception
import Control.Monad
import Data.Binary.Get
import Data.Binary.Put
import qualified Data.ByteString.Lazy as BSL
import qualified Data.Vector.Storable as VS
import qualified Data.Vector.Storable.Mutable as VSM
import Data.Int
import Data.Maybe
import Data.Word
import System.IO

import Audio.AudioData
import Audio.AudioSample
import Audio.Segment
import Audio.Segments
import Audio.Wave.Header
import Audio.Wave.Sample


data Samples
  = OfWord8 !(VS.Vector Word8)
  | OfInt16 !(VS.Vector Int16)
  | OfInt24 !(VS.Vector Int24)
  | OfInt32 !(VS.Vector Int32)
  | OfFloat !(VS.Vector Float)

data WaveFile
  = WaveFile
    { headerOf :: !WaveFileHeader
    , samplesOf :: !Samples
    }

instance AudioData WaveFile where
  numberOfSamplesIn = fileSizeOf . headerOf
  sampleRateOf = getSampleRate . headerOf
  sampleTypeOf waveFile =
    case samplesOf waveFile of
      OfWord8 _ -> Word8
      OfInt16 _ -> Int16
      OfInt24 _ -> Int24
      OfInt32 _ -> Int32
      OfFloat _ -> Float
  fillWith waveFile segment contentType f =
    case (contentType, samplesOf waveFile) of
      (Word8Content, OfWord8 v) -> populate f segment v
      (Int16Content, OfInt16 v) -> populate f segment v
      (Int24Content, OfInt24 v) -> populate f segment v
      (FloatContent, OfInt24 v) -> populate (f . VS.map int24ToFloatSample) segment v
      (Int32Content, OfInt32 v) -> populate f segment v
      (FloatContent, OfFloat v) -> populate f segment v
      -- while this case should not happen, it can when testing on Windows
      -- and the SDL_AUDIODRIVER is not set, so we `return ()` here instead
      -- of erroring:
      _ -> return ()
  dBFSAt index waveFile =
    case samplesOf waveFile of
      OfWord8 v -> dBFS (v VS.! index)
      OfInt16 v -> dBFS (v VS.! index)
      OfInt24 v -> dBFS (v VS.! index)
      OfInt32 v -> dBFS (v VS.! index)
      OfFloat v -> dBFS (v VS.! index)
  belowThreshold waveFile threshold =
    case samplesOf waveFile of
      OfWord8 v -> belowdBFS threshold . (v VS.!)
      OfInt16 v -> belowdBFS threshold . (v VS.!)
      OfInt24 v -> belowdBFS threshold . (v VS.!)
      OfInt32 v -> belowdBFS threshold . (v VS.!)
      OfFloat v -> belowdBFS threshold . (v VS.!)

populate
  :: (AudioSample s, Monad m, VS.Storable s)
  => (VS.Vector s -> m ()) -> Segment -> VS.Vector s -> m ()
populate f (Clip s l) source = f (VS.slice s l source)
populate f (InsertedGap l) _ = f (VS.replicate l zeroValue)
populate f (ReferenceGap s l o) source = f $
  if l <= o
  then VS.slice s l source
  else VS.slice s o source VS.++ VS.replicate (l-o) zeroValue


--------------------------------
-- Exceptions
--------------------------------

data WaveFileContentsException
  = ErrorReadingContents
  | FileCutOffUnexpectedly
  | IOErrorReadingContents !IOException
  deriving (Show)

instance Exception WaveFileContentsException

idWaveFileContentsException :: SomeException -> Maybe WaveFileContentsException
idWaveFileContentsException ex =
  fromException ex :: Maybe WaveFileContentsException

fromContentsIOException :: SomeException -> Maybe WaveFileContentsException
fromContentsIOException ex =
  case fromException ex :: Maybe IOException of
    Just e  -> Just (IOErrorReadingContents e)
    Nothing -> Nothing


--------------------------------
-- File Reading & Writing
--------------------------------

type Reporter = Float -> IO ()

{-
 - The following few functions have been optimized as follows:
 -
 - The `hGetContents` was used to lazily load data chunks (32k from the docs)
 - into memory in the background (which will hopefully be loaded into the
 - L2 cache) for faster reading of the data file.
 - The `unsafeFreeze` function was used to avoid reallocating/copying the
 - vector, instead doing an in-place freeze.
 - Multiple samples are extracted from the lazily-loaded byte string at a time,
 - which really speeds things up.
 - `readSamplesChunkSize` was roughly tuned (no actual benchmarks were taken).
 -}

type VectorWriter a = VSM.IOVector a -> IO ()

readWaveFileContents :: FilePath -> Reporter -> IO WaveFile
readWaveFileContents path report = do
  header <- readWaveFileHeader path
  let len = fileSizeOf header
  let offset = offsetOfDataInFileOf header
  let sampleType = waveSampleTypeOf header
  r <- try $ withBinaryFile path ReadMode $ \handle -> do
    hSeek handle AbsoluteSeek offset
    bs <- BSL.hGetContents handle
    case sampleType of
      Word8NE -> OfWord8 <$> intoVectors len (readSamples bs report 0)
      Int16LE -> OfInt16 <$> intoVectors len (readSamples bs report 0)
      Int24LE -> OfInt24 <$> intoVectors len (readSamples bs report 0)
      Int32LE -> OfInt32 <$> intoVectors len (readSamples bs report 0)
      FloatLE -> OfFloat <$> intoVectors len (readSamples bs report 0)
  case r of
    Right v -> return (WaveFile header v)
    Left ex -> throwIO . head $
      catMaybes
        [ idWaveFileContentsException ex
        , fromContentsIOException ex
        , Just ErrorReadingContents
        ]

intoVectors
  :: (VS.Storable a, WaveSample a)
  => Int -> VectorWriter a -> IO (VS.Vector a)
intoVectors len write =
  if len == 0
  then return VS.empty
  else do
    -- does creation need exception handling?
    vec <- VSM.new len
    write vec
    -- use `unsafeFreeze` here since we're not going to use the vectors further
    VS.unsafeFreeze vec

readSamplesChunkSize = 512

getters :: forall a . (WaveSample a) => Get [a]
getters = replicateM readSamplesChunkSize (getter @a)

-- Note to self:
-- requires -XScopedTypeVariables, -XTypeApplications, and `forall a`
-- to use `@a` here
readSamples
  :: forall a . (VS.Storable a, WaveSample a)
  => BSL.ByteString -> Reporter -> Int -> VectorWriter a
readSamples bs report startingIndex vec = do
  let indices = [startingIndex..]
  let startingIndex' = startingIndex + readSamplesChunkSize
  let len = VSM.length vec
  if startingIndex' < len
  then
    case runGetOrFail getters bs of
      Right (bs', _, samples) -> do
        zipWithM_ (VSM.write vec) indices samples
        report (fromIntegral startingIndex' / fromIntegral len)
        readSamples bs' report startingIndex' vec
      _ -> throwIO FileCutOffUnexpectedly
  else
    let getters = replicateM (VSM.length vec - startingIndex) (getter @a) in
    case runGetOrFail getters bs of
      Right (_, _, samples) -> do
        zipWithM_ (VSM.write vec) indices samples
        report 1
      _ -> throwIO FileCutOffUnexpectedly


data WriterContext
  = WriterContext
    { handleOf :: !Handle
    , reporterOf :: !Reporter
    , totalOf :: !SampleCount
    , waveFileOf :: !WaveFile
    }

writeWaveFileContents :: FilePath -> WaveFile -> Segments -> Reporter -> IO ()
writeWaveFileContents path waveFile segments report =
  writeWithWaveHeader (headerOf waveFile) path $ \h -> do
    let total = numSamplesIn segments
    let context = WriterContext h report total waveFile
    writeSegments context 0 segments

writeSegments :: WriterContext -> SampleCount -> Segments -> IO ()
writeSegments c@(WriterContext h report total wf) accum segments =
  when (numSegments segments > 0) $ do
    let segment = segmentAt 0 segments
    let accum' = accum + lengthOf segment
    BSL.hPut h $ runPut $
      case samplesOf wf of
        OfWord8 _ -> fillWith wf segment Word8Content (VS.mapM_ putter)
        OfInt16 _ -> fillWith wf segment Int16Content (VS.mapM_ putter)
        OfInt24 _ -> fillWith wf segment Int24Content (VS.mapM_ putter)
        OfInt32 _ -> fillWith wf segment Int32Content (VS.mapM_ putter)
        OfFloat _ -> fillWith wf segment FloatContent (VS.mapM_ putter)
    report (fromIntegral accum' / fromIntegral total)
    writeSegments c accum' (dropSegments 1 segments)
