{-# LANGUAGE GADTs #-}

module Audio.AudioData
 ( ExpectedContent(..)
 , AudioData(..)
 ) where

import Data.Int
import qualified Data.Vector.Storable as VS
import Data.Word

import Audio.AudioSample
import Audio.Decibel
import Audio.Segment


data ExpectedContent a where
  Word8Content :: ExpectedContent Word8
  Int16Content :: ExpectedContent Int16
  Int24Content :: ExpectedContent Int24
  Int32Content :: ExpectedContent Int32
  FloatContent :: ExpectedContent Float


class AudioData a where
  numberOfSamplesIn :: a -> SampleCount
  sampleRateOf :: a -> Int
  sampleTypeOf :: a -> SampleType
  fillWith
    :: (Monad m, VS.Storable s)
    => a -> Segment -> ExpectedContent s -> (VS.Vector s -> m ()) -> m ()
  dBFSAt :: SampleIndex -> a -> Decibel
  belowThreshold :: a -> Float -> SampleIndex -> Bool
