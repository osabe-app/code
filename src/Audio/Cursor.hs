module Audio.Cursor
 ( Cursor
 --
 , cursorAtFirstSample
 , cursorAtLastSample
 , makeCursorSpanning
 --
 , reformSampleOffset
 , moveToBeginningOfSegment
 , moveToEndOfSegment
 , moveToEndOfPreviousSegment
 , moveToBeginningOfNextSegment
 , retreatCursor
 , advanceCursor
 , retreatCursorInBounds
 , advanceCursorInBounds
 --
 , pointToSameSegment
 , numSamplesBetween
 --
 , order
 , arrangeForRemoval
 --
 , segmentAtCursor
 , segmentsSpanning
 , getExactSegments
 , insertAfter
 , replaceSegments
 ) where

import Control.Monad.Reader as R
import Data.Either
import Data.Foldable as F
import Data.Maybe
import qualified Data.Vector as V

import Audio.AudioSample
import Audio.Segment
import Audio.Segments


--------------------------------
-- cursor
--------------------------------

data Cursor
  = Cursor
    { segmentIndexOf :: !SegmentIndex
    , sampleOffsetOf :: !(Maybe SampleOffset)
    }
  deriving (Eq, Show)

instance Ord Cursor where
  compare l r
    | c /= EQ   = c
    | otherwise = o
    where
      c = compare (segmentIndexOf l) (segmentIndexOf r)
      o = compare (sampleOffsetOf l) (sampleOffsetOf r)


--------------------------------
-- cursor creation
--------------------------------

cursorAtFirstSample :: Segments -> Cursor
cursorAtFirstSample segments =
  Cursor 0 . offsetOfFirstSampleIn $ segmentAt 0 segments

cursorAtLastSample :: Segments -> Cursor
cursorAtLastSample segments =
  let i = numSegments segments - 1 in
  Cursor i . offsetOfLastSampleIn $ segmentAt i segments

makeCursorSpanning :: SegmentCount -> Cursor -> Segments -> Cursor
makeCursorSpanning num start segments =
  let move = (>>= flip moveToBeginningOfNextSegment segments)
      moves = foldl1 (.) (replicate (num-1) move)
  in if num == 1 then start else fromJust $ moves (Just start)


--------------------------------
-- cursor manipulation
--------------------------------

reformSampleOffset :: Cursor -> Segments -> Cursor
reformSampleOffset cursor segments =
  let segment = segmentAtCursor cursor segments
  in case (sampleOffsetOf cursor, lengthOf segment) of
    (Nothing, 0) -> cursor { sampleOffsetOf = Nothing }
    (Just _,  0) -> cursor { sampleOffsetOf = Nothing }
    (Nothing, _) -> cursor { sampleOffsetOf = Just 0 }
    (Just _,  _) -> cursor

moveToBeginningOfSegment :: Cursor -> Segments -> Cursor
moveToBeginningOfSegment cursor segments =
  let segment = segmentAtCursor cursor segments
  in cursor { sampleOffsetOf = offsetOfFirstSampleIn segment }

moveToEndOfSegment :: Cursor -> Segments -> Cursor
moveToEndOfSegment cursor segments =
  let segment = segmentAtCursor cursor segments
  in cursor { sampleOffsetOf = offsetOfLastSampleIn segment }

moveToEndOfPreviousSegment :: Cursor -> Segments -> Maybe Cursor
moveToEndOfPreviousSegment (Cursor index _) segments =
  let index' = index - 1 in
  if index' > -1
  then Just (Cursor index' $ offsetOfLastSampleIn (segmentAt index' segments))
  else Nothing

moveToBeginningOfNextSegment :: Cursor -> Segments -> Maybe Cursor
moveToBeginningOfNextSegment (Cursor index _) segments =
  let index' = index + 1 in
  if index' < numSegments segments
  then Just (Cursor index' $ offsetOfFirstSampleIn (segmentAt index' segments))
  else Nothing

retreatCursor :: SampleOffset -> Cursor -> Segments -> Maybe Cursor
retreatCursor offset cursor segments =
  let moved = moveToEndOfPreviousSegment cursor segments
      continueMoving = retreatCursor
  in case sampleOffsetOf cursor of
    Nothing -> moved >>= flip (continueMoving offset) segments
    Just s -> if offset == 0 then Just cursor else
        if condition
        then moved >>= flip (continueMoving (offset'-1)) segments
        else Just (cursor { sampleOffsetOf = Just diff })
        where
          condition = signum diff < 0
          offset' = abs diff
          diff = s - offset

advanceCursor :: SampleOffset -> Cursor -> Segments -> Maybe Cursor
advanceCursor offset cursor segments =
  let moved = moveToBeginningOfNextSegment cursor segments
      continueMoving = advanceCursor
  in case sampleOffsetOf cursor of
    Nothing -> moved >>= flip (continueMoving offset) segments
    Just s -> if offset == 0 then Just cursor else
        if condition
        then moved >>= flip (continueMoving (offset'-1)) segments
        else Just (cursor { sampleOffsetOf = Just diff })
        where
          samplesLeftInSegment = lengthOf (segmentAtCursor cursor segments) - 1 - s
          condition = offset > samplesLeftInSegment
          offset' = offset - samplesLeftInSegment
          diff = s + offset

retreatCursorInBounds :: SampleOffset -> Cursor -> Segments -> Cursor
retreatCursorInBounds numSamples cursor segments =
  fromMaybe (cursorAtFirstSample segments) (retreatCursor numSamples cursor segments)

advanceCursorInBounds :: SampleOffset -> Cursor -> Segments -> Cursor
advanceCursorInBounds numSamples cursor segments =
  fromMaybe (cursorAtLastSample segments) (advanceCursor numSamples cursor segments)


--------------------------------
-- cursor measurements
--------------------------------

pointToSameSegment :: Cursor -> Cursor -> Bool
pointToSameSegment (Cursor index1 _) (Cursor index2 _) = index1 == index2

numSamplesBetween :: Cursor -> Cursor -> Segments -> SampleCount
numSamplesBetween cursor1 cursor2 segments =
  if pointToSameSegment start end
  then endingSamples - startingSamples
  else endingSamples - startingSamples + numSamplesIn span
  where
    (start, end) = order cursor1 cursor2
    startingSamples = fromMaybe 0 (sampleOffsetOf start)
    endingSamples = fromMaybe 0 (sampleOffsetOf end)
    end' = fromJust $ moveToEndOfPreviousSegment end segments
    span = segmentsSpanning start end' segments


--------------------------------
-- cursor arrangement
--------------------------------

order :: Cursor -> Cursor -> (Cursor, Cursor)
order c1 c2 = if c1 < c2 then (c1, c2) else (c2, c1)

-- cursors should not be pointing to the same gap
arrangeForRemoval :: Cursor -> Cursor -> Segments -> (Cursor, Cursor)
arrangeForRemoval c1 c2 segments =
  (from', to')
  where
    (from, to) = order c1 c2
    fromSegment = segmentAtCursor from segments
    toSegment = segmentAtCursor to segments
    from' =
      if isClip fromSegment
      then from
      else fromJust (moveToBeginningOfNextSegment from segments)
    to' =
      if isClip toSegment
      then fromJust (moveToBeginningOfNextSegment to segments)
      else to


--------------------------------
-- segment stuff
--------------------------------

segmentAtCursor :: Cursor -> Segments -> Segment
segmentAtCursor (Cursor index _) = segmentAt index

segmentsSpanning :: Cursor -> Cursor -> Segments -> Segments
segmentsSpanning (Cursor from _) (Cursor to _) = sliceSegments from (to+1-from)

getExactSegments :: Either Int Int -> Cursor -> Segments -> Segments
getExactSegments fit from segments =
  let numSamples = either id id fit
      to = fromMaybe (cursorAtLastSample segments) (advanceCursor numSamples from segments)
      span = segmentsSpanning from to segments
      spanLength = numSamplesIn span
      frontToTrim = fromMaybe 0 (sampleOffsetOf from)
      availableLength = spanLength - frontToTrim
      modFirstSegment = trimBeginning frontToTrim
      modLastSegment
       | availableLength > numSamples = trimEnd (availableLength - numSamples)
       | isRight fit = expandGap (numSamples - availableLength)
       | otherwise = id
  in modifyFirstAndLast span modFirstSegment modLastSegment

insertAfter :: Cursor -> Segments -> Segments -> Segments
insertAfter cursor insert segments =
  if pointToSameSegment cursor (cursorAtLastSample segments)
  then concatSegments [segments, insert]
  -- yes, cursor' > cursor and they're out of order here as arguments,
  -- but they're supposed to be that way.
  else replaceSegments cursor' cursor insert segments
  where
    cursor' = fromJust (moveToBeginningOfNextSegment cursor segments)

replaceSegments :: Cursor -> Cursor -> Segments -> Segments -> Segments
replaceSegments (Cursor i1 _) (Cursor i2 _) replacements segments =
  concatSegments
  [ takeSegments i1 segments
  , replacements
  , dropSegments (i2+1) segments
  ]
