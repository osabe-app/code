{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}

module Audio.Segment
 ( SampleCount
 , SampleIndex
 , SampleOffset
 , Segment(..)
 , makeGap
 , isClip
 , isGap
 , offsetOfFirstSampleIn
 , offsetOfLastSampleIn
 , trimBeginning
 , trimEnd
 , splitSegment
 , expandGap
 ) where

import Control.DeepSeq
import GHC.Generics (Generic)


type SampleCount = Int
type SampleIndex = Int
type SampleOffset = Int

data Segment
  = Clip
    { startOf :: !SampleIndex
    , lengthOf :: !SampleCount
    }
  | InsertedGap
    { lengthOf :: !SampleCount
    }
  | ReferenceGap
    { startOf :: !SampleIndex
    , lengthOf :: !SampleCount
    , originalLengthOf :: !SampleCount
    }
  deriving (Eq, Generic, NFData, Ord, Read, Show)


makeGap = InsertedGap

isClip :: Segment -> Bool
isClip Clip{} = True
isClip _ = False

isGap :: Segment -> Bool
isGap = not . isClip

offsetOfFirstSampleIn :: Segment -> Maybe SampleOffset
offsetOfFirstSampleIn segment =
  if lengthOf segment == 0 then Nothing else Just 0

offsetOfLastSampleIn :: Segment -> Maybe SampleOffset
offsetOfLastSampleIn segment =
  if lengthOf segment == 0 then Nothing else Just (lengthOf segment - 1)

trimBeginning :: SampleCount -> Segment -> Segment
trimBeginning amount = \case
  Clip s l           -> Clip (s+amount) (l-amount)
  InsertedGap l      -> InsertedGap (l-amount)
  ReferenceGap s l o ->
    if o <= amount
    then InsertedGap (l-amount)
    else ReferenceGap (s+amount) (l-amount) (o-amount)

trimEnd :: SampleCount -> Segment -> Segment
trimEnd amount = \case
  Clip s l           -> Clip s (l-amount)
  InsertedGap l      -> InsertedGap (l-amount)
  ReferenceGap s l o -> ReferenceGap s (l-amount) o

splitSegment :: SampleOffset -> Segment -> Maybe (Segment, Segment)
splitSegment offset segment =
  if offset <= 0 || offset >= lengthOf segment
  then Nothing
  else Just
       ( trimEnd (lengthOf segment - offset) segment
       , trimBeginning offset segment
       ) where offset' = max 1 $ min (lengthOf segment - 1) offset

expandGap :: SampleCount -> Segment -> Segment
expandGap _ c@Clip{} = c
expandGap amount gap = gap { lengthOf = max 0 $ lengthOf gap + amount }
