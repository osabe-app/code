{-# OPTIONS_GHC -Wno-overflowed-literals #-}

{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Audio.Int24
 ( Int24(..)
 , getInt24le
 , putInt24le
 ) where

import Data.Binary.Get
import Data.Binary.Put
import Data.Bits
import Data.Int
import Data.Word
import Foreign.Storable


-- Another way to store 24-bits in a 32-bit container:
-- https://stackoverflow.com/questions/24151973/reading-24-bit-samples-from-a-wav-file
-- this reminded me I needed to extend the sign bit in the container when appropriate


newtype Int24 = I24 { unI24 :: Int32 }
  deriving (Enum, Eq, Integral, Num, Ord, Real, Storable)


getInt24le :: Get Int24
getInt24le = do
  l <- getWord8
  m <- getWord8
  u <- getWord8
  let l' = (fromIntegral l :: Int32)
  let m' = (fromIntegral m :: Int32) `shiftL` 8
  let u' = (fromIntegral u :: Int32) `shiftL` 16
  let us = if u .&. 0x80 == 0 then 0x00000000 else 0xFF000000
  let i24 = us .|. u' .|. m' .|. l'
  return (I24 i24)

putInt24le :: Int24 -> Put
putInt24le (I24 i24) = do
  let l = fromIntegral i24             :: Word8
  let m = fromIntegral (shiftR i24 8)  :: Word8
  let u = fromIntegral (shiftR i24 16) :: Word8
  putWord8 l
  putWord8 m
  putWord8 u
