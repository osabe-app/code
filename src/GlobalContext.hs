module GlobalContext
 ( GlobalContext(..)
 ) where

import Config
import Graphics.Context


data GlobalContext
  = GlobalContext
    { programNameOf :: String
    , configOf :: Config
    , graphicsContextOf :: GraphicsContext
    }

instance HasGraphicsContext GlobalContext where
  getGraphicsContext = graphicsContextOf

instance HasProgramName GlobalContext where
  getProgramName = programNameOf

instance HasSettings GlobalContext where
  getSettings = getSettings . configOf

instance HasStyle GlobalContext where
  getStyle = getStyle . configOf
  updateStyle s c = c { configOf = updateStyle s (configOf c) }

