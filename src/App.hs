{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module App
 ( App
 ) where

import Prelude hiding (read)
import Control.Concurrent
import qualified Control.Concurrent.Async as A
import Control.Concurrent.MVar
import Control.Monad.IO.Unlift
import Control.Monad.Reader
import Data.IORef

import Audio
import Control.Concurrent.Container
import Control.Concurrent.Operation
import GlobalContext


type App = ReaderT GlobalContext IO

instance AudioLoader App where
  readAudio path report = do
    UnliftIO runInBase <- askUnliftIO
    liftIO $ readAudioFile path (runInBase . report)
  saveAudio audioFile segments report = do
    UnliftIO runInBase <- askUnliftIO
    liftIO $ saveAudioFile audioFile segments (runInBase . report)

instance Container App MVar where
  create = liftIO . create
  read = liftIO . read
  update a f = liftIO (update a f)

data IORunState r
  = IORunState
    { asyncOf :: A.Async r
    , progressOf :: IORef Float
    }

instance Operation App IORunState where
  start op = do
    ioRef <- liftIO (newIORef 0)
    let report = liftIO . writeIORef ioRef
    UnliftIO runInBase <- askUnliftIO
    -- libs we're using appear to need `forkOS` to be used, which `asyncBound` uses
    async <- liftIO (A.asyncBound (runInBase (op report)))
    return (IORunState async ioRef)
  cancel = liftIO . A.cancel . asyncOf
  poll (IORunState a r) = do
    status <- liftIO (A.poll a)
    case status of
      Nothing -> do
        p <- liftIO (readIORef r)
        return (Progressing p)
      Just (Left e) ->
        return (Errored e)
      Just (Right v) ->
        return (Completed v)

