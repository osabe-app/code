{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE Rank2Types #-}

module Graphics.Context
 ( GraphicsContext(..)
 , initializeGraphicsContext
 , destroyGraphicsContext
 , rebuildWithStyle
 , buildPathTEXTure
 , freePathTEXTure
 , windowDims
 --
 , HasGraphicsContext(..)
 , getRenderer
 , getWindowDims
 , setWindowTitle
 , readHighDPIFlag
 , updateHighDPIFlag
 , getTextures
 , getTexture
 , getLowDPITextureDims
 , displayTEXT
 , widthOfTEXT
 , lowDPIWidthOfTEXT
 , lowDPIHeightOfTEXT
 ) where

import Control.Monad.Reader
import Control.Monad.State.Strict
import Data.IORef
import Data.List
import qualified Data.Map as Map
import qualified Data.Text as Text
import qualified Data.StateVar as SV
import SDL hiding (get)
import qualified SDL
import qualified SDL.Font as Font
import qualified SDL.Image as Image

import Config
import Graphics.Assets
import Graphics.SDLShim
import Primitive.Geometry as UI


data GraphicsContext
  = GraphicsContext
    { assetsOf :: !Assets
    , highDPIOf :: !(IORef Bool)
    , rendererOf :: !Renderer
    , windowOf :: !Window
    }


initializeGraphicsContext :: (MonadIO m) => Style -> String -> m GraphicsContext
initializeGraphicsContext style programName = do
  initialize [InitAudio,InitVideo,InitEvents]
  Font.initialize
  Image.initialize [Image.InitPNG]
  let windowProperties = defaultWindow { windowResizable = True, windowHighDPI = True }
  window <- createWindow (Text.pack programName) windowProperties
  windowMinimumSize window $= V2 400 300
  renderer <- createRenderer window (-1) defaultRenderer
  rendererDrawBlendMode renderer $= BlendAlphaBlend
  assets <- buildAssets style renderer
  dpiFlag <- liftIO $ newIORef False
  useAssetsIconIn window assets
  return GraphicsContext
    { assetsOf = assets
    , highDPIOf = dpiFlag
    , rendererOf = renderer
    , windowOf = window
    }

destroyGraphicsContext :: (MonadIO m) => GraphicsContext -> m ()
destroyGraphicsContext context = do
  destroyRenderer (rendererOf context)
  destroyWindow (windowOf context)
  destroyAssets (assetsOf context)
  Font.quit
  Image.quit
  quit

rebuildWithStyle :: (MonadIO m) => Style -> GraphicsContext -> m GraphicsContext
rebuildWithStyle style context = do
  destroyAssets (assetsOf context)
  assets <- buildAssets style (rendererOf context)
  return context { assetsOf = assets }

buildPathTEXTure :: (MonadIO m) => FilePath -> Style -> GraphicsContext -> m GraphicsContext
buildPathTEXTure path style context = do
  let definition =
        TEXTureDefinition
        { keyOf = LoadScreenFilePath
        , textOf = path
        , fontOf = LoadScreen
        , foregroundColorOf = loadScreenTextColorOf style
        , backgroundColorOf = Just $ loadScreenBackgroundColorOf style
        }
  assets' <- buildTEXTureAsset (rendererOf context) definition (assetsOf context)
  return context { assetsOf = assets' }

freePathTEXTure :: (MonadIO m) => GraphicsContext -> m ()
freePathTEXTure context =
  void $ freeTEXTureAsset (rendererOf context) LoadScreenFilePath (assetsOf context)

windowDims :: (MonadIO m) => GraphicsContext -> m Dimensions
windowDims = getDimensions . windowOf


--------------------------------
-- Reader Operations
--------------------------------

class HasGraphicsContext a where
  getGraphicsContext :: a -> GraphicsContext


getRenderer
  :: (MonadReader r m, HasGraphicsContext r)
  => m Renderer
getRenderer = asks (rendererOf . getGraphicsContext)

getWindowDims
  :: (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => m Dimensions
getWindowDims = asks getGraphicsContext >>= windowDims

setWindowTitle
  :: (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => String -> m ()
setWindowTitle title = do
  titleVar <- asks (windowTitle . windowOf . getGraphicsContext)
  titleVar $= Text.pack title

readHighDPIFlag
  :: (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => m Bool
readHighDPIFlag = do
  dpiFlag <- asks (highDPIOf . getGraphicsContext)
  liftIO $ readIORef dpiFlag

updateHighDPIFlag
  :: (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => m ()
updateHighDPIFlag = do
  graphics <- asks getGraphicsContext
  let dpiFlag = highDPIOf graphics
  let window = windowOf graphics
  let renderer = rendererOf graphics
  ws <- SDL.get (windowSize window)
  vp <- SDL.get (rendererViewport renderer)
  case vp of
    Nothing -> liftIO $ writeIORef dpiFlag False
    Just (SDL.Rectangle _ dims) -> liftIO $ writeIORef dpiFlag $ ws /= dims

getTextures
  :: (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => m Textures
getTextures = do
  highDPI <- readHighDPIFlag
  asks (getDPITextures highDPI . assetsOf . getGraphicsContext)

getTexture
  :: (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => TextureName -> m Texture
getTexture name = (Map.! name) <$> getTextures

getLowDPITextureDims
  :: (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => TextureName -> m Dimensions
getLowDPITextureDims name = do
  texture <- asks ((Map.! name) . getDPITextures False . assetsOf . getGraphicsContext)
  getDimensions texture

displayTEXT
  :: (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => Renderer -> UI.Coordinates -> TEXT -> m ()
displayTEXT renderer coords text = do
  textures <- getTextures
  displayTEXTures renderer coords textures text

widthOfTEXT
  :: (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => TEXT -> m Int
widthOfTEXT text = do
  textures <- getTextures
  widthOfTEXTures textures text

lowDPIWidthOfTEXT
  :: (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => TEXT -> m Int
lowDPIWidthOfTEXT text = do
  textures <- asks (getDPITextures False . assetsOf . getGraphicsContext)
  widthOfTEXTures textures text

lowDPIHeightOfTEXT
  :: (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => TEXT -> m Int
lowDPIHeightOfTEXT text = do
  textures <- asks (getDPITextures False . assetsOf . getGraphicsContext)
  heightOfTEXTures textures text

