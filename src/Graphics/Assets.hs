{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}

module Graphics.Assets
 ( Assets
 , buildAssets
 , destroyAssets
 , getDPITextures
 , buildTEXTureAsset
 , freeTEXTureAsset
 , FontUsage(..)
 , useAssetsIconIn
 , Textures
 , TextureName(..)
 , TEXTureDefinition(..)
 , TEXT
 , updateTEXTure
 , displayTEXTures
 , widthOfTEXTures
 , heightOfTEXTures
 ) where

import Control.DeepSeq
import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.Function
import Data.List
import qualified Data.Map as M
import qualified Data.Text as T
import GHC.Generics (Generic)
import SDL
import SDL.Font as Font hiding (Color)
import SDL.Image as Image
import SDL.Internal.Types
import qualified SDL.Raw.Types as Raw
import qualified SDL.Raw.Video as Raw
import System.Directory
import System.Environment
import System.FilePath
import System.Info

import Audio
import Config as C
import Graphics.SDLShim
import Primitive.Color
import Primitive.Geometry as UI


data Assets
  = Assets
    { lowDPIFontsOf :: !Fonts
    , highDPIFontsOf :: !Fonts
    , iconOf :: !Surface
    , lowDPITexturesOf :: !Textures
    , highDPITexturesOf :: !Textures
    }


getAssetsDir :: IO FilePath
getAssetsDir = do
  let assetsDirName = "assets"
  exePath <- getExecutablePath
  let exeName = (takeFileName . dropExtension) exePath
  if "-exe" `isSuffixOf` exeName
  then return assetsDirName -- dev env, use path relative to current dir
  else return $ takeDirectory exePath
          </> (if os == "darwin" then "../Resources" else "")
          </> assetsDirName

buildAssets :: (MonadIO m) => C.Style -> Renderer -> m Assets
buildAssets style renderer = do
  ldFonts <- loadLowDPIFonts
  hdFonts <- loadHighDPIFonts
  --
  icon <- loadIcon
  --
  let textDefinitions = concat
        [ loadScreenTEXTureDefinitions
            (loadScreenTextColorOf style)
            (Just $ loadScreenBackgroundColorOf style)
        , statusBarTEXTureDefinitions
            (editScreenStatusBarTextColorOf style)
            (Just $ editScreenStatusBarBackgroundColorOf style)
        , axisTEXTureDefinitions
            (editScreenAxisTextColorOf style)
            (Just $ editScreenAxisBackgroundColorOf style)
        ]
  ldTextures <- loadLowDPITextures renderer
  ldTextures' <- foldM (createTEXTure renderer ldFonts) ldTextures textDefinitions
  hdTextures <- loadHighDPITextures renderer
  hdTextures' <- foldM (createTEXTure renderer hdFonts) hdTextures textDefinitions
  return (Assets ldFonts hdFonts icon ldTextures' hdTextures')

destroyAssets :: (MonadIO m) => Assets -> m ()
destroyAssets assets = do
  destroyFonts (lowDPIFontsOf assets)
  destroyFonts (highDPIFontsOf assets)
  freeSurface (iconOf assets)
  destroyTextures (lowDPITexturesOf assets)
  destroyTextures (highDPITexturesOf assets)

getDPITextures :: Bool -> Assets -> Textures
getDPITextures highDPI = if highDPI then highDPITexturesOf else lowDPITexturesOf

buildTEXTureAsset :: (MonadIO m) => Renderer -> TEXTureDefinition -> Assets -> m Assets
buildTEXTureAsset renderer definition (Assets ldf hdf icn ldt hdt) = do
  ldt' <- updateTEXTure renderer ldf ldt definition
  hdt' <- updateTEXTure renderer hdf hdt definition
  return (Assets ldf hdf icn ldt' hdt')

freeTEXTureAsset :: (MonadIO m) => Renderer -> TextureName -> Assets -> m Assets
freeTEXTureAsset renderer name (Assets ldf hdf icn ldt hdt) = do
  destroyTexture (ldt M.! name)
  destroyTexture (hdt M.! name)
  let ldt' = M.delete name ldt
  let hdt' = M.delete name hdt
  return (Assets ldf hdf icn ldt' hdt')


--------------------------------
-- Fonts
--------------------------------

data FontUsage
  = LoadScreen
  | StatusBar
  | Axis
  deriving (Eq, Ord)

type Fonts = M.Map FontUsage Font


loadLowDPIFonts :: (MonadIO m) => m Fonts
loadLowDPIFonts = do
  assetsDir <- liftIO getAssetsDir
  let loads =
        [ loadFont (assetsDir </> "fonts/amble/Amble-Regular.ttf") 14 LoadScreen
        , loadFont (assetsDir </> "fonts/amble/Amble-Bold.ttf") 12 StatusBar
        , loadFont (assetsDir </> "fonts/amble/Amble-Regular.ttf") 10 Axis
        ]
  foldM (&) M.empty loads

loadHighDPIFonts:: (MonadIO m) => m Fonts
loadHighDPIFonts = do
  assetsDir <- liftIO getAssetsDir
  let loads =
        [ loadFont (assetsDir </> "fonts/amble/Amble-Regular.ttf") 28 LoadScreen
        , loadFont (assetsDir </> "fonts/amble/Amble-Bold.ttf") 24 StatusBar
        , loadFont (assetsDir </> "fonts/amble/Amble-Regular.ttf") 20 Axis
        ]
  foldM (&) M.empty loads

loadFont
  :: (MonadIO m)
  => FilePath -> Int -> FontUsage -> Fonts -> m Fonts
loadFont path size name fonts = do
  font <- Font.load path size
  return (M.insert name font fonts)

destroyFonts :: (MonadIO m) => Fonts -> m ()
destroyFonts fonts = mapM_ free (M.elems fonts)


--------------------------------
-- Icon
--------------------------------

loadIcon :: (MonadIO m) => m Surface
loadIcon = do
  assetsDir <- liftIO getAssetsDir
  Image.load $ assetsDir </> "images/icon.png"

useAssetsIconIn window assets =
  let Surface pIcon _ = iconOf assets
      Window pWin = window
  in Raw.setWindowIcon pWin pIcon


--------------------------------
-- Textures
--------------------------------

data TextureName
  = InstructionsScreen
  --
  | LoadScreenLoadingText
  | LoadScreenFilePath
  | LoadScreenFileBarGraphic
  | LoadScreenFileStatusText
  | LoadScreenClipsBarGraphic
  | LoadScreenClipsStatusText
  | LoadScreenVisualsBarGraphic
  | LoadScreenVisualsStatusText
  --
  | AxisText !Char
  --
  | EditScreenConfigIcon
  | EditScreenRedoIcon
  | EditScreenSaveIcon
  | EditScreenUndoIcon
  | EditScreenResizeBarPattern
  | EditScreenStatusBarNameValueSeparator
  | EditScreenStatusBarSampleRateLabel
  | EditScreenStatusBarChar !Char
  | EditScreenStatusBarHertz
  | EditScreenStatusBarSampleTypeLabel
  | EditScreenStatusBarSampleType !SampleType
  | EditScreenStatusBarFileStatusLabel
  | EditScreenStatusBarSaved
  | EditScreenStatusBarSaveErrored
  | EditScreenStatusBarUnsaved
  | EditScreenStatusBarPlaybackStatusLabel
  | EditScreenStatusBarPlaying
  | EditScreenStatusBarStopped
  deriving (Eq, Generic, NFData, Ord)

type Textures = M.Map TextureName Texture


loadLowDPITextures :: (MonadIO m) => Renderer -> m Textures
loadLowDPITextures renderer = do
  assetsDir <- liftIO getAssetsDir
  let loads =
        [ loadImage (assetsDir </> "images/icons/config-icon.png") renderer EditScreenConfigIcon
        , loadImage (assetsDir </> "images/icons/redo-icon.png") renderer EditScreenRedoIcon
        , loadImage (assetsDir </> "images/icons/save-icon.png") renderer EditScreenSaveIcon
        , loadImage (assetsDir </> "images/icons/undo-icon.png") renderer EditScreenUndoIcon
        , loadImage (assetsDir </> "images/instructions/instructions.png") renderer InstructionsScreen
        , loadImage (assetsDir </> "images/loading/file-bar.png") renderer LoadScreenFileBarGraphic
        , loadImage (assetsDir </> "images/loading/clips-bar.png") renderer LoadScreenClipsBarGraphic
        , loadImage (assetsDir </> "images/loading/visuals-bar.png") renderer LoadScreenVisualsBarGraphic
        , loadImage (assetsDir </> "images/resize-bar/resize-bar-pattern.png") renderer EditScreenResizeBarPattern
        ]
  foldM (&) M.empty loads

loadHighDPITextures :: (MonadIO m) => Renderer -> m Textures
loadHighDPITextures renderer = do
  assetsDir <- liftIO getAssetsDir
  let loads =
        -- the Mac convention is to use a suffix of "@2x" for high DPI files
        -- but the WiX installer builder on Windows doesn't like this, so
        -- the suffix of "_at2x" is used instead.
        [ loadImage (assetsDir </> "images/icons/config-icon_at2x.png") renderer EditScreenConfigIcon
        , loadImage (assetsDir </> "images/icons/redo-icon_at2x.png") renderer EditScreenRedoIcon
        , loadImage (assetsDir </> "images/icons/save-icon_at2x.png") renderer EditScreenSaveIcon
        , loadImage (assetsDir </> "images/icons/undo-icon_at2x.png") renderer EditScreenUndoIcon
        , loadImage (assetsDir </> "images/instructions/instructions_at2x.png") renderer InstructionsScreen
        , loadImage (assetsDir </> "images/loading/file-bar_at2x.png") renderer LoadScreenFileBarGraphic
        , loadImage (assetsDir </> "images/loading/clips-bar_at2x.png") renderer LoadScreenClipsBarGraphic
        , loadImage (assetsDir </> "images/loading/visuals-bar_at2x.png") renderer LoadScreenVisualsBarGraphic
        , loadImage (assetsDir </> "images/resize-bar/resize-bar-pattern_at2x.png") renderer EditScreenResizeBarPattern
        ]
  foldM (&) M.empty loads

loadImage
  :: (MonadIO m)
  => FilePath -> Renderer -> TextureName -> Textures -> m Textures
loadImage path renderer name textures = do
  texture <- loadTexture renderer path
  return (M.insert name texture textures)

destroyTextures :: (MonadIO m) => Textures -> m ()
destroyTextures textures = mapM_ destroyTexture (M.elems textures)


--------------------------------
-- TEXTures
--------------------------------

data TEXTureDefinition
  = TEXTureDefinition
    { keyOf :: !TextureName
    , textOf :: !String
    , fontOf :: !FontUsage
    , foregroundColorOf :: !Color
    , backgroundColorOf :: !(Maybe Color)
    }

type TEXT = [TextureName]


createTEXTure
  :: (MonadIO m)
  => Renderer -> Fonts -> Textures -> TEXTureDefinition -> m Textures
createTEXTure renderer fonts textures definition = do
  let font = fonts M.! fontOf definition
  let v4fgColor = toV4 (foregroundColorOf definition)
  let text = T.pack (textOf definition)
  surface <-
    case backgroundColorOf definition of
      Just bgColor -> do
        let v4bgColor = toV4 bgColor
        shaded font v4fgColor v4bgColor text
      Nothing ->
        blended font v4fgColor text
  texture <- createTextureFromSurface renderer surface
  freeSurface surface
  return (M.insert (keyOf definition) texture textures)

updateTEXTure
  :: (MonadIO m)
  => Renderer -> Fonts -> Textures -> TEXTureDefinition -> m Textures
updateTEXTure renderer fonts textures definition = do
  forM_ (M.lookup (keyOf definition) textures) destroyTexture
  createTEXTure renderer fonts textures definition

displayTEXTures
  :: (MonadIO m)
  => Renderer -> UI.Coordinates -> Textures -> TEXT -> m ()
displayTEXTures renderer (UI.Coordinates x y) textures text = do
  let ts = map (textures M.!) text
  dims <- mapM getDimensions ts
  let toPosition accum (UI.Dimensions w _) = (accum+w, UI.Coordinates accum y)
  let (_, positions) = mapAccumL toPosition x dims
  mapM_ (draw renderer) (zipWith Positioned positions ts)

widthOfTEXTures
  :: (MonadIO m)
  => Textures -> TEXT -> m Int
widthOfTEXTures textures text = do
  let ts = map (textures M.!) text
  dims <- mapM getDimensions ts
  return $ foldl (\accum dim -> UI.widthOf dim + accum) 0 dims

heightOfTEXTures
  :: (MonadIO m)
  => Textures -> TEXT -> m Int
heightOfTEXTures textures text = do
  let ts = map (textures M.!) text
  dims <- mapM getDimensions ts
  return (maximum $ map UI.heightOf dims)

loadScreenTEXTureDefinitions :: Color -> Maybe Color -> [TEXTureDefinition]
loadScreenTEXTureDefinitions fgColor bgColor =
  [ TEXTureDefinition
    { keyOf = LoadScreenLoadingText
    , textOf = "Loading file at path:"
    , fontOf = LoadScreen
    , foregroundColorOf = fgColor
    , backgroundColorOf = bgColor
    }
  , TEXTureDefinition
    { keyOf = LoadScreenFileStatusText
    , textOf = "Reading file from disk..."
    , fontOf = LoadScreen
    , foregroundColorOf = fgColor
    , backgroundColorOf = bgColor
    }
  , TEXTureDefinition
    { keyOf = LoadScreenClipsStatusText
    , textOf = "Segmenting into clips..."
    , fontOf = LoadScreen
    , foregroundColorOf = fgColor
    , backgroundColorOf = bgColor
    }
  , TEXTureDefinition
    { keyOf = LoadScreenVisualsStatusText
    , textOf = "Creating visuals..."
    , fontOf = LoadScreen
    , foregroundColorOf = fgColor
    , backgroundColorOf = bgColor
    }
  ]

statusBarTEXTureDefinitions :: Color -> Maybe Color -> [TEXTureDefinition]
statusBarTEXTureDefinitions fgColor bgColor =
  TEXTureDefinition
  { keyOf = EditScreenStatusBarNameValueSeparator
  , textOf = ": "
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  TEXTureDefinition
  { keyOf = EditScreenStatusBarSampleRateLabel
  , textOf = "Sample rate"
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  TEXTureDefinition
  { keyOf = EditScreenStatusBarHertz
  , textOf = "Hz"
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  TEXTureDefinition
  { keyOf = EditScreenStatusBarSampleTypeLabel
  , textOf = "Sample type"
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  TEXTureDefinition
  { keyOf = EditScreenStatusBarSampleType Word8
  , textOf = "8-bit unsigned int"
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  TEXTureDefinition
  { keyOf = EditScreenStatusBarSampleType Int16
  , textOf = "16-bit signed int"
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  TEXTureDefinition
  { keyOf = EditScreenStatusBarSampleType Int24
  , textOf = "24-bit signed int"
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  TEXTureDefinition
  { keyOf = EditScreenStatusBarSampleType Int32
  , textOf = "32-bit signed int"
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  TEXTureDefinition
  { keyOf = EditScreenStatusBarSampleType Float
  , textOf = "32-bit float"
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  TEXTureDefinition
  { keyOf = EditScreenStatusBarFileStatusLabel
  , textOf = "File status"
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  TEXTureDefinition
  { keyOf = EditScreenStatusBarSaved
  , textOf = "Saved"
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  TEXTureDefinition
  { keyOf = EditScreenStatusBarSaveErrored
  , textOf = "Errored"
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  TEXTureDefinition
  { keyOf = EditScreenStatusBarUnsaved
  , textOf = "Unsaved"
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  TEXTureDefinition
  { keyOf = EditScreenStatusBarPlaybackStatusLabel
  , textOf = "Playback"
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  TEXTureDefinition
  { keyOf = EditScreenStatusBarPlaying
  , textOf = "Playing"
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  TEXTureDefinition
  { keyOf = EditScreenStatusBarStopped
  , textOf = "Stopped"
  , fontOf = StatusBar
  , foregroundColorOf = fgColor
  , backgroundColorOf = bgColor
  } :
  map (\c ->
    TEXTureDefinition
    { keyOf = EditScreenStatusBarChar c
    , textOf = [c]
    , fontOf = StatusBar
    , foregroundColorOf = fgColor
    , backgroundColorOf = bgColor
    }) (' ' : '%' : take 10 (enumFrom '0'))

axisTEXTureDefinitions :: Color -> Maybe Color -> [TEXTureDefinition]
axisTEXTureDefinitions fgColor bgColor =
  map (\c ->
    TEXTureDefinition
    { keyOf = AxisText c
    , textOf = [c]
    , fontOf = Axis
    , foregroundColorOf = fgColor
    , backgroundColorOf = bgColor
    }) ('.' : ':' : take 10 (enumFrom '0'))
