{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Graphics.Renderable where

import Control.Monad.IO.Class
import Control.Monad.Reader

import Config.Style
import Graphics.Context
import Primitive.Geometry


class
  ( HasGraphicsContext r
  , HasStyle r
  , MonadIO m
  , MonadReader r m
  ) => CanRender r m where {}
instance
  ( HasGraphicsContext r
  , HasStyle r
  , MonadIO m
  , MonadReader r m
  ) => CanRender r m where {}

class Renderable c where
  render :: (CanRender r m) => c -> Rectangle -> m ()

instance Renderable () where
  render _ _ = return ()


data Rendering = forall c . Renderable c => Rendering c

instance Renderable Rendering where
  render (Rendering c) = render c

