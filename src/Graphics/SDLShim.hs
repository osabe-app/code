{-# LANGUAGE FlexibleInstances #-}

module Graphics.SDLShim where

import Control.Monad
import Control.Monad.IO.Class
import Foreign.C.Types
import qualified Data.Vector.Storable as VS
import qualified Data.Vector.Unboxed as VU
import Data.Word
import SDL

import Primitive.Color
import qualified Primitive.Geometry as UI


--------------------------------
-- Location & Size
--------------------------------

class HasCoordinates a where
  getCoordinates :: MonadIO m => a -> m UI.Coordinates


class HasDimensions a where
  getDimensions :: MonadIO m => a -> m UI.Dimensions

instance HasDimensions Window where
  getDimensions window = do
    V2 cWidth cHeight <- get (windowSize window)
    return UI.Dimensions
      { UI.widthOf = fromIntegral cWidth
      , UI.heightOf = fromIntegral cHeight
      }

instance HasDimensions Texture where
  getDimensions texture = do
    textureInfo <- queryTexture texture
    return UI.Dimensions
      { UI.widthOf = fromIntegral (textureWidth textureInfo)
      , UI.heightOf = fromIntegral (textureHeight textureInfo)
      }

instance HasDimensions UI.Rectangle where
  getDimensions = return . UI.dimensionsOf


data Positioned a
  = Positioned
    { positionOf :: !UI.Coordinates
    , itemOf :: !a
    }

instance HasCoordinates (Positioned a) where
  getCoordinates = return . positionOf

positionAtOrigin :: MonadIO m => w -> a -> m (Positioned a)
positionAtOrigin window item = return (Positioned UI.origin item)

positionInCenter
  :: (HasDimensions w, HasDimensions a, MonadIO m)
  => w -> a -> m (Positioned a)
positionInCenter window item = do
  UI.Dimensions winWidth winHeight <- getDimensions window
  UI.Dimensions itemWidth itemHeight <- getDimensions item
  let x = max 0 ((winWidth - itemWidth) `div` 2)
  let y = max 0 ((winHeight - itemHeight) `div` 2)
  return (Positioned (UI.Coordinates x y) item)

moveBy :: (Int, Int) -> Positioned a -> Positioned a
moveBy (dx, dy) (Positioned (UI.Coordinates x y) item) =
  Positioned (UI.Coordinates (x+dx) (y+dy)) item


--------------------------------
-- Primitives to SDL
--------------------------------

class ToV4 a where
  toV4 :: a -> V4 Word8

instance ToV4 Color where
  toV4 (Color r g b a) = V4 r g b a
  {-# INLINE toV4 #-}


class ToV2 a where
  toV2 :: a -> V2 CInt

instance Integral a => ToV2 (a, a) where
  toV2 (a, b) = V2 (fromIntegral a) (fromIntegral b)
  {-# INLINE toV2 #-}

instance ToV2 UI.Coordinates where
  toV2 (UI.Coordinates x y) = V2 (fromIntegral x) (fromIntegral y)
  {-# INLINE toV2 #-}

instance ToV2 UI.Dimensions where
  toV2 (UI.Dimensions w h) = V2 (fromIntegral w) (fromIntegral h)
  {-# INLINE toV2 #-}


toSdlPoint :: (ToV2 a) => a -> Point V2 CInt
toSdlPoint = P . toV2

toSdlLine :: UI.Line -> (Point V2 CInt, Point V2 CInt)
toSdlLine (UI.Line c1 c2) = (P (toV2 c1), P (toV2 c2))
{-# INLINE toSdlLine #-}

toSdlRect :: UI.Rectangle -> SDL.Rectangle CInt
toSdlRect (UI.Rectangle c d) = makeSdlRect c d
{-# INLINE toSdlRect #-}

fromSdlRect :: SDL.Rectangle CInt -> UI.Rectangle
fromSdlRect (Rectangle (P (V2 x y)) (V2 w h)) =
  UI.Rectangle
    (UI.Coordinates (fromIntegral x) (fromIntegral y))
    (UI.Dimensions (fromIntegral w) (fromIntegral h))
{-# INLINE fromSdlRect #-}

makeSdlRect :: (ToV2 c, ToV2 d) => c -> d -> SDL.Rectangle CInt
makeSdlRect coords dims = SDL.Rectangle (P (toV2 coords)) (toV2 dims)
{-# INLINE makeSdlRect #-}

toSdlPoly :: VU.Vector UI.Coordinates -> VS.Vector (Point V2 CInt)
toSdlPoly = VS.map toSdlPoint . VS.convert


--------------------------------
-- Drawing
--------------------------------

class Drawable a where
  draw :: MonadIO m => Renderer -> a -> m ()

instance Drawable Color where
  draw renderer color = do
    rendererDrawColor renderer $= toV4 color
    clear renderer

-- Textures

instance Drawable (Positioned Texture) where
  draw renderer (Positioned coords texture) = do
    dimensions <- getDimensions texture
    copy renderer texture Nothing (Just (makeSdlRect coords dimensions))

data Portion
  = Portion
    { boundsOf :: !UI.Rectangle
    , textureOf :: !Texture
    }

instance Drawable (Positioned Portion) where
  draw renderer portion = do
    let Positioned loc (Portion (UI.Rectangle coords dims) texture) = portion
    let src = makeSdlRect coords dims
    let dest = makeSdlRect loc dims
    copy renderer texture (Just src) (Just dest)

-- Primitives

instance Drawable UI.Line where
  draw renderer line = uncurry (drawLine renderer) (toSdlLine line)
  {-# INLINE draw #-}

instance Drawable (V4 Word8, UI.Line) where
  draw renderer (color, line) = do
    rendererDrawColor renderer $= color
    uncurry (drawLine renderer) $ toSdlLine line

instance Drawable (Color, UI.Line) where
  draw renderer (color, line) = draw renderer (toV4 color, line)
  {-# INLINE draw #-}

instance Drawable UI.Polyline where
  draw renderer (UI.Polyline points) = drawLines renderer (toSdlPoly points)
  {-# INLINE draw #-}

instance Drawable (V4 Word8, UI.Polyline) where
  draw renderer (color, UI.Polyline points) = do
    rendererDrawColor renderer $= color
    drawLines renderer (toSdlPoly points)

instance Drawable (Color, UI.Polyline) where
  draw renderer (color, poly) = draw renderer (toV4 color, poly)
  {-# INLINE draw #-}

instance Drawable UI.Rectangle where
  draw renderer (UI.Rectangle coords dims) =
    fillRect renderer (Just (makeSdlRect coords dims))
  {-# INLINE draw #-}

instance Drawable (V4 Word8, UI.Rectangle) where
  draw renderer (color, UI.Rectangle coords dims) = do
    rendererDrawColor renderer $= color
    let rect = makeSdlRect coords dims
    fillRect renderer (Just rect)

instance Drawable (Color, UI.Rectangle) where
  draw renderer (color, rect) = draw renderer (toV4 color, rect)
  {-# INLINE draw #-}


--------------------------------
-- Texture Writing
--------------------------------

createWritableTexture :: MonadIO m => Renderer -> Int -> Int -> m Texture
createWritableTexture renderer width height = do
  let size = toV2 (width, height)
  texture <- createTexture renderer RGBA8888 TextureAccessTarget size
  textureBlendMode texture $= BlendAlphaBlend
  return texture

tilePattern :: MonadIO m => Renderer -> Texture -> UI.Rectangle -> m ()
tilePattern renderer texture rect = do
  UI.Dimensions pw ph <- getDimensions texture
  let (UI.Rectangle (UI.Coordinates x y) (UI.Dimensions w h)) = rect
  let coords =
        [UI.Coordinates x' y'
          | x' <- takeWhile (< x+w) [x, x+pw..]
          , y' <- takeWhile (< y+h) [y, y+ph..]
          ]
  forM_ coords $ \c@(UI.Coordinates x' y') -> do
    let dims = toV2 (UI.Dimensions (min pw $ x+w-x') (min ph $ y+h-y'))
    let src = SDL.Rectangle (P (V2 0 0)) dims
    let dest = SDL.Rectangle (P (toV2 c)) dims
    copy renderer texture (Just src) (Just dest)


--------------------------------
-- Display Rate
--------------------------------

limitFpsTo fps op = do
  ticks1 <- ticks
  op
  ticks2 <- ticks
  let diff = ticks2 - ticks1
  let fpsDelay = 1000 `div` fps
  when (diff < fpsDelay) (delay (fpsDelay - diff))

