module Screens.Instructions
 ( instructionsScreen
 ) where

import Control.Monad.Reader
import Pipes
import Pipes.Core
import Pipes.Lift

import Config
import GlobalContext
import Graphics.Context
import Primitive.Geometry
import Screens.Instructions.Presenter
import Screens.Shared.Output
import Screens.Shared.View


layers
  :: ( HasGraphicsContext r
     , HasProgramName r
     , HasSettings r
     , HasStyle r
     , MonadIO m
     , MonadReader r m
     )
  => Dimensions -> [String] -> Effect m Output
layers windowSize = presenter windowSize >+> view

instructionsScreen :: GlobalContext -> [String] -> IO Output
instructionsScreen globalContext messages = do
  windowSize <- windowDims (getGraphicsContext globalContext)
  runEffect (runReaderP globalContext (layers windowSize messages))

