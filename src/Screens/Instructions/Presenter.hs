{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Instructions.Presenter
 ( presenter
 ) where

import Control.Monad.Reader
import FULE
import Pipes
import Pipes.Core

import Config
import Graphics.Assets
import Graphics.Context
import Graphics.SDLShim
import Primitive.Color
import Primitive.Geometry
import Screens.Shared.Messages hiding (FileDropped)
import Screens.Shared.Output
import Screens.Shared.Presenter.Context
import Screens.Shared.Presenter.Reactor
import Screens.Shared.Presenter.Window
import Screens.Shared.View.Color
import Screens.Shared.View.Texture


class
  ( HasGraphicsContext r
  , HasProgramName r
  , HasSettings r
  , HasStyle r
  , MonadIO m
  , MonadReader r m
  ) => PresenterStateConstraint m r where {}
instance
  ( HasGraphicsContext r
  , HasProgramName r
  , HasSettings r
  , HasStyle r
  , MonadIO m
  , MonadReader r m
  ) => PresenterStateConstraint m r where {}


type MO = ()
type PO = Output
type R = Reactor' (RInput () ()) (ROutput MO PO ())

type PresenterState = Server ViewToPresenter PresenterToView
type PresenterStateWithContext m = PresenterState (WithContext () MO PO () m)


instructionsUI
  :: forall m r . (PresenterStateConstraint m r)
  => Int -> Int -> m (Layout, [ComponentInfo (R m)])
instructionsUI width height = do
  instBgColor :: R m <- asks (reactor' . instructionsScreenBackgroundColorOf . getStyle)
  let instGraphic :: R m = reactor' InstructionsScreen
  layoutM
    (window (width, height) (windowControl windowEventHandler)
      (layered
        ([item instBgColor
        , item $ centered instGraphic
        ]::[ItemM m (R m)])))


presenter
  :: PresenterStateConstraint m r
  => Dimensions -> ViewToPresenter -> PresenterState m Output
presenter (Dimensions width height) input = do
  ui <- lift $ instructionsUI width height
  runWithContext ui $ idle input

idle
  :: PresenterStateConstraint m r
  => ViewToPresenter -> PresenterStateWithContext m Output
idle input = do
  lift $ integrateViewInput input
  output <- lift $ getOutputsForPresenter
  if not (null output)
  then return (head output)
  else do
    programName <- getNameOfProgram
    view <- lift renderViewOutput
    input' <- respond (Display view programName)
    idle input'

