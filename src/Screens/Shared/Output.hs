{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}

module Screens.Shared.Output where

import Control.DeepSeq
import GHC.Generics (Generic)

import Audio
import Screens.Editing.View.Waveform.Representation


data Output
  = FileDropped !FilePath
  | FileLoadErrored !FilePath !String
  | FileLoaded !AudioFile !Segments !WaveformRepresentation
  | CloseRequested
  deriving (Generic, NFData)

