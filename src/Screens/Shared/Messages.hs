{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}

module Screens.Shared.Messages where

import Control.DeepSeq
import GHC.Generics (Generic)

import Graphics.Renderable
import Primitive.Geometry
import Screens.Shared.DialogOption
import Screens.Shared.UserInput


data ViewToPresenter
  = CloseRequested
  | ContentDisplaying !Bool
  | FileDropped !FilePath
  | Interactivity !UserInput
  | PromptResponse !(Maybe Int)
  | WindowResized !Dimensions
  | Render
  deriving (Generic, NFData, Show)

data PresenterToView
  = Display
    { toDrawOf :: [(Rendering, [Rectangle])]
    , windowTitleOf :: String
    }
  | Alert
    { summaryOf :: String
    , detailsOf :: String
    }
  | Notify
    { summaryOf :: String
    , detailsOf :: String
    }
  | Prompt
    { summaryOf :: String
    , detailsOf :: String
    , optionsOf :: DialogOptions
    }

