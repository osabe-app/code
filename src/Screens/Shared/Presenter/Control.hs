{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Screens.Shared.Presenter.Control
 ( Control
 , control
 , noControlReaction
 ) where

import Control.DeepSeq
import FULE
import GHC.Generics (Generic)

import Screens.Shared.Presenter.InputContext
import Screens.Shared.Presenter.Reactor


data Control mo po
  = Control
    { modelOutputOf :: Maybe mo
    , presenterOutputOf :: Maybe po
    , modelOutputGenOf :: InputContext -> Maybe mo
    , presenterOutputGenOf :: InputContext -> Maybe po
    }
  deriving (Generic, NFData)

instance (NFData mo, NFData po)
  => Reaction (Control mo po) (RInput mi vm) (ROutput mo po vm) where
  addReactant (ViewInput vi) _ ctrl@(Control { modelOutputGenOf = mog, presenterOutputGenOf = pog }) =
    ctrl { modelOutputOf = mog vi, presenterOutputOf = pog vi }
  addReactant _ _ ctrl = ctrl
  getProduct _ ModelOutput (Control { modelOutputOf = mo }) = mo
  getProduct _ PresenterOutput (Control { presenterOutputOf = po }) = po
  getProduct _ _ _ = Nothing

control :: (InputContext -> Maybe mo) -> (InputContext -> Maybe po) -> Control mo po
control = Control Nothing Nothing

noControlReaction :: b -> Maybe a
noControlReaction = const Nothing

