{-# LANGUAGE LambdaCase #-}

module Screens.Shared.Presenter.State
 ( State
 , initialState
 , previousState
 , currentState
 , transition
 , transitioning
 , onTransition
 ) where

import Control.DeepSeq


data State s
  = Static !s
  | Transitioning !s !s

instance (NFData s) => NFData (State s) where
  rnf t@(Static s) = seq t . deepseq s $ ()
  rnf t@(Transitioning s1 s2) = seq t . deepseq s1 . deepseq s2 $ ()

initialState :: s -> State s
initialState = Static

previousState :: State s -> Maybe s
previousState = \case
  Static _ -> Nothing
  Transitioning s _ -> Just s

currentState :: State s -> s
currentState = \case
  Static s -> s
  Transitioning _ s -> s

transition :: State s -> Maybe s -> State s
transition (Static s) (Just s') = Transitioning s s'
transition (Transitioning _ s) (Just s') = Transitioning s s'
transition (Transitioning _ s) Nothing = Static s
transition (Static s) Nothing = Static s

transitioning :: State s -> Bool
transitioning = \case
  Transitioning{} -> True
  _ -> False

onTransition :: (s -> s -> Maybe o) -> State s -> Maybe o
onTransition f = \case
  Static _ -> Nothing
  Transitioning s s' -> f s s'

