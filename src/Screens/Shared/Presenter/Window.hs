{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Screens.Shared.Presenter.Window
 ( WindowControl
 , windowControl
 , WindowEvent(..)
 , WindowEventHandler
 , dummyWindowEventHandler
 , windowEventHandler
 ) where

import Control.DeepSeq
import FULE
import GHC.Generics (Generic)

import Primitive.Geometry hiding (dimensionsOf)
import qualified Screens.Shared.Messages as M
import qualified Screens.Shared.Output as O
import Screens.Shared.Presenter.InputContext
import Screens.Shared.Presenter.Reactor


data WindowActivity
  = CloseRequested
  | FileDropped !FilePath
  | WindowResized
    { widthOf :: !Int
    , heightOf :: !Int
    }
  deriving (Generic, NFData, Show)

inputToActivity :: InputContext -> Maybe WindowActivity
inputToActivity = \case
  InputContext { inputOf = M.CloseRequested } -> Just CloseRequested
  InputContext { inputOf = M.FileDropped p } -> Just (FileDropped p)
  InputContext { inputOf = M.WindowResized (Dimensions nw nh) } ->
    Just (WindowResized nw nh)
  input ->
    if isQuitCommand input then Just CloseRequested else Nothing


data WindowEvent
  = CloseRequestEvent
  | FileDropEvent !FilePath
  deriving (Generic, NFData)

type WindowEventHandler p = WindowEvent -> Maybe p

dummyWindowEventHandler :: WindowEventHandler p
dummyWindowEventHandler _ = Nothing

windowEventHandler :: WindowEventHandler O.Output
windowEventHandler = \case
  CloseRequestEvent -> Just O.CloseRequested
  FileDropEvent p -> Just (O.FileDropped p)

activityToEvent :: Maybe WindowActivity -> Maybe WindowEvent
activityToEvent = \case
  Just CloseRequested -> Just CloseRequestEvent
  Just (FileDropped p) -> Just (FileDropEvent p)
  _ -> Nothing


data WindowControl p
  = State
    { activityOf :: !(Maybe WindowActivity)
    , eventHandlerOf :: !(WindowEventHandler p)
    , guidesOf :: !(GuideID, GuideID)
    , windowAdjustmentOf :: !(Maybe (Layout -> Layout))
    }
  deriving (Generic, NFData)

instance Reaction (WindowControl po) (RInput mi vm) (ROutput mo po vm) where
  addReactant (LayoutInput li _) _ state@(State { guidesOf = (gw, gh) }) =
    -- NOTE: forcing to fix memory leak
    force $ state
    { windowAdjustmentOf = case activityOf state of
        Just (WindowResized nw nh) ->
          Just (reactToChanges [(gw, nw-ow), (gh, nh-oh)])
          where [ow, oh] = getGuides [gw, gh] li
        _ -> Nothing
    }
  addReactant (ViewInput vi) _ state = state { activityOf = inputToActivity vi }
  addReactant _ _ state = state
  getProduct _ LayoutOutput r = windowAdjustmentOf r
  getProduct _ PresenterOutput r =
    (activityToEvent . activityOf $ r) >>= eventHandlerOf r
  getProduct _ _ _ = Nothing

windowControl
  :: (Monad m)
  => WindowEventHandler po -> WindowAdjustorGen (Reactor' (RInput mi vm) (ROutput mo po vm) m)
windowControl handler gw gh = reactor' (State Nothing handler (gw, gh) Nothing)

