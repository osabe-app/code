{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Shared.Presenter.Button
 ( Button
 , ButtonState(..) -- from View
 , button
 , buttonIcon
 , enableOn
 , onClick
 , noReaction
 ) where

import Control.DeepSeq
import Control.Monad.Reader
import FULE hiding (boundsOf)
import GHC.Generics (Generic)

import Graphics.Assets
import Graphics.Context
import Graphics.Renderable
import Primitive.Geometry
import Screens.Shared.Presenter.InputContext
import Screens.Shared.Presenter.Reactor
import Screens.Shared.Presenter.State
import Screens.Shared.View.Button


type IconGenerator = ButtonState -> ButtonIcon
type StateUpdater mi = ButtonState -> mi -> Maybe ButtonState
type TransitionHandler o = ButtonState -> ButtonState -> Maybe o


data Button mi mo po
  = Button
    { boundsOf :: !Rectangle
    , iconGeneratorOf :: !IconGenerator
    , stateMovementOf :: !(State ButtonState)
    , stateUpdateOf :: !(StateUpdater mi)
    , modelOutputOf :: !(TransitionHandler mo)
    , presenterOutputOf :: !(TransitionHandler po)
    }
  deriving (Generic, NFData)

instance Reaction (Button mi mo po) (RInput mi vm) (ROutput mo po vm) where
  addReactant (LayoutInput li bi) _ bu = bu { boundsOf = makeBoundsRect li bi }
  addReactant (ModelInput mi) _ bu@(Button { stateMovementOf = m, stateUpdateOf = u }) =
    -- NOTE: forcing to fix memory leak
    force $ bu
    { stateMovementOf = maybe m (transition m . Just) (u (currentState m) mi)
    }
  addReactant (ViewInput vi) _ bu@(Button { boundsOf = b, stateMovementOf = m }) =
    bu { stateMovementOf =
      if | mouseInBounds b vi && state == Enabled ->
            transition m (Just Hovered)
         | mouseInBounds b vi && leftPressEvent vi && state == Hovered ->
            transition m (Just Pressed)
         | mouseInBounds b vi && leftReleaseEvent vi && state == Pressed ->
            transition m (Just Hovered)
         | not (mouseInBounds b vi) && state /= Disabled ->
            transition m (Just Enabled)
         | otherwise ->
            transition m Nothing
    }
    where state = currentState m
  addReactant _ _ bu = bu
  getProduct _ ModelOutput (Button { stateMovementOf = m, modelOutputOf = mo }) =
    onTransition mo m
  getProduct _ PresenterOutput (Button { stateMovementOf = m, presenterOutputOf = po }) =
    onTransition po m
  getProduct _ ViewOutput (Button { iconGeneratorOf = i, stateMovementOf = m }) =
    Just . Rendering . i $ currentState m
  getProduct _ _ _ = Nothing

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => Component (Button mi mo po) m where
  requiredWidth (Button { iconGeneratorOf = i }) = requiredWidth (i Disabled)
  requiredHeight (Button { iconGeneratorOf = i }) = requiredHeight (i Disabled)

button
  :: IconGenerator -> ButtonState -> StateUpdater mi
  -> TransitionHandler mo -> TransitionHandler po
  -> Button mi mo po
button iconGen state = Button zeroRect iconGen (initialState state)

buttonIcon :: TextureName -> IconGenerator
buttonIcon = ButtonIcon

enableOn :: (mi -> Bool) -> ButtonState -> mi -> Maybe ButtonState
enableOn f state mi =
  case (state, f mi) of
    (Disabled, True)  -> Just Enabled
    (_       , False) -> Just Disabled
    _ -> Nothing

onClick :: a -> ButtonState -> ButtonState -> Maybe a
onClick value Pressed Hovered = Just value
onClick _ _ _ = Nothing

noReaction :: ButtonState -> ButtonState -> Maybe a
noReaction _ _ = Nothing

