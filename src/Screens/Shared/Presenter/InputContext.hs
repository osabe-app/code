{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}

module Screens.Shared.Presenter.InputContext where

import Control.DeepSeq
import Data.Maybe
import GHC.Generics (Generic)

import Primitive.Geometry
import Screens.Shared.Messages
import Screens.Shared.UserInput


data MetaKeyStates
  = MetaKeyStates
    { altOf :: !Bool
    , cmdOf :: !Bool
    , ctrlOf :: !Bool
    , shiftOf :: !Bool
    }
  deriving (Eq, Generic, NFData, Show)

defaultMetaKeyStates
  = MetaKeyStates
    { altOf = False
    , cmdOf = False
    , ctrlOf = False
    , shiftOf = False
    }


data MouseState
  = MouseState
    { locationStateOf :: !(Maybe Coordinates)
    , leftButtonOf :: !Bool
    , rightButtonOf :: !Bool
    }
  deriving (Eq, Generic, NFData, Show)

defaultMouseState
  = MouseState
    { locationStateOf = Nothing
    , leftButtonOf = False
    , rightButtonOf = False
    }


data InputContext
  = InputContext
    { inputOf :: !ViewToPresenter
    , keyboardStateOf :: !MetaKeyStates
    , mouseStateOf :: !MouseState
    }
  deriving (Generic, NFData, Show)

defaultInputContext
  = InputContext
    { inputOf = Render
    , keyboardStateOf = defaultMetaKeyStates
    , mouseStateOf = defaultMouseState
    }

trackInput :: ViewToPresenter -> InputContext -> InputContext
trackInput input@(Interactivity i) state@(InputContext _ k m) =
  s { inputOf = input }
  where
    s = case i of
      Keyboard (Key d MetaAlt) ->
        state { keyboardStateOf = k { altOf = toBool d } }
      Keyboard (Key d MetaCmd) ->
        state { keyboardStateOf = k { cmdOf = toBool d } }
      Keyboard (Key d MetaCtrl) ->
        state { keyboardStateOf = k { ctrlOf = toBool d } }
      Keyboard (Key d MetaShift) ->
        state { keyboardStateOf = k { shiftOf = toBool d } }
      Mouse (Button d LeftButton l) ->
        state
        { mouseStateOf = m
          { locationStateOf = Just l
          , leftButtonOf = toBool d
          }
        }
      Mouse (Button d RightButton l) ->
        state
        { mouseStateOf = m
          { locationStateOf = Just l
          , rightButtonOf = toBool d
          }
        }
      Mouse (Position p) ->
        state
        { mouseStateOf = m
          { locationStateOf = Just (positionOf p)
          }
        }
      Mouse Lost ->
        state { mouseStateOf = m { locationStateOf = Nothing } }
      _ -> state
trackInput input state = state { inputOf = input }

inputEvent :: InputContext -> Bool
inputEvent (InputContext { inputOf = input }) =
  case input of
    Interactivity _ -> True
    _ -> False

interruptingInputEvent :: InputContext -> Bool
interruptingInputEvent (InputContext { inputOf = Interactivity i }) =
  case i of
    Keyboard (Key _ KeySpace)  -> False
    Keyboard (Key _ MetaShift) -> False
    Mouse Position{} -> False
    Mouse Scroll{}   -> False
    None -> False
    _ -> True
interruptingInputEvent (InputContext { inputOf = Render }) = False
interruptingInputEvent _ = True

keyDownEvent :: KeyName -> InputContext -> Bool
keyDownEvent keyName (InputContext { inputOf = input }) =
  case input of
    Interactivity (Keyboard (Key Down key)) | key == keyName -> True
    _ -> False

keyUpEvent :: KeyName -> InputContext -> Bool
keyUpEvent keyName (InputContext { inputOf = input }) =
  case input of
    Interactivity (Keyboard (Key Up key)) | key == keyName -> True
    _ -> False

anyCtrlEvent :: InputContext -> Bool
anyCtrlEvent (InputContext { inputOf = input }) =
  case input of
    Interactivity (Keyboard (Key _ key)) | key == MetaCmd || key == MetaCtrl -> True
    _ -> False

altHeld :: InputContext -> Bool
altHeld (InputContext { keyboardStateOf = kbs }) = altOf kbs

anyCtrlHeld :: InputContext -> Bool
anyCtrlHeld (InputContext { keyboardStateOf = kbs }) = cmdOf kbs || ctrlOf kbs

shiftHeld :: InputContext -> Bool
shiftHeld (InputContext { keyboardStateOf = kbs }) = shiftOf kbs

isQuitCommand :: InputContext -> Bool
isQuitCommand context = anyCtrlHeld context && keyDownEvent KeyQ context

isRedoCommand :: InputContext -> Bool
isRedoCommand context =
  (anyCtrlHeld context && keyDownEvent KeyY context)
  || (anyCtrlHeld context && shiftHeld context && keyDownEvent KeyZ context)

isSaveCommand :: InputContext -> Bool
isSaveCommand context = anyCtrlHeld context && keyDownEvent KeyS context

isUndoCommand :: InputContext -> Bool
isUndoCommand context =
  anyCtrlHeld context && not (shiftHeld context) && keyDownEvent KeyZ context

clickEvent :: ButtonName -> Direction -> InputContext -> Bool
clickEvent button dir context =
  case inputOf context of
    Interactivity (Mouse (Button d b _)) | d == dir && b == button -> True
    _ -> False

leftPressEvent = clickEvent LeftButton Down

leftReleaseEvent = clickEvent LeftButton Up

rightPressEvent = clickEvent RightButton Down

rightReleaseEvent = clickEvent RightButton Up

leftButtonHeld :: InputContext -> Bool
leftButtonHeld = leftButtonOf . mouseStateOf

rightButtonHeld :: InputContext -> Bool
rightButtonHeld = rightButtonOf . mouseStateOf

autoscrollInterruptingInputHeld :: InputContext -> Bool
autoscrollInterruptingInputHeld ic =
  altHeld ic
  || anyCtrlHeld ic
  || shiftHeld ic
  || leftButtonHeld ic
  || rightButtonHeld ic

mouseInBounds :: Rectangle -> InputContext -> Bool
mouseInBounds bounds =
  maybe False (inBounds bounds) . locationStateOf . mouseStateOf

mouseMovementEvent :: InputContext -> Bool
mouseMovementEvent context =
  case inputOf context of
    Interactivity (Mouse (Position _)) -> True
    _ -> False

mouseMovementData :: InputContext -> Maybe Deltas
mouseMovementData context =
  case inputOf context of
    Interactivity (Mouse (Position (Movement{ movementOf = deltas }))) ->
      Just deltas
    _ -> Nothing

mouseScrollData :: InputContext -> Maybe (Int, Int)
mouseScrollData context =
  case inputOf context of
    Interactivity (Mouse (Scroll (Deltas sx sy))) -> Just (sx, sy)
    _ -> Nothing

