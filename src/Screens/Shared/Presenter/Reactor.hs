{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Screens.Shared.Presenter.Reactor
 ( RInput(..)
 , ROutput(..)
 ) where

import FULE

import Graphics.Assets
import Graphics.Renderable
import Primitive.Color
import Screens.Shared.Presenter.InputContext
import Screens.Shared.View.Color
import Screens.Shared.View.Texture


data RInput mi vm
  = LayoutInput Layout Bounds
  | ModelInput mi
  | NoInput
  | ViewInput InputContext
  | ViewModelInput vm

data ROutput mo po vm a where
  LayoutOutput :: ROutput mo po vm (Layout -> Layout)
  ModelOutput :: ROutput mo po vm mo
  NoOutput :: ROutput mo po vm ()
  PresenterOutput :: ROutput mo po vm po
  ViewModelOutput :: ROutput mo po vm vm
  ViewOutput :: ROutput mo po vm Rendering


instance Reaction Color (RInput mi vm) (ROutput mo po vm) where
  getProduct _ ViewOutput r = Just (Rendering r)
  getProduct _ _ _ = Nothing

instance Reaction TextureName (RInput mi vm) (ROutput mo po vm) where
  getProduct _ ViewOutput r = Just (Rendering r)
  getProduct _ _ _ = Nothing

