{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}

module Screens.Shared.Presenter.Context
 ( Context
 , WithContext
 , runWithContext
 , integrateViewInput
 , integrateModelInput
 , getOutputsForModel
 , getOutputsForPresenter
 , renderViewOutput
 ) where

import Control.DeepSeq
import Control.Monad.State.Strict
import Data.List
import Data.Maybe
import qualified Data.Set as Set
import FULE hiding (boundsOf)
import qualified FULE
import Pipes.Core
import Pipes.Lift

import Graphics.Renderable
import Primitive.Geometry
import Screens.Shared.Messages
import Screens.Shared.Presenter.Reactor
import Screens.Shared.Presenter.InputContext


type R mi mo po vm = Reactor' (RInput mi vm) (ROutput mo po vm)
type CIR mi mo po vm m = ComponentInfo (R mi mo po vm m)

data Context mi mo po vm m
  = Context
    { boundsOf :: ![FULE.Bounds]
    , componentsOf :: ![CIR mi mo po vm m]
    , inputContextOf :: !InputContext
    , layoutOf :: !Layout
    }

type WithContext mi mo po vm m = StateT (Context mi mo po vm m) m


runWithContext
  :: (Monad m)
  => (Layout, [CIR mi mo po vm m])
  -> Proxy a' a b' b (WithContext mi mo po vm m) o
  -> Proxy a' a b' b m o
runWithContext (layout, components) = evalStateP $
  Context
  { boundsOf = map FULE.boundsOf components
  , componentsOf = components
  , inputContextOf = defaultInputContext
  , layoutOf = layout
  }


-- for some reason the compiler has trouble with the types, so we have to
-- specify them manually:

ar :: RInput mi vm -> ROutput mo po vm o -> R mi mo po vm m -> R mi mo po vm m
ar = addReactant

gp :: RInput mi vm -> ROutput mo po vm o -> R mi mo po vm m -> Maybe o
gp = getProduct


addInput :: RInput mi vm -> [CIR mi mo po vm m] -> [CIR mi mo po vm m]
addInput input = map (ar input NoOutput <$>)

getOutputs :: ROutput mo po vm o -> [CIR mi mo po vm m] -> [o]
getOutputs outputType = mapMaybe (gp NoInput outputType . componentOf)


integrateViewInput :: (Monad m) => ViewToPresenter -> WithContext mi mo po vm m ()
integrateViewInput input = do
  c@Context { componentsOf = components, inputContextOf = inputContext } <- get
  let inputContext' = trackInput input inputContext
  let components' = addInput (ViewInput inputContext') components
  put (c { componentsOf = components', inputContextOf = inputContext' })

integrateModelInput :: (Monad m) => mi -> WithContext mi mo po vm m ()
integrateModelInput model = modify' (\s ->
  s { componentsOf = addInput (ModelInput model) (componentsOf s) })

getOutputsForModel :: (Monad m) => WithContext mi mo po vm m [mo]
getOutputsForModel = getOutputs ModelOutput <$> gets componentsOf

getOutputsForPresenter :: (Monad m) => WithContext mi mo po vm m [po]
getOutputsForPresenter = getOutputs PresenterOutput <$> gets componentsOf


adjustToLayout :: (Monad m) => Layout -> CIR mi mo po vm m -> CIR mi mo po vm m
adjustToLayout layout ci = ar (LayoutInput layout $ FULE.boundsOf ci) NoOutput <$> ci

updateLayout :: (Monad m) => Context mi mo po vm m -> Context mi mo po vm m
updateLayout context =
  context { boundsOf = bounds', componentsOf = components', layoutOf = layout' }
  where
    Context { componentsOf = components, layoutOf = layout } = context
    layout' = ($ layout) . foldl' (.) id $ getOutputs LayoutOutput components
    components' = map (adjustToLayout layout') components
    bounds' = map FULE.boundsOf components'

updateViewModels :: (Monad m) => Context mi mo po vm m -> Context mi mo po vm m
updateViewModels context@(Context { componentsOf = components }) =
  context { componentsOf = components' }
  where
    viewModels = getOutputs ViewModelOutput components
    components' = foldl' (flip (addInput . ViewModelInput)) components viewModels

renderViewOutput :: (Monad m) => WithContext mi mo po vm m [(Rendering, [Rectangle])]
renderViewOutput = do
  modify' updateLayout
  modify' updateViewModels
  bounds <- gets boundsOf
  infos <- gets componentsOf
  layout <- gets layoutOf
  let viewOutputs = map (gp NoInput ViewOutput . componentOf) infos
  let rects = map (makeBoundsRects layout) bounds
  return . catMaybes $ zipWith (\v b -> (, b) <$> v) viewOutputs rects

