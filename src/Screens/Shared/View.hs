{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Shared.View
 ( view
 ) where

import Control.Monad.Reader
import Data.Char
import Data.List
import Pipes
import Pipes.Core
import Pipes.Lift
import System.FilePath

import Audio.Playback
import Config
import Graphics.Context
import Graphics.Renderable
import Graphics.SDLShim
import Screens.Shared.Messages
import Screens.Shared.View.Dialog
import Screens.Shared.View.Display
import Screens.Shared.View.Event


type ViewState = Client ViewToPresenter PresenterToView

class
  ( HasGraphicsContext r
  , HasProgramName r
  , HasSettings r
  , HasStyle r
  , MonadIO m
  , MonadReader r m
  ) => ViewStateConstraint m r where {}
instance
  ( HasGraphicsContext r
  , HasProgramName r
  , HasSettings r
  , HasStyle r
  , MonadIO m
  , MonadReader r m
  ) => ViewStateConstraint m r where {}


view
  :: ViewStateConstraint m r
  => [String] -> ViewState m a
view messages = do
  style <- asks getStyle
  programName <- asks getProgramName
  driverName <- getAudioDriverName
  res <- initializeAudioDriver driverName
  let messages' =
        case res of
          Left m -> m : messages
          Right _ -> messages
  mapM_ (notify "Issue during initialization") messages'
  running Render

running
  :: ViewStateConstraint m r
  => ViewToPresenter -> ViewState m a
running input = do
  limitFpsTo 60 do
    response <- request input
    processPresenterResponse response
  uiInput <- readInput
  running uiInput

processPresenterResponse
  :: ViewStateConstraint m r
  => PresenterToView -> ViewState m ()
processPresenterResponse = \case
    Display components windowTitle -> lift do
      updateHighDPIFlag
      setWindowTitle windowTitle
      display components
    Alert summary details -> do
      alert summary details
      response <- request Render
      processPresenterResponse response
    Notify summary details -> do
      notify summary details
      response <- request Render
      processPresenterResponse response
    Prompt summary details options -> do
      selection <- prompt summary details options
      response <- request (PromptResponse selection)
      processPresenterResponse response

