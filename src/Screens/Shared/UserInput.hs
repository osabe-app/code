{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}

module Screens.Shared.UserInput where

import Control.DeepSeq
import GHC.Generics (Generic)

import Primitive.Geometry


--------------------------------
-- direction
--------------------------------

data Direction = Down | Up
  deriving (Eq, Generic, NFData, Show)

toBool Down = True
toBool Up   = False


--------------------------------
-- keyboard
--------------------------------

data KeyName
  = KeyQ
  | KeyS
  | KeyY
  | KeyZ
  | KeyF4
  | KeyEnd
  | KeyHome
  | KeyPageDown
  | KeyPageUp
  | KeySpace
  | MediaAudioNext
  | MediaAudioPrev
  | MediaAudioPlay
  | MediaAudioStop
  | MetaAlt -- AKA Option (on Mac)
  | MetaCmd -- Mac ('Windows' key on Windows)
  | MetaCtrl
  | MetaShift
  deriving (Eq, Generic, NFData, Show)

data KeyboardInput
  = Key
    { keyDirectionOf :: !Direction
    , keyNameOf :: !KeyName
    }
  deriving (Eq, Generic, NFData, Show)


--------------------------------
-- mouse
--------------------------------

data ButtonName
  = LeftButton
  | RightButton
  deriving (Eq, Generic, NFData, Show)

data Movement
  = Movement
    { positionOf :: !Coordinates
    , movementOf :: !Deltas
    }
  deriving (Eq, Generic, NFData, Show)

data MouseInput
  = Button
    { buttonDirectionOf :: !Direction
    , buttonNameOf :: !ButtonName
    , locationOf :: !Coordinates
    }
  | Position !Movement
  | Scroll
    { amountOf :: !Deltas
    }
  | Lost
  deriving (Eq, Generic, NFData, Show)


--------------------------------
-- window
--------------------------------

newtype WindowInput
  -- = CloseRequest
  -- | ContentDisplaying !Bool
  = Resize Rectangle
  deriving (Eq, Generic, NFData, Show)


--------------------------------
-- user input
--------------------------------

data UserInput
  = Keyboard !KeyboardInput
  | Mouse !MouseInput
  | Window !WindowInput
  --
  | UnspecifiedInput -- nothing that we're processing but which might interrupt
  | None
  deriving (Eq, Generic, NFData, Show)

