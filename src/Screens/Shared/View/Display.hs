{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE MonoLocalBinds #-}

module Screens.Shared.View.Display where

import Control.Monad
import Data.Maybe
import SDL

import Graphics.Renderable
import Graphics.Context
import Graphics.SDLShim
import Primitive.Color
import Primitive.Geometry as G


display :: (CanRender r m) => [(Rendering, [G.Rectangle])] -> m ()
display items = do
  renderer <- getRenderer
  rendererDrawColor renderer $= toV4 black
  clear renderer
  dims <- getWindowDims
  highDPI <- readHighDPIFlag
  let adjustToDPI = if highDPI then doubleRect else id
  forM_ items \(rendering, rects) -> do
    let boundingRect = adjustToDPI (head rects) -- we have to have at least bounds
    let clippingRect = adjustToDPI (fromMaybe zeroRect (totalIntersection rects))
    --
    let G.Rectangle _ (Dimensions bw bh) = boundingRect
    let G.Rectangle _ (Dimensions cw ch) = clippingRect
    unless (bh <= 0 || bw <= 0 || ch <= 0 || cw <= 0) do
      -- NOTE the clip rect defaults to an empty rectangle instead of Nothing
      rendererClipRect renderer $= Just (toSdlRect clippingRect)
      render rendering boundingRect
  present renderer

