module Screens.Shared.View.Color where

import Graphics.Context
import Graphics.Renderable
import Graphics.SDLShim
import Primitive.Color


instance Renderable Color where
  render color bounds = do
    renderer <- getRenderer
    draw renderer (color, bounds)

