{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}

module Screens.Shared.View.Event
 ( readInput
 , toDropInput
 ) where

import Control.Monad
import Control.Monad.IO.Class
import Data.Maybe
import Foreign.C.String
import SDL

import Primitive.Geometry
import Screens.Shared.Messages
import Screens.Shared.UserInput as UI


readInput :: (MonadIO m) => m ViewToPresenter
readInput = do
  -- `pumpEvents` should be called implicitly by `pollEvent`, but this seems to
  -- have changed or broken in SDL2 version 2.0.20 (so far as I can tell), so we
  -- have to call it explicitly here:
  pumpEvents
  txGuiEvent =<< pollEvent

toDropInput :: Maybe FilePath -> ViewToPresenter
toDropInput = maybe Render FileDropped

txGuiEvent :: (MonadIO m) => Maybe SDL.Event -> m ViewToPresenter
txGuiEvent event =
  case event of
    Nothing -> return Render
    Just e ->
      case eventPayload e of
        -- keyboard
        KeyboardEvent e -> return (fromMaybe Render $ txKeyboardEvent e)
        -- mouse
        MouseButtonEvent e -> return (txMouseButtonEvent e)
        MouseMotionEvent e -> return (txMouseMotionEvent e)
        MouseWheelEvent e -> return (txMouseWheelEvent e)
        -- window
        DropEvent e -> txDropEvent e
        WindowClosedEvent e -> return CloseRequested
        WindowSizeChangedEvent e -> return (txWindowSizeChangedEvent e)
        WindowShownEvent e -> return (ContentDisplaying True)
        WindowHiddenEvent e -> return (ContentDisplaying False)
        WindowExposedEvent e -> return (ContentDisplaying True)
        WindowMinimizedEvent e -> return (ContentDisplaying False)
        WindowMaximizedEvent e -> return (ContentDisplaying True)
        WindowRestoredEvent e -> return (ContentDisplaying True)
        WindowGainedMouseFocusEvent e -> return (Interactivity UI.UnspecifiedInput)
        WindowLostMouseFocusEvent e -> return (Interactivity (UI.Mouse UI.Lost))
        -- default
        _ -> return Render

txKeyboardEvent :: KeyboardEventData -> Maybe ViewToPresenter
txKeyboardEvent e =
  if repeat then Nothing else Just . Interactivity $
  maybe UI.UnspecifiedInput (UI.Keyboard . UI.Key direction) name
  where
    direction = if keyboardEventKeyMotion e == Pressed then UI.Down else UI.Up
    repeat = keyboardEventRepeat e
    name = keycodeToKeyName . keysymKeycode $ keyboardEventKeysym e

keycodeToKeyName :: Keycode -> Maybe KeyName
keycodeToKeyName = \case
  KeycodeAudioNext -> Just MediaAudioNext
  KeycodeAudioPrev -> Just MediaAudioPrev
  KeycodeAudioPlay -> Just MediaAudioPlay
  KeycodeAudioStop -> Just MediaAudioStop
  KeycodeQ         -> Just KeyQ
  KeycodeS         -> Just KeyS
  KeycodeY         -> Just KeyY
  KeycodeZ         -> Just KeyZ
  KeycodeSpace     -> Just KeySpace
  KeycodeF4        -> Just KeyF4
  KeycodeHome      -> Just KeyHome
  KeycodeEnd       -> Just KeyEnd
  KeycodePageUp    -> Just KeyPageUp
  KeycodePageDown  -> Just KeyPageDown
  KeycodeLAlt      -> Just MetaAlt
  KeycodeRAlt      -> Just MetaAlt
  KeycodeLGUI      -> Just MetaCmd
  KeycodeRGUI      -> Just MetaCmd
  KeycodeLCtrl     -> Just MetaCtrl
  KeycodeRCtrl     -> Just MetaCtrl
  KeycodeLShift    -> Just MetaShift
  KeycodeRShift    -> Just MetaShift
  _                -> Nothing

txMouseButtonEvent :: MouseButtonEventData -> ViewToPresenter
txMouseButtonEvent e =
  let direction = if mouseButtonEventMotion e == Pressed then UI.Down else UI.Up
      button = mouseButtonEventButton e
      P (V2 x y) = mouseButtonEventPos e
      coords = Coordinates { xOf = fromIntegral x, yOf = fromIntegral y }
  in Interactivity case button of
      ButtonLeft  -> UI.Mouse (UI.Button direction LeftButton coords)
      ButtonRight -> UI.Mouse (UI.Button direction RightButton coords)
      _ -> UI.UnspecifiedInput

txMouseMotionEvent :: MouseMotionEventData -> ViewToPresenter
txMouseMotionEvent e =
  let P (V2 x y) = mouseMotionEventPos e
      V2 dx dy = mouseMotionEventRelMotion e
  in Interactivity . UI.Mouse . UI.Position $ Movement
    { positionOf = Coordinates { xOf = fromIntegral x, yOf = fromIntegral y }
    , movementOf = Deltas { deltaXOf = fromIntegral dx, deltaYOf = fromIntegral dy }
    }

txMouseWheelEvent :: MouseWheelEventData -> ViewToPresenter
txMouseWheelEvent e =
  let V2 x y = mouseWheelEventPos e
      coords = Deltas { deltaXOf = fromIntegral x, deltaYOf = fromIntegral y }
      invCoords = Deltas { deltaXOf = -1 * fromIntegral x, deltaYOf = -1 * fromIntegral y }
  in Interactivity
    case mouseWheelEventDirection e of
      ScrollNormal  -> UI.Mouse (UI.Scroll { UI.amountOf = coords })
      ScrollFlipped -> UI.Mouse (UI.Scroll { UI.amountOf = invCoords })

txDropEvent :: (MonadIO m) => DropEventData -> m ViewToPresenter
txDropEvent e = do
  let cString = dropEventFile e
  filePath <- liftIO (peekCString cString)
  return (FileDropped filePath)

txWindowSizeChangedEvent :: WindowSizeChangedEventData -> ViewToPresenter
txWindowSizeChangedEvent e =
  let V2 width height = windowSizeChangedEventSize e
  in WindowResized Dimensions
    { widthOf = fromIntegral width
    , heightOf = fromIntegral height
    }

