{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Shared.View.Texture where

import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.Proxy
import FULE

import Graphics.Assets
import Graphics.Context
import Graphics.Renderable
import Graphics.SDLShim
import Primitive.Geometry


instance Renderable TextureName where
  render name (Rectangle coords _) = do
    renderer <- getRenderer
    texture <- getTexture name
    draw renderer (Positioned coords texture)

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => Component TextureName m where
  requiredWidth name = do
    Dimensions width _ <- getLowDPITextureDims name
    return (Just width)
  requiredHeight name = do
    Dimensions _ height <- getLowDPITextureDims name
    return (Just height)

