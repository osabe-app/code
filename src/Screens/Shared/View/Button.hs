{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Shared.View.Button
 ( ButtonState(..)
 , ButtonIcon(..)
 ) where

import Control.DeepSeq
import Control.Monad.Reader
import FULE
import GHC.Generics (Generic)

import Graphics.Assets
import Graphics.Context
import Graphics.Renderable
import Graphics.SDLShim
import Primitive.Geometry


data ButtonState
  = Disabled
  | Enabled
  | Hovered
  | Pressed
  deriving (Eq, Generic, NFData, Ord)

indexFor :: ButtonState -> Int
indexFor = \case
  Disabled -> 0
  Enabled  -> 1
  Hovered  -> 2
  Pressed  -> 3


data ButtonIcon
  = ButtonIcon
    { textureOf :: TextureName
    , stateOf :: ButtonState
    }

instance Renderable ButtonIcon where
  render (ButtonIcon textureName state) (Rectangle coords d@(Dimensions w _ )) = do
    renderer <- getRenderer
    texture <- getTexture textureName
    let portion = Rectangle (Coordinates (indexFor state * w) 0) d
    let positioned = Positioned coords (Portion portion texture)
    draw renderer positioned

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => Component ButtonIcon m where
  requiredWidth (ButtonIcon textureName _) =
    Just . (`div` 4) . widthOf <$> getLowDPITextureDims textureName
  requiredHeight (ButtonIcon textureName _) =
    Just . heightOf <$> getLowDPITextureDims textureName

