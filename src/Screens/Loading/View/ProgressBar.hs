{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Loading.View.ProgressBar where

import Control.Monad.Reader
import qualified Data.Vector.Unboxed as VU
import FULE

import Config
import Graphics.Assets
import Graphics.Context
import Graphics.Renderable
import Graphics.SDLShim
import Primitive.Geometry


data ProgressBar
  = FileLoadProgress { progressOf :: Float }
  | SegmentationProgress { progressOf :: Float }
  | VisualsCreationProgress { progressOf :: Float }

instance Renderable ProgressBar where
  render progressBar bounds = do
    renderer <- getRenderer
    style <- asks getStyle
    highDPI <- readHighDPIFlag
    let dpiMul = if highDPI then 2 else 1
    let (Rectangle (Coordinates x y) (Dimensions width height)) = bounds
    let progress = progressOf progressBar
    -- background
    let bgColor =
          if | progress == 0 -> loadScreenProgressBarInactiveBackgroundColorOf style
             | progress == 1 -> loadScreenProgressBarCompleteBackgroundColorOf style
             | otherwise     -> loadScreenProgressBarActiveBackgroundColorOf style
    draw renderer (bgColor, bounds)
    -- bar
    -- TODO change style to have a single padding value for all bars
    -- and maybe split out horiz and veritcal padding -- so, two values?
    let padding = fromIntegral $ dpiMul *
          if | progress == 0 -> loadScreenProgressBarInactivePaddingSizeOf style
             | progress == 1 -> loadScreenProgressBarCompletePaddingSizeOf style
             | otherwise     -> loadScreenProgressBarActivePaddingSizeOf style
    let borderSize = fromIntegral dpiMul
    bar <- getTexture case progressBar of
      FileLoadProgress{} -> LoadScreenFileBarGraphic
      SegmentationProgress{} -> LoadScreenClipsBarGraphic
      VisualsCreationProgress{} -> LoadScreenVisualsBarGraphic
    (Dimensions texWidth texHeight) <- getDimensions bar
    let texWidth' = ceiling $ fromIntegral texWidth * progress
    let subCoords = Coordinates 0 0
    let subDims = Dimensions texWidth' texHeight
    let offset = borderSize + padding
    let barCoords = Coordinates (x + offset) (y + offset)
    let portion = Portion (Rectangle subCoords subDims) bar
    draw renderer (Positioned barCoords portion)
    -- border
    let borderColor =
          if | progress == 0 -> loadScreenProgressBarInactiveBorderColorOf style
             | progress == 1 -> loadScreenProgressBarCompleteBorderColorOf style
             | otherwise     -> loadScreenProgressBarActiveBorderColorOf style
    let innerAdjustment = 2 * borderSize + 2 * padding
    let leftOuterX = x
    let leftInnerX = leftOuterX + innerAdjustment
    let rightOuterX = x + width - 1
    let rightInnerX = rightOuterX - innerAdjustment
    let upperY = y
    let lowerY = upperY + height - 1
    if progress == 0
    then do
      draw renderer (borderColor, Polyline $ VU.fromList
        [ Coordinates leftInnerX upperY
        , Coordinates leftOuterX upperY
        , Coordinates leftOuterX lowerY
        , Coordinates leftInnerX lowerY
        ])
      draw renderer (borderColor, Polyline $ VU.fromList
        [ Coordinates rightInnerX upperY
        , Coordinates rightOuterX upperY
        , Coordinates rightOuterX lowerY
        , Coordinates rightInnerX lowerY
        ])
    else
      draw renderer (borderColor, rectToPoly bounds)

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r, HasStyle r)
  => Component ProgressBar m where
  requiredWidth pb = do
    Dimensions width _ <- getLowDPITextureDims LoadScreenFileBarGraphic
    style <- asks getStyle
    let padding = fromIntegral $ loadScreenProgressBarInactivePaddingSizeOf style
    let borderSize = 1
    return $ Just (width + padding * 2 + borderSize * 2)
  requiredHeight pb = do
    Dimensions _ height <- getLowDPITextureDims LoadScreenFileBarGraphic
    style <- asks getStyle
    let padding = fromIntegral $ loadScreenProgressBarInactivePaddingSizeOf style
    let borderSize = 1
    return $ Just (height + padding * 2 + borderSize * 2)

