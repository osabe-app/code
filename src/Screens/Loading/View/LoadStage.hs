{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Loading.View.LoadStage
 ( LoadStage(..)
 ) where

import Control.DeepSeq
import Control.Monad.Reader
import Data.Proxy
import FULE
import GHC.Generics (Generic)

import Graphics.Assets
import Graphics.Context
import Graphics.Renderable
import Graphics.SDLShim
import Primitive.Geometry


data LoadStage
  = LoadingFile
  | Segmenting
  | CreatingVisuals
  deriving (Generic, NFData)

instance Renderable LoadStage where
  render loadStage (Rectangle coords _) = do
    renderer <- getRenderer
    texture <- getTexture case loadStage of
      LoadingFile -> LoadScreenFileStatusText
      Segmenting -> LoadScreenClipsStatusText
      CreatingVisuals -> LoadScreenVisualsStatusText
    let positioned = Positioned coords texture
    draw renderer positioned

textureNames =
  [ LoadScreenFileStatusText
  , LoadScreenClipsStatusText
  , LoadScreenVisualsStatusText
  ]

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => Component LoadStage m where
  requiredWidth _ =
    Just . maximum . map widthOf <$> mapM getLowDPITextureDims textureNames
  requiredHeight _ =
    Just . maximum . map heightOf <$> mapM getLowDPITextureDims textureNames

