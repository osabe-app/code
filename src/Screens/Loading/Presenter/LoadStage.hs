{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Loading.Presenter.LoadStage
 ( LoadStage
 , loadStage
 ) where


import Control.DeepSeq
import Control.Monad.Reader
import FULE
import GHC.Generics (Generic)

import Config
import Graphics.Context
import Graphics.Renderable
import Screens.Loading.Messages
import qualified Screens.Loading.View.LoadStage as View
import Screens.Shared.Presenter.Reactor


newtype LoadStage = LoadStage View.LoadStage
  deriving (Generic, NFData)

loadStage :: LoadStage
loadStage = LoadStage View.LoadingFile

instance Reaction LoadStage (RInput ModelToPresenter vm) (ROutput mo po vm) where
  addReactant (ModelInput (Progress _ l s v)) _ _ = LoadStage case (l, s, v) of
    (1, 1, _) -> View.CreatingVisuals
    (1, _, _) -> View.Segmenting
    (_, _, _) -> View.LoadingFile
  addReactant _ _ state = state
  getProduct _ ViewOutput (LoadStage s) = Just (Rendering s)
  getProduct _ _ _ = Nothing

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r, HasStyle r)
  => Component LoadStage m where
  requiredWidth pb = requiredWidth View.LoadingFile
  requiredHeight pb = requiredHeight View.LoadingFile

