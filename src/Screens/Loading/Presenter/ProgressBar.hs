{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Loading.Presenter.ProgressBar
 ( ProgressBar
 , progressBar
 ) where

import Control.DeepSeq
import Control.Monad.Reader
import FULE
import GHC.Generics (Generic)

import Config
import Graphics.Context
import Graphics.Renderable
import Screens.Loading.Messages
import qualified Screens.Loading.View.ProgressBar as View
import Screens.Shared.Presenter.Reactor


data ProgressBar
  = ProgressBar
    { readModelOf :: ModelToPresenter -> Float
    , progressOf :: Float
    , constructViewOf :: Float -> View.ProgressBar
    }
  deriving (Generic, NFData)

progressBar
  :: (ModelToPresenter -> Float) -> (Float -> View.ProgressBar)
  -> ProgressBar
progressBar readModel = ProgressBar readModel 0

instance Reaction ProgressBar (RInput ModelToPresenter vm) (ROutput mo po vm) where
  addReactant (ModelInput mi) _ state = state { progressOf = readModelOf state mi }
  addReactant _ _ state = state
  getProduct _ ViewOutput (ProgressBar _ p f) = Just . Rendering . f $ p
  getProduct _ _ _ = Nothing

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r, HasStyle r)
  => Component ProgressBar m where
  requiredWidth pb = requiredWidth (View.FileLoadProgress 0)
  requiredHeight pb = requiredHeight (View.FileLoadProgress 0)

