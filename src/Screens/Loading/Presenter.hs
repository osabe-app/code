{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Loading.Presenter
 ( presenter
 ) where

import Control.Monad.Reader
import FULE
import Pipes
import Pipes.Core

import Config
import Graphics.Assets
import Graphics.Context
import Graphics.SDLShim
import Primitive.Color
import Primitive.Geometry
import Screens.Loading.Messages
import Screens.Loading.Presenter.LoadStage
import Screens.Loading.Presenter.ProgressBar
import Screens.Loading.View.ProgressBar as View
import Screens.Shared.Messages hiding (FileDropped)
import Screens.Shared.Output
import Screens.Shared.Presenter.Context
import Screens.Shared.Presenter.Reactor
import Screens.Shared.Presenter.Window
import Screens.Shared.View.Color
import Screens.Shared.View.Texture


class
  ( HasGraphicsContext r
  , HasProgramName r
  , HasSettings r
  , HasStyle r
  , MonadIO m
  , MonadReader r m
  ) => PresenterStateConstraint m r where {}
instance
  ( HasGraphicsContext r
  , HasProgramName r
  , HasSettings r
  , HasStyle r
  , MonadIO m
  , MonadReader r m
  ) => PresenterStateConstraint m r where {}


type MO = ()
type PO = Output
type R = Reactor' (RInput ModelToPresenter ()) (ROutput MO PO ())
type I m = ItemM m (R m)

type PresenterState = Proxy PresenterToModel ModelToPresenter ViewToPresenter PresenterToView
type PresenterStateWithContext m = PresenterState (WithContext ModelToPresenter MO PO () m)


-- for some reason the program hangs if this is in `loadScreenUI`
getLoadStageText :: PresenterStateConstraint m r => m (R m)
getLoadStageText = return (reactor' loadStage)

loadScreenUI
  :: forall m r . (PresenterStateConstraint m r)
  => Int -> Int -> m (Layout, [ComponentInfo (R m)])
loadScreenUI width height = do
  loadScreenBgColor :: R m <- asks (reactor' . loadScreenBackgroundColorOf . getStyle)
  let loadScreenLoadingText :: R m = reactor' LoadScreenLoadingText
  let pathText :: R m = reactor' LoadScreenFilePath
  loadStage <- getLoadStageText
  let fileLoadProgressBar :: R m = reactor' (progressBar fileLoadProgressOf View.FileLoadProgress)
  let segmentationProgressBar :: R m = reactor' (progressBar segmentationProgressOf View.SegmentationProgress)
  let visualsCreationProgressBar :: R m = reactor' (progressBar visualsCreationProgressOf View.VisualsCreationProgress)
  layoutM
    (window (width, height) (windowControl windowEventHandler)
      (layered
        ([item loadScreenBgColor
        , item (centered
            (arrayedVert (padding 10 10) -- TODO read spacing from style
              ([item loadScreenLoadingText
              , item (unreckonedHoriz pathText)
              , item loadStage
              , item fileLoadProgressBar
              , item segmentationProgressBar
              , item visualsCreationProgressBar
              ]::[I m])))
        ]::[I m])))


presenter
  :: PresenterStateConstraint m r
  => Dimensions -> ViewToPresenter -> PresenterState m Output
presenter (Dimensions width height) input = do
  ui <- lift $ loadScreenUI width height
  runWithContext ui $ loading input

loading
  :: PresenterStateConstraint m r
  => ViewToPresenter -> PresenterStateWithContext m Output
loading input = do
  lift $ integrateViewInput input
  output <- lift $ getOutputsForPresenter
  state <- request if null output then Report else Stop (head output)
  lift $ integrateModelInput state
  programName <- getNameOfProgram
  view <- lift renderViewOutput
  let title = concat ["loading ", filePathOf state, " - ", programName]
  input' <- respond (Display view title)
  loading input'

