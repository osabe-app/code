module Screens.Loading.Messages where

import Screens.Shared.Output


data PresenterToModel
  = Report
  | Stop Output

data ModelToPresenter
  = Progress
    { filePathOf :: FilePath
    , fileLoadProgressOf :: Float
    , segmentationProgressOf :: Float
    , visualsCreationProgressOf :: Float
    }

