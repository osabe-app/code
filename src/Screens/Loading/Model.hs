{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Screens.Loading.Model where

import Control.Exception
import Control.Monad.Reader
import Pipes.Core

import Audio
import Config
import Control.Concurrent.Operation
import Screens.Editing.View.Waveform.Representation
import Screens.Loading.Messages
import Screens.Shared.Output


type ModelState = Server PresenterToModel ModelToPresenter
type WaveRep = WaveformRepresentation

class
  ( Audio.AudioLoader m
  , HasSettings r
  , MonadReader r m
  , Operation m o
  ) => ModelStateConstraint m r o where {}
instance
  ( Audio.AudioLoader m
  , HasSettings r
  , MonadReader r m
  , Operation m o
  ) => ModelStateConstraint m r o where {}


model
  :: ModelStateConstraint m r o
  => FilePath -> PresenterToModel -> ModelState m Output
model path input = do
  op <- lift (start (Audio.readAudio path))
  loadingFile path op input


--------------------------------
-- file load state
--------------------------------

loadingFile
  :: ModelStateConstraint m r o
  => FilePath -> o Audio.AudioFile -> PresenterToModel
  -> ModelState m Output
loadingFile path op = \case
  Stop output -> do
    lift (cancel op)
    return output
  _ -> do
    loadState <- lift (poll op)
    processLoadProgress path op loadState

processLoadProgress
  :: ModelStateConstraint m r o
  => FilePath -> o Audio.AudioFile -> Status Audio.AudioFile
  -> ModelState m Output
processLoadProgress path op = \case
  Progressing p -> do
    input' <- respond (Progress path p 0 0)
    loadingFile path op input'
  Errored e -> do
    return (FileLoadErrored path (displayException e))
  Completed audio -> do
    threshold <- lift getThresholdIndBDown
    silence <- lift getMaxIntraclipSilenceInMs
    let context = Audio.makeSegmentsReadingContext threshold silence audio
    op' <- lift (start (Audio.constructSegments context))
    input' <- respond (Progress path 1 0 0)
    makingSegments audio op' input'


--------------------------------
-- segments creation state
--------------------------------

makingSegments
  :: ModelStateConstraint m r o
  => Audio.AudioFile -> o Audio.Segments -> PresenterToModel
  -> ModelState m Output
makingSegments audio op = \case
  Stop output -> do
    lift (cancel op)
    return output
  _ -> do
    loadState <- lift (poll op)
    processSegmentsProgress audio op loadState

processSegmentsProgress
  :: ModelStateConstraint m r o
  => Audio.AudioFile -> o Audio.Segments -> Status Audio.Segments
  -> ModelState m Output
processSegmentsProgress audio op = \case
  Progressing p -> do
    let path = Audio.pathOf audio
    input' <- respond (Progress path 1 p 0)
    makingSegments audio op input'
  Errored e -> do
    let path = Audio.pathOf audio
    return (FileLoadErrored path (displayException e))
  Completed segments -> do
    let path = Audio.pathOf audio
    threshold <- lift getThresholdIndBDown
    op' <- lift (start (makeWaveformRepresentation audio segments threshold))
    input' <- respond (Progress path 1 1 0)
    makingVisuals audio segments op' input'


--------------------------------
-- visuals creation state
--------------------------------

makingVisuals
  :: ModelStateConstraint m r o
  => Audio.AudioFile -> Audio.Segments -> o WaveRep -> PresenterToModel
  -> ModelState m Output
makingVisuals audio segments op = \case
  Stop output -> do
    lift (cancel op)
    return output
  _ -> do
    loadState <- lift (poll op)
    processVisualsProgress audio segments op loadState

processVisualsProgress
  :: ModelStateConstraint m r o
  => Audio.AudioFile -> Audio.Segments -> o WaveRep -> Status WaveRep
  -> ModelState m Output
processVisualsProgress audio segments op = \case
  Progressing p -> do
    let path = Audio.pathOf audio
    input' <- respond (Progress path 1 1 p)
    makingVisuals audio segments op input'
  Errored _ -> do
    let path = Audio.pathOf audio
    let message = "Something went wrong when creating visuals for the file."
    return (FileLoadErrored path message)
  Completed waveRep -> do
    return (FileLoaded audio segments waveRep)

