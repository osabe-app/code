{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Screens.Editing
 ( editingScreen
 ) where

import Control.Concurrent.MVar
import Control.Monad.Fix
import Control.Monad.IO.Class
import Control.Monad.Reader
import Pipes
import Pipes.Core
import Pipes.Lift
import SDL.Audio

import App
import Audio
import Config
import Control.Concurrent.Container
import Control.Concurrent.Operation
import GlobalContext
import Graphics.Context
import Primitive.Geometry
import Screens.Editing.Model
import Screens.Editing.Model.State
import Screens.Editing.Presenter
import Screens.Editing.View.Waveform.Representation
import Screens.Shared.Output
import Screens.Shared.View


instance AudioPlayer App MVar State AudioDevice where
  openPlaybackDevice = openDevice
  closePlaybackDevice = closeDevice
  startPlaybackDevice = startDevice
  stopPlaybackDevice = stopDevice
  togglePlaybackDevice = toggleDevice
  deviceIsPlaying = isPlaying

layers
  :: ( AudioLoader m
     , AudioPlayer m a State d
     , Container m a
     , HasGraphicsContext r
     , HasProgramName r
     , HasSettings r
     , HasStyle r
     , MonadFix m
     , MonadIO m
     , MonadReader r m
     , Operation m o
     )
  => Dimensions -> AudioFile -> Segments -> WaveformRepresentation -> [String]
  -> Effect m Output
layers windowSize audio segments waveRep =
  model audio segments waveRep >+> presenter windowSize >+> view

editingScreen
  :: GlobalContext -> [String] -> AudioFile -> Segments -> WaveformRepresentation
  -> IO Output
editingScreen globalContext messages audio segments waveRep = do
  windowSize <- windowDims (getGraphicsContext globalContext)
  runEffect . runReaderP globalContext $
    layers windowSize audio segments waveRep messages

