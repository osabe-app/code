module Screens.Loading
 ( loadingScreen
 ) where

import Control.Monad.Reader
import Pipes
import Pipes.Core
import Pipes.Lift

import App
import Audio
import Config
import Control.Concurrent.Operation
import GlobalContext
import Graphics.Context
import Primitive.Geometry
import Screens.Loading.Model
import Screens.Loading.Presenter
import Screens.Shared.Output
import Screens.Shared.View
import Screens.Shared.View.Dialog


layers
  :: ( AudioLoader m
     , HasGraphicsContext r
     , HasProgramName r
     , HasSettings r
     , HasStyle r
     , MonadIO m
     , MonadReader r m
     , Operation m o
     )
  => FilePath -> Dimensions -> [String] -> Effect m Output
layers filePath windowSize = model filePath >+> presenter windowSize >+> view

loadingScreen :: GlobalContext -> [String] -> FilePath -> IO Output
loadingScreen globalContext messages filePath = do
  -- create path texture
  let graphicsContext = graphicsContextOf globalContext
  let style = getStyle $ configOf globalContext
  graphicsContext' <- buildPathTEXTure filePath style graphicsContext
  let globalContext' = globalContext { graphicsContextOf = graphicsContext' }
  --
  windowSize <- windowDims graphicsContext'
  output <- runEffect . runReaderP globalContext' $
    layers filePath windowSize messages
  -- it was cleaner to alert here rather than in the MVP:
  case output of
    FileLoadErrored path error -> do
      let message = concat ["Could not load the file:\n", path, "\n\n", error]
      alert "Error loading file" message
    _ -> return ()
  -- free path texture
  freePathTEXTure graphicsContext'
  --
  return output

