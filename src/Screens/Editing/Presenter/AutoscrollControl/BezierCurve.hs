module Screens.Editing.Presenter.AutoscrollControl.BezierCurve
 ( ScrollIntent(..)
 , BezierConfig(..)
 , makeBezier
 ) where

import qualified Audio as A
import Primitive.Geometry


data ScrollIntent
  = ToMeet
  | ToMatch

slopeForIntent ToMeet  = 0
slopeForIntent ToMatch = 1


data BezierConfig
  = BezierConfig
    { playbackCursorPositionOf :: !A.SampleIndex
    , sampleRateOf :: !Int
    , scrollIntentOf :: !ScrollIntent
    , timeSpanOf :: !Int -- seconds
    , viewCursorPositionOf :: !A.SampleIndex
    , viewOffsetOf :: !A.SampleCount
    , viewSpanOf :: !A.SampleCount
    }


-- A Bézier curve is used to auto-scroll the view to the playback-cursor's
-- position, attaining the appropriate velocity when we reach it.
-- When going backwards, the velocity starts and ends at 0;
-- when going forwards, the velocity starts at 0 and ends matching the velcotiy
-- of the playback cursor.
-- The x-axis here is our time, which is relative to the starting position
-- of the playback cursor; time increments are created by adding multiples of
-- the sample rate to it.
-- The y-axis represents our desired position over time; in the beginning we
-- start at the current view position, and in the end we match the position of
-- the playback cursor, possibly minus some offset (so the cursor is located in
-- the middle of the screen after we scroll the view forward to it).
-- (When we're scrolling backwards, we just go to where the playback cursor will
-- be when it enters the screen on the left, and then we'll wait until it hits
-- the center of the screen to start scrolling with it again.)
-- The y-values of the two mid-points of the Bézier curve give us the starting
-- and ending velocities relative to the starting and ending points, resp. --
-- when the y-values of the mid-points are equal to those of the starting and
-- ending points, then the velocities will be zero.
-- In the case that we're matching the playback cursor's position when coming
-- from behind, the third point (2) will have a smaller y-value than the ending
-- point so that we'll match the velocity of the playback cursor when we reach
-- it as well.
--
-- For Bézier points 0-3: P_0 is the current view position, P_3 is the position
-- of the view when matcing the eventual position of the playback cursor:
--
-- Scrolling Forwards:
--
--    |
--    |         3
--    |      /
--    |    2
--    |    |
--    |    |
--    |    |
--   -0====1----------
--    |
--
--
-- Scrolling Backwards:
--
--    |
--   -0====1----------
--    |    |
--    |    |
--    |    2----3
--    |
--
makeBezier :: BezierConfig -> Float -> Coordinates
makeBezier config = bezier [p0,p1,p2,p3]
  where
    numSeconds = timeSpanOf config
    sampleRate = sampleRateOf config
    slopeToggle = slopeForIntent (scrollIntentOf config)
    initPCP = playbackCursorPositionOf config
    finalPCP = initPCP + sampleRate * numSeconds
    midPCP = initPCP + (finalPCP - initPCP) `div` 2
    initVCP = viewCursorPositionOf config
    finalVCP = finalPCP + viewOffsetOf config
    p0 = Coordinates initPCP initVCP
    p1 = Coordinates midPCP  initVCP
    -- a slopeToggle of 1 here gives us a matching velocity
    -- a slopeToggle of 0 gives us no velocity
    p2 = Coordinates midPCP  (finalVCP - slopeToggle * (finalPCP - midPCP))
    p3 = Coordinates finalPCP finalVCP

plus :: Coordinates -> Coordinates -> Coordinates
(Coordinates xl yl) `plus` (Coordinates xr yr) = Coordinates (xl+xr) (yl+yr)
infixl 6 `plus`

times :: Float -> Coordinates -> Coordinates
c `times` (Coordinates x y) =
  Coordinates (round (c * fromIntegral x)) (round (c * fromIntegral y))
infixl 7 `times`

-- https://en.wikipedia.org/wiki/Bézier_curve
-- a nice visualization:
-- https://tholman.com/bezier-curve-simulation/
bezier :: [Coordinates] -> Float -> Coordinates
bezier [p0,p1,p2] t = -- quadratic
  (1-t)^2 `times` p0 `plus`
  2 * t * (1-t) `times` p1 `plus`
  t^2 `times` p2
bezier [p0,p1,p2,p3] t = -- cubic
  (1-t)^3 `times` p0 `plus`
  3 * t * (1-t)^2 `times` p1 `plus`
  3 * t^2 * (1-t) `times` p2 `plus`
  t^3 `times` p3

