{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE MultiWayIf #-}

module Screens.Editing.Presenter.DynamicWaveform
 ( DynamicWaveform
 , dynamicWaveform
 ) where

import Control.Applicative
import Control.DeepSeq
import Data.Maybe
import FULE hiding (boundsOf)
import GHC.Generics (Generic)

import Audio
import Graphics.Renderable
import Primitive.Geometry
import qualified Screens.Editing.Messages as M
import Screens.Editing.Model.State hiding (State)
import Screens.Editing.Presenter.PrecisionCursor.ViewModel
import qualified Screens.Editing.Presenter.ViewModel as VM
import Screens.Editing.Presenter.Waveform.ViewModel
import qualified Screens.Editing.View.Waveform as View
import qualified Screens.Editing.View.Waveform.ViewModel as View
import Screens.Shared.Presenter.InputContext
import Screens.Shared.Presenter.Reactor hiding (outputForView)
import Screens.Shared.Presenter.State
import Screens.Shared.UserInput


-- IMPORTANT NOTE:
-- The term "cursor" in this module refers to a reference within the audio,
-- as implemented in Audio.Cursor, not to the mouse pointer.

type WVM = View.WaveformViewModel

data DynamicState
  = AdjustmentIdle
  | AdjustmentDragging
  | NormalIdle
  | NormalPrimed
  | NormalDragging
  | OperationPrimed { samplesFromViewStartOf :: !SampleOffset }
  | OperationDragging { samplesFromViewStartOf :: !SampleOffset }
  | PrecisionIdle
  | PrecisionPlayback
  | PrecisionOperation
  deriving (Eq, Generic, NFData)

toPrecisionCursorState :: DynamicState -> PrecisionCursorState
toPrecisionCursorState PrecisionIdle = PrecisionCursorIdle
toPrecisionCursorState PrecisionPlayback = PrecisionCursorActivating
toPrecisionCursorState PrecisionOperation = PrecisionCursorModifying
toPrecisionCursorState _ = PrecisionCursorNone

nextState
  :: DynamicState -> InputContext -> Rectangle -> WVM
  -> DynamicState
nextState AdjustmentIdle ic bounds _
  | anyCtrlEvent ic && not (anyCtrlHeld ic) && altHeld ic = PrecisionIdle
  | anyCtrlEvent ic && not (anyCtrlHeld ic) = NormalIdle
  | keyDownEvent MetaAlt ic = PrecisionIdle
  | keyUpEvent MetaAlt ic = AdjustmentIdle
  | mouseInBounds bounds ic && leftPressEvent ic = AdjustmentDragging
nextState AdjustmentDragging ic _ _
  | anyCtrlEvent ic && not (anyCtrlHeld ic) && altHeld ic = PrecisionPlayback
  | anyCtrlEvent ic && not (anyCtrlHeld ic) = NormalDragging
  | keyDownEvent MetaAlt ic = PrecisionPlayback
  | keyUpEvent MetaAlt ic = AdjustmentDragging
  | interruptingInputEvent ic = AdjustmentIdle
nextState NormalIdle ic bounds wvm
  | anyCtrlHeld ic = AdjustmentIdle
  | altHeld ic = PrecisionIdle
  | mouseInBounds bounds ic && leftPressEvent ic = NormalPrimed
  | mouseInBounds bounds ic && rightPressEvent ic = OperationPrimed $
      fromJust (samplesFromViewStart bounds wvm ic) -- fromJust OK
nextState NormalPrimed ic _ _
  | anyCtrlEvent ic && anyCtrlHeld ic = AdjustmentDragging
  | anyCtrlEvent ic = NormalPrimed
  | keyDownEvent MetaAlt ic = PrecisionPlayback
  | keyUpEvent MetaAlt ic = NormalPrimed
  | mouseMovementEvent ic = NormalDragging
  | interruptingInputEvent ic = NormalIdle
nextState NormalDragging ic _ _
  | anyCtrlEvent ic && anyCtrlHeld ic = AdjustmentDragging
  | anyCtrlEvent ic = NormalDragging
  | keyDownEvent MetaAlt ic = PrecisionPlayback
  | keyUpEvent MetaAlt ic = NormalDragging
  | interruptingInputEvent ic = NormalIdle
nextState state@(OperationPrimed s) ic _ _
  | anyCtrlEvent ic && anyCtrlHeld ic = AdjustmentIdle
  | anyCtrlEvent ic = state
  | keyDownEvent MetaAlt ic = PrecisionOperation
  | keyUpEvent MetaAlt ic = state
  | mouseMovementEvent ic = OperationDragging s
  | interruptingInputEvent ic = NormalIdle
nextState state@OperationDragging{} ic _ _
  | anyCtrlEvent ic && anyCtrlHeld ic = AdjustmentIdle
  | anyCtrlEvent ic = state
  | keyDownEvent MetaAlt ic = PrecisionOperation
  | keyUpEvent MetaAlt ic = state
  | interruptingInputEvent ic = NormalIdle
nextState PrecisionIdle ic bounds _
  | anyCtrlEvent ic && anyCtrlHeld ic = AdjustmentIdle
  | keyUpEvent MetaAlt ic && anyCtrlHeld ic = AdjustmentIdle
  | keyUpEvent MetaAlt ic = NormalIdle
  | mouseInBounds bounds ic && leftPressEvent ic = PrecisionPlayback
  | mouseInBounds bounds ic && rightPressEvent ic = PrecisionOperation
nextState PrecisionPlayback ic _ _
  | anyCtrlEvent ic && anyCtrlHeld ic = AdjustmentDragging
  | anyCtrlEvent ic = PrecisionPlayback
  | keyUpEvent MetaAlt ic && anyCtrlHeld ic = AdjustmentDragging
  | keyUpEvent MetaAlt ic = NormalDragging
  | interruptingInputEvent ic = PrecisionIdle
nextState PrecisionOperation ic _ _
  | anyCtrlEvent ic && anyCtrlHeld ic = AdjustmentIdle
  | anyCtrlEvent ic = PrecisionOperation
  | keyUpEvent MetaAlt ic && anyCtrlHeld ic = AdjustmentIdle
  | keyUpEvent MetaAlt ic = NormalIdle
  | interruptingInputEvent ic = PrecisionIdle
nextState state _ _ _ = state

samplesFromViewStart
  :: Rectangle -> WVM -> InputContext -> Maybe SampleOffset
samplesFromViewStart bounds wvm ic =
  if mouseInBounds bounds ic
  then do
    let Rectangle (Coordinates x _) _ = bounds
    mx <- xOf <$> locationStateOf (mouseStateOf ic)
    View.pixelOffsetToSampleOffset (mx - x) wvm
  else Nothing

transitionOutputForModel
  :: InputContext -> Rectangle -> Maybe SampleOffset -> Segments
  -> DynamicState -> DynamicState
  -> Maybe M.PresenterToModel
transitionOutputForModel _ _ _ _ s1 s2 | s1 == s2 = Nothing
transitionOutputForModel ic b sc _ _ AdjustmentDragging =
  M.PerformAction . startGapExpansion <$> sc
transitionOutputForModel ic b sc _ AdjustmentDragging _ =
  if (mouseInBounds b ic && leftReleaseEvent ic)
       || (anyCtrlEvent ic && not (anyCtrlHeld ic))
       || keyDownEvent MetaAlt ic
  then return . M.PerformAction $ completeGapExpansion
  else return . M.PerformAction $ abortGapExpansion
transitionOutputForModel ic b sc segments NormalPrimed NormalIdle = do
  sm <- sc
  segment <- View.segmentUnderSampleOffset sm segments
  if | mouseInBounds b ic && leftReleaseEvent ic && isClip segment ->
        return . (`M.PerformActions` M.StartPlayback) $
          setPlaybackCursorToMarkingCursor . setMarkingCursorToClipInViewAt sm
     | mouseInBounds b ic && leftReleaseEvent ic && isGap segment ->
        M.PerformAction . setMarkingCursorInViewTo <$> sc
     | otherwise -> Nothing
transitionOutputForModel ic b sc _ OperationPrimed{} _ =
  if mouseInBounds b ic && rightReleaseEvent ic
  then M.PerformAction . removeInvolving <$> sc
  else Nothing
transitionOutputForModel ic b sc _ (OperationDragging sfvs) _ =
  if mouseInBounds b ic && rightReleaseEvent ic
  then M.PerformAction . removeInvolvingSpan sfvs <$> sc
  else Nothing
transitionOutputForModel ic b sc segments PrecisionPlayback PrecisionIdle = do
  sm <- sc
  if mouseInBounds b ic && leftReleaseEvent ic && sm < numSamplesIn segments
  then do
    return . (`M.PerformActions` M.StartPlayback) $
      setPlaybackCursorToMarkingCursor . setMarkingCursorInViewTo sm
  else Nothing
transitionOutputForModel ic b sc segments PrecisionOperation PrecisionIdle =
  if mouseInBounds b ic && rightReleaseEvent ic
  then do
    sm <- sc
    segment <- View.segmentUnderSampleOffset sm segments
    return . M.PerformAction $
      if isClip segment then splitClip sm else truncateGap sm
  else Nothing
transitionOutputForModel _ _ _ _ _ _ = Nothing

stateOutputForModel
  :: InputContext -> Rectangle -> Maybe SampleOffset -> WVM -> DynamicState
  -> Maybe M.PresenterToModel
stateOutputForModel ic _ sc wvm NormalDragging = do
  dx <- deltaXOf <$> mouseMovementData ic
  return . M.PerformAction . moveViewCursorBy $ View.samplesToShift dx wvm
stateOutputForModel ic b sc wvm AdjustmentDragging = do
  mx <- xOf <$> locationStateOf (mouseStateOf ic)
  sm <- sc
  dx <- deltaXOf <$> mouseMovementData ic
  let x = xOf (coordinatesOf b)
  dm <- View.pixelOffsetToSampleOffset (mx - x - dx) wvm
  return . M.PerformAction $ continueGapExpansion (sm - dm)
stateOutputForModel _ _ _ _ _ = Nothing

outputForView
  :: DynamicState -> Maybe SampleOffset -> SampleOffset -> Segments
  -> View.Interactivity
outputForView AdjustmentIdle _ _ _ = View.NoInteractivity
outputForView AdjustmentDragging _ _ _ = View.NoInteractivity -- TODO highlight clip/gap
outputForView NormalIdle sc _ _ = maybe View.NoInteractivity View.Hovering sc
outputForView NormalPrimed sc _ _ = maybe View.NoInteractivity View.Activating sc
outputForView NormalDragging _ _ _ = View.NoInteractivity
outputForView (OperationPrimed samplesFromViewStart1) sc delta segments =
  if isClip (segmentAtCursor cursor segments)
  then getRemovalSelection segments samplesFromViewStart1 sc
  else getRemovalSelection segments delta sc
  where
    beginningCursor = cursorAtFirstSample segments
    cursor = advanceCursorInBounds samplesFromViewStart1 beginningCursor segments
outputForView (OperationDragging samplesFromViewStart1) sc _ segments =
  getRemovalSelection segments samplesFromViewStart1 sc
outputForView PrecisionIdle _ _ _ = View.NoInteractivity
outputForView PrecisionPlayback _ _ _ = View.NoInteractivity
outputForView PrecisionOperation sc _ segments =
  fromMaybe View.NoInteractivity do
    sm <- sc
    sg <- View.segmentUnderSampleOffset sm segments
    return
      if isClip sg
      then View.NoInteractivity -- TODO highlight somehow
      else View.Truncating sm

getRemovalSelection
  :: Segments -> SampleOffset -> Maybe SampleOffset -> View.Interactivity
getRemovalSelection segments samplesFromViewStart1 sc =
  fromMaybe View.NoInteractivity do
    sm <- sc
    let beginningCursor = cursorAtFirstSample segments
    let advancedCursor =
          advanceCursorInBounds samplesFromViewStart1 beginningCursor segments
    let cursor1 = if samplesFromViewStart1 < 0 then beginningCursor else advancedCursor
    let cursor2 = advanceCursorInBounds sm beginningCursor segments
    (c1, c2) <- arrangeForRemovalHighlight cursor1 cursor2 segments
    let samples1 = numSamplesBetween beginningCursor c1 segments
    let samples2 = numSamplesBetween beginningCursor c2 segments
    return (View.RemovingBetween samples1 samples2)

arrangeForRemovalHighlight
  :: Cursor -> Cursor -> Segments -> Maybe (Cursor, Cursor)
arrangeForRemovalHighlight cursor1 cursor2 segments =
  if pointToSameSegment cursor1 cursor2 && isGap (segmentAtCursor cursor1 segments)
  then Nothing
  else Just
      ( if isGap (segmentAtCursor cursor1' segments)
        then fromMaybe (cursorAtLastSample segments)
          (moveToBeginningOfNextSegment cursor1' segments)
        else cursor1'
      , if isClip (segmentAtCursor cursor2' segments)
        then
          -- special case where trailing gap is 0-length;
          -- it was highlighting the following clip erroneously
          -- (because of the use of `numSamplesBetween` in the
          -- function above)
          if lengthOf (segmentAtCursor c segments) == 0 then cursor2' else c
        else cursor2'
      )
  where
    (cursor1', cursor2') = order cursor1 cursor2
    c = fromMaybe cursor2' $ moveToBeginningOfNextSegment cursor2' segments


data DynamicWaveform
  = DynamicWaveform
    { boundsOf :: !Rectangle
    , cursorDeltaOf :: !SampleOffset
    , dynamicStateOf :: !(State DynamicState)
    , inputContextOf :: !InputContext
    , segmentsOf :: !Segments
    , viewStartOf :: !SampleIndex
    , waveformRepresentationOf :: !View.WaveformRepresentation
    , waveformViewModelOf :: !WVM
    }
  deriving (Generic, NFData)

instance Reaction DynamicWaveform
  (RInput M.ModelToPresenter VM.ViewModelRecord)
  (ROutput M.PresenterToModel po VM.ViewModelRecord)
  where
  addReactant (LayoutInput li bi) _ w = w { boundsOf = makeBoundsRect li bi }
  addReactant (ModelInput mi) _ w =
    w
    { cursorDeltaOf = M.markingCursorPositionOf mi - M.viewCursorPositionOf mi
    , segmentsOf = M.segmentsInViewOf mi
    , viewStartOf = M.viewCursorPositionOf mi
    , waveformRepresentationOf = M.waveformRepresentationOf mi
    }
  addReactant (ViewInput vi) _ w =
    w
    { dynamicStateOf = transition state if ns /= cs then Just ns else Nothing
    , inputContextOf = vi
    }
    where
      DynamicWaveform
        { boundsOf = bounds
        , dynamicStateOf = state
        , waveformViewModelOf = viewModel
        } = w
      cs = currentState state
      ns = nextState cs vi bounds viewModel
  addReactant (ViewModelInput (VM.DynamicWaveform vmi)) _ w = w { waveformViewModelOf = vmi }
  addReactant _ _ w = w
  getProduct _ ModelOutput w =
    onTransition (transitionOutputForModel ic bounds sfvs segments) state
    <|> stateOutputForModel ic bounds sfvs viewModel (currentState state)
    where
      DynamicWaveform
        { boundsOf = bounds
        , dynamicStateOf = state
        , inputContextOf = ic
        , segmentsOf = segments
        , waveformViewModelOf = viewModel
        } = w
      sfvs = samplesFromViewStart bounds viewModel ic
  getProduct _ ViewOutput w =
    Just $ Rendering View.Waveform
    { View.interactivityOf = outputForView (currentState state) sfvs delta segments
    , View.segmentsOf = segments
    , View.viewStartOf = viewStart
    , View.waveformRepresentationOf = waveRep
    , View.waveformViewModelOf = viewModel
    }
    where
      DynamicWaveform
        { boundsOf = bounds
        , cursorDeltaOf = delta
        , dynamicStateOf = state
        , inputContextOf = ic
        , segmentsOf = segments
        , viewStartOf = viewStart
        , waveformRepresentationOf = waveRep
        , waveformViewModelOf = viewModel
        } = w
      sfvs = samplesFromViewStart bounds viewModel ic
  getProduct _ ViewModelOutput w =
    Just . VM.PrecisionCursorState . toPrecisionCursorState . currentState
    $ dynamicStateOf w
  getProduct _ _ _ = Nothing

dynamicWaveform =
  DynamicWaveform
  { boundsOf = zeroRect
  , cursorDeltaOf = 0
  , dynamicStateOf = initialState NormalIdle
  , inputContextOf = defaultInputContext
  , segmentsOf = emptySegments
  , viewStartOf = 0
  , waveformRepresentationOf = View.emptyWaveformRepresentation
  , waveformViewModelOf = View.emptyWaveformViewModel
  }

