{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Screens.Editing.Presenter.Waveform.ViewModel
 ( WaveformViewModel
 , WaveformView(..)
 , waveformViewModel
 )where

import Control.DeepSeq
import FULE hiding (boundsOf)
import GHC.Generics (Generic)

import Audio
import Primitive.Geometry
import Screens.Editing.Messages
import Screens.Editing.Presenter.ViewModel
import Screens.Editing.View.Waveform.Representation
import qualified Screens.Editing.View.Waveform.ViewModel as View
import Screens.Shared.Presenter.Reactor


data WaveformView = Dynamic | Full
  deriving (Eq, Generic, NFData, Show)

data WaveformViewModel
  = WaveformViewModel
    { boundsOf :: !Rectangle
    , numSamplesOf :: !SampleCount
    , startingIndexOf :: !SampleIndex
    , viewOf :: !WaveformView
    }
  deriving (Generic, NFData)

instance Reaction WaveformViewModel
  (RInput ModelToPresenter ViewModelRecord)
  (ROutput mo po ViewModelRecord)
  where
  addReactant (LayoutInput layout bounds) _ wvm =
    wvm { boundsOf = makeBoundsRect layout bounds }
  addReactant (ModelInput mi) _ wvm =
    case viewOf wvm of
      Dynamic ->
        wvm { numSamplesOf = viewSpanOf mi, startingIndexOf = viewCursorPositionOf mi }
      Full ->
        wvm { numSamplesOf = sampleCountOf mi, startingIndexOf = 0 }
  addReactant _ _ wvm = wvm
  getProduct _ ViewModelOutput wvm =
    Just . viewModelConstructor $ View.makeWaveformViewModel width start num chunkSize
    where
      WaveformViewModel
        { boundsOf = bounds
        , numSamplesOf = num
        , startingIndexOf = start
        , viewOf = view
        } = wvm
      width = widthOf (dimensionsOf bounds)
      viewModelConstructor =
        case view of
          Dynamic -> DynamicWaveform
          Full -> FullWaveform
  getProduct _ _ _ = Nothing

waveformViewModel :: WaveformView -> WaveformViewModel
waveformViewModel = WaveformViewModel zeroRect 0 0

