module Screens.Editing.Presenter.ViewModel where

import Screens.Editing.Presenter.PrecisionCursor.ViewModel
import Screens.Editing.View.Waveform.ViewModel


data ViewModelRecord
  = DynamicWaveform !WaveformViewModel
  | FullWaveform !WaveformViewModel
  | PrecisionCursorState !PrecisionCursorState

