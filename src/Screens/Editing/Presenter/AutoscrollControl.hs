{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE MultiWayIf #-}

module Screens.Editing.Presenter.AutoscrollControl
 ( AutoscrollControl
 , autoscrollControl
 ) where

import Control.DeepSeq
import FULE
import GHC.Generics (Generic)

import qualified Audio as A
import Primitive.Geometry
import qualified Screens.Editing.Messages as M
import Screens.Editing.Model.State
import qualified Screens.Editing.Presenter.AutoscrollControl.BezierCurve as B
import Screens.Shared.Presenter.InputContext
import Screens.Shared.Presenter.Reactor


data AutoscrollState
  = Delay
  | Idle
  | Matching
  | Primed
  | Scrolling
    { percentToCoordsOf :: !(Float -> Coordinates)
    , sampleToPercentOf :: !(A.SampleIndex -> Float)
    , successorOf :: !AutoscrollState
    }
  | Waiting
  deriving (Generic, NFData)

backwardTravelTime = 2 -- number of seconds to take to find backwards
forwardTravelTime = 3 -- number of seconts to take to match forwards

nextState :: AutoscrollState -> AutoscrollControl -> AutoscrollState
nextState Idle ac | shouldScrollIn ac = Delay
nextState Idle _ = Idle
nextState Delay ac | shouldScrollIn ac = Primed
nextState Delay _ = Idle
nextState Primed ac =
  case (shouldScrollIn ac, playbackCursorPositionOf ac) of
    (True, Just p) ->
      if | p+btt*sampleRate < vcp -> scrolling ac p btt B.ToMeet 0
         | p < vcp + half -> Waiting
         | otherwise      -> scrolling ac p forwardTravelTime B.ToMatch (-half)
    _ -> Idle
  where
    btt = backwardTravelTime
    half = halfView ac
    sampleRate = sampleRateOf ac
    vcp = viewCursorPositionOf ac
nextState Waiting ac =
  case (shouldScrollIn ac, playbackCursorPositionOf ac) of
    (True, Just p) ->
      if | p > vcp + halfView ac -> Matching
         | p+btt*sampleRate < vcp -> scrolling ac p backwardTravelTime B.ToMeet 0
         | otherwise -> Waiting
    _ -> Idle
  where
    btt = backwardTravelTime
    sampleRate = sampleRateOf ac
    vcp = viewCursorPositionOf ac
nextState state@Scrolling{} ac =
  case (shouldScrollIn ac, playbackCursorPositionOf ac) of
    (True, Just p) ->
      if sampleToPercentOf state p > 1
      then successorOf state
      else state
    _ -> Idle
nextState Matching ac | shouldScrollIn ac = Matching
nextState Matching _ = Idle

stateForIntent B.ToMeet  = Waiting
stateForIntent B.ToMatch = Matching

scrolling ac cursorPosition time intent offset =
  Scrolling
  { percentToCoordsOf = percentToCoords
  , sampleToPercentOf = sampleToPercent
  , successorOf = successor
  }
  where
    config =
      B.BezierConfig
      { B.playbackCursorPositionOf = cursorPosition
      , B.sampleRateOf = sampleRateOf ac
      , B.scrollIntentOf = intent
      , B.timeSpanOf = time
      , B.viewCursorPositionOf = viewCursorPositionOf ac
      , B.viewOffsetOf = offset
      , B.viewSpanOf = viewSpanOf ac
      }
    percentToCoords = B.makeBezier config
    sampleToPercent p =
      fromIntegral (p - B.playbackCursorPositionOf config) /
      fromIntegral (B.timeSpanOf config * B.sampleRateOf config)
    successor = stateForIntent (B.scrollIntentOf config)

outputForState :: AutoscrollState -> AutoscrollControl -> Maybe M.PresenterToModel
outputForState Idle _ = Nothing
outputForState Delay _ = Nothing
outputForState Primed _ = Nothing
outputForState Waiting ac =
  case (shouldScrollIn ac, playbackCursorPositionOf ac) of
    (True, Just p) ->
      if p > viewCursorPositionOf ac + halfView ac
      then scrollViewTo (p - halfView ac)
      else Nothing
    _ -> Nothing
outputForState state@Scrolling{} ac =
  case (shouldScrollIn ac, playbackCursorPositionOf ac) of
    (True, Just p) ->
      scrollViewTo (yOf (percentToCoordsOf state (sampleToPercentOf state p)))
    _ -> Nothing
outputForState Matching ac =
  case (shouldScrollIn ac, playbackCursorPositionOf ac) of
    (True, Just p) -> scrollViewTo (p - halfView ac)
    _ -> Nothing

-- TODO disable scrolling if end of file is on screen?
-- might mess with zooming if we don't
shouldScrollIn :: AutoscrollControl -> Bool
shouldScrollIn ac = not (inputHeldOf ac) && isPlayingOf ac

scrollViewTo = Just . M.PerformAction . moveViewCursorTo

halfView = (`div` 2) . viewSpanOf


data AutoscrollControl
  = AutoscrollControl
    { autoscrollStateOf :: !AutoscrollState
    , inputHeldOf :: !Bool
    , isPlayingOf :: !Bool
    , playbackCursorPositionOf :: !(Maybe A.SampleIndex)
    , sampleRateOf :: !Int
    , viewCursorPositionOf :: !A.SampleIndex
    , viewSpanOf :: !A.SampleCount
    }
  deriving (Generic, NFData)

instance Reaction AutoscrollControl
  (RInput M.ModelToPresenter vm)
  (ROutput M.PresenterToModel po vm)
  where
  addReactant (ModelInput mi) _ ac =
    ac' { autoscrollStateOf = nextState (autoscrollStateOf ac') ac' }
    where
      ac' =
        ac
        { isPlayingOf = M.isPlayingOf mi
        , playbackCursorPositionOf = force $ M.playbackCursorPositionOf mi
        , sampleRateOf = M.sampleRateOf mi
        , viewCursorPositionOf = M.viewCursorPositionOf mi
        , viewSpanOf = M.viewSpanOf mi
        }
  addReactant (ViewInput ic) _ ac = ac { inputHeldOf = autoscrollInterruptingInputHeld ic }
  addReactant _ _ ac = ac
  getProduct _ ModelOutput ac = outputForState (autoscrollStateOf ac) ac
  getProduct _ _ _ = Nothing

autoscrollControl :: AutoscrollControl
autoscrollControl = AutoscrollControl Idle False False Nothing 0 0 0

