{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Editing.Presenter.SampleRate
 ( SampleRate
 , sampleRate
 ) where

import Control.DeepSeq
import Control.Monad.Reader
import FULE
import GHC.Generics (Generic)

import Config
import Graphics.Context
import Graphics.Renderable
import Screens.Editing.Messages
import qualified Screens.Editing.View.SampleRate as View
import Screens.Shared.Presenter.Reactor


newtype SampleRate = SampleRate View.SampleRate
  deriving (Generic, NFData)

sampleRate :: SampleRate
sampleRate = SampleRate (View.SampleRate 0)

instance Reaction SampleRate (RInput ModelToPresenter vm) (ROutput mo po vm) where
  addReactant (ModelInput status) _ _ =
    SampleRate . View.SampleRate $ sampleRateOf status
  addReactant _ _ state = state
  getProduct _ ViewOutput (SampleRate s) = Just (Rendering s)
  getProduct _ _ _ = Nothing

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r, HasStyle r)
  => Component SampleRate m where
  requiredWidth _ = requiredWidth (View.SampleRate 0)
  requiredHeight _ = requiredHeight (View.SampleRate 0)

