{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Screens.Editing.Presenter.Cursor
 ( Cursor
 , CursorType(..)
 , cursor
 ) where

import Control.DeepSeq
import FULE
import GHC.Generics (Generic)

import Audio hiding (Cursor)
import Graphics.Renderable
import qualified Screens.Editing.Messages as M
import Screens.Editing.Presenter.ViewModel
import Screens.Editing.Presenter.Waveform.ViewModel
import qualified Screens.Editing.View.Cursor as View
import qualified Screens.Editing.View.Waveform.ViewModel as View
import Screens.Shared.Presenter.Reactor


data CursorType
  = MarkingCursor
  | PlaybackCursor
  deriving (Generic, NFData)

data Cursor
  = Cursor
    { cursorPositionOf :: !(Maybe SampleIndex)
    , cursorTypeOf :: !CursorType
    , isPlayingOf :: !Bool
    , viewModelOf :: !View.WaveformViewModel
    , viewModelTypeOf :: !WaveformView
    }
  deriving (Generic, NFData)

instance Reaction Cursor
  (RInput M.ModelToPresenter ViewModelRecord)
  (ROutput mo po ViewModelRecord)
  where
  addReactant (ModelInput mi) _ cursor =
    cursor
    { cursorPositionOf = force $ getCursorPosition cursor  mi
    , isPlayingOf = M.isPlayingOf mi
    }
  addReactant (ViewModelInput (DynamicWaveform vmi)) _ cu@(Cursor { viewModelTypeOf = Dynamic }) =
    cu { viewModelOf = vmi }
  addReactant (ViewModelInput (FullWaveform vmi)) _ cu@(Cursor { viewModelTypeOf = Full }) =
    cu { viewModelOf = vmi }
  addReactant _ _ cu = cu
  getProduct _ ViewOutput cursor = cursorPositionOf cursor >>= \c -> Just . Rendering $
    View.Cursor
    { View.cursorPositionOf = c
    , View.cursorTypeOf = case cursorTypeOf cursor of
        MarkingCursor -> View.MarkingCursor
        PlaybackCursor ->
          if isPlayingOf cursor
          then View.PlaybackCursor View.PlaybackCursorActive
          else View.PlaybackCursor View.PlaybackCursorIdle
    , View.viewModelOf = viewModelOf cursor
    }
  getProduct _ _ _ = Nothing

getCursorPosition :: Cursor -> M.ModelToPresenter -> Maybe SampleIndex
getCursorPosition cursor = forView forType
  where
    forType =
      case cursorTypeOf cursor of
        MarkingCursor -> Just . M.markingCursorPositionOf
        PlaybackCursor -> M.playbackCursorPositionOf
    forView =
      case viewModelTypeOf cursor of
        Dynamic -> getCursorInView
        Full -> id

getCursorInView
  :: (M.ModelToPresenter -> Maybe SampleIndex) -> M.ModelToPresenter
  -> Maybe SampleIndex
getCursorInView f mi =
  case f mi of
    Just si -> if si >= vc && si < vc + vs then Just si else Nothing
    Nothing -> Nothing
  where
    M.Status { M.viewCursorPositionOf = vc, M.viewSpanOf = vs } = mi

cursor :: CursorType -> WaveformView -> Cursor
cursor cursorType = Cursor Nothing cursorType False View.emptyWaveformViewModel

