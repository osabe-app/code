{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Screens.Editing.Presenter.ViewWindow where

import Control.Applicative
import Control.DeepSeq
import Data.Bool
import Data.Functor
import Data.Maybe
import FULE hiding (boundsOf, offsetOf)
import GHC.Generics (Generic)

import Audio
import Graphics.Renderable
import Primitive.Geometry
import qualified Screens.Editing.Messages as M
import Screens.Editing.Model.State
import Screens.Editing.Presenter.ViewModel
import Screens.Editing.Presenter.Waveform.ViewModel
import qualified Screens.Editing.View.ViewWindow as View
import qualified Screens.Editing.View.Waveform.ViewModel as View
import Screens.Shared.Presenter.InputContext
import Screens.Shared.Presenter.Reactor


data ViewWindow
  = ViewWindow
    { boundsOf :: !Rectangle
    , mouseInBoundsOf :: !Bool
    , mousePositionOf :: !(Maybe SampleIndex)
    , offsetOf :: !(Maybe SampleCount)
    , waveformViewModelOf :: !View.WaveformViewModel
    , windowSpanOf :: !SampleCount
    , windowStartOf :: !SampleIndex
    }
  deriving (Generic, NFData)

instance Reaction ViewWindow
  (RInput M.ModelToPresenter ViewModelRecord)
  (ROutput M.PresenterToModel po ViewModelRecord)
  where
  addReactant (LayoutInput li bi) _ vw = vw { boundsOf = makeBoundsRect li bi }
  addReactant (ModelInput mi) _ vw =
    vw
    { windowSpanOf = M.viewSpanOf mi
    , windowStartOf = M.viewCursorPositionOf mi
    }
  addReactant (ViewInput ic) _ vw =
    vw
    { mouseInBoundsOf = mouseInBounds bounds ic
    , mousePositionOf = position
    , offsetOf = offset
    }
    where
      offset
        | mouseInBounds bounds ic && leftPressEvent ic = calcOffset <$> position
        | interruptingInputEvent ic = Nothing
        | otherwise = offsetOf vw
      position = do
        let (Rectangle (Coordinates x _) _) = boundsOf vw
        xOffset <- (\l -> max 0 $ xOf l - x) <$> locationStateOf (mouseStateOf ic)
        View.pixelOffsetToSample xOffset (waveformViewModelOf vw)
      ViewWindow { boundsOf = bounds, windowStartOf = vcp, windowSpanOf = vs } = vw
      calcOffset p = if inViewWindow vw p then p - vcp else vs `div` 2
  addReactant (ViewModelInput (FullWaveform vmi)) _ vw = vw { waveformViewModelOf = vmi }
  addReactant _ _ vw = vw
  getProduct _ ModelOutput vw = do
    mousePosition <- mousePositionOf vw
    offset <- offsetOf vw
    return . M.PerformAction . moveViewCursorTo $ mousePosition - offset
  getProduct _ ViewOutput vw = Just . Rendering $
    View.ViewWindow
    { View.viewWindowStateOf = fromJust $ -- fromJust OK
        offsetOf vw $> View.ViewWindowActive
        <|> fmap stateForHover (mousePositionOf vw)
        <|> Just View.ViewWindowNeutral -- makes fromJust OK
    , View.waveformViewModelOf = waveformViewModelOf vw
    , View.windowSpanOf = windowSpanOf vw
    , View.windowStartOf = windowStartOf vw
    }
    where
      stateForHover =
        bool View.ViewWindowNeutral View.ViewWindowHover
        . (mouseInBoundsOf vw &&) . inViewWindow vw
  getProduct _ _ _ = Nothing

inViewWindow :: ViewWindow -> SampleIndex -> Bool
inViewWindow (ViewWindow { windowStartOf = vcp, windowSpanOf = vs }) p =
  vcp <= p && p < vcp+vs

viewWindow :: ViewWindow
viewWindow =
  ViewWindow
  { boundsOf = zeroRect
  , mouseInBoundsOf = False
  , mousePositionOf = Nothing
  , offsetOf = Nothing
  , waveformViewModelOf = View.emptyWaveformViewModel
  , windowSpanOf = 0
  , windowStartOf = 0
  }

