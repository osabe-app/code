{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Editing.Presenter.PlaybackStatus
 ( PlaybackStatus
 , playbackStatus
 ) where

import Control.DeepSeq
import Control.Monad.Reader
import FULE
import GHC.Generics (Generic)

import Config
import Graphics.Context
import Graphics.Renderable
import Screens.Editing.Messages
import qualified Screens.Editing.View.PlaybackStatus as View
import Screens.Shared.Presenter.Reactor


newtype PlaybackStatus = PlaybackStatus View.PlaybackStatus
  deriving (Generic, NFData)

playbackStatus :: PlaybackStatus
playbackStatus = PlaybackStatus View.Stopped

instance Reaction PlaybackStatus (RInput ModelToPresenter vm) (ROutput mo po vm) where
  addReactant (ModelInput status) _ _ =
    PlaybackStatus if isPlayingOf status then View.Playing else View.Stopped
  addReactant _ _ state = state
  getProduct _ ViewOutput (PlaybackStatus s) = Just (Rendering s)
  getProduct _ _ _ = Nothing

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r, HasStyle r)
  => Component PlaybackStatus m where
  requiredWidth _ = requiredWidth View.Stopped
  requiredHeight _ = requiredHeight View.Stopped

