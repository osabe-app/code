{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Editing.Presenter.FileStatus
 ( FileStatus
 , fileStatus
 ) where

import Control.DeepSeq
import Control.Monad.Reader
import FULE
import GHC.Generics (Generic)

import Config
import Graphics.Context
import Graphics.Renderable
import Screens.Editing.Messages
import qualified Screens.Editing.View.FileStatus as View
import Screens.Shared.Presenter.Reactor


newtype FileStatus = FileStatus View.FileStatus
  deriving (Generic, NFData)

fileStatus :: FileStatus
fileStatus = FileStatus View.Saved

instance Reaction FileStatus (RInput ModelToPresenter vm) (ROutput mo po vm) where
  addReactant (ModelInput Status { saveStatusOf = status }) _ _ =
    FileStatus case status of
      SaveErrored -> View.SaveErrored
      Saved       -> View.Saved
      Saving p    -> View.Saving p
      Unsaved     -> View.Unsaved
  addReactant _ _ state = state
  getProduct _ ViewOutput (FileStatus s) = Just (Rendering s)
  getProduct _ _ _ = Nothing

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r, HasStyle r)
  => Component FileStatus m where
  requiredWidth _ = requiredWidth View.Saved
  requiredHeight _ = requiredHeight View.Saved

