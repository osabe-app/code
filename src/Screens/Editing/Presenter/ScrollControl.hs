{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Screens.Editing.Presenter.ScrollControl
 ( ScrollControl
 , scrollControl
 ) where

import Control.DeepSeq
import FULE hiding (boundsOf)
import GHC.Generics (Generic)

import Audio
import Primitive.Geometry
import qualified Screens.Editing.Messages as M
import Screens.Editing.Model.Model
import qualified Screens.Editing.View.Waveform.ViewModel as WVM
import Screens.Shared.Presenter.InputContext
import Screens.Shared.Presenter.Reactor


data ScrollControl
  = ScrollControl
    { boundsOf :: !Rectangle
    , inputContextOf :: !InputContext
    , numSamplesInViewOf :: !SampleCount
    }
  deriving (Generic, NFData)

instance Reaction ScrollControl
  (RInput M.ModelToPresenter vm)
  (ROutput M.PresenterToModel po vm)
  where
  addReactant (LayoutInput li bi) _ sc = sc { boundsOf = makeBoundsRect li bi }
  addReactant (ModelInput mi) _ sc = sc { numSamplesInViewOf = M.viewSpanOf mi }
  addReactant (ViewInput vi) _ sc = sc { inputContextOf = vi }
  addReactant _ _ sc = sc
  getProduct _ ModelOutput sc =
    if mouseInBounds bounds ic
    then
      M.PerformAction
      . moveViewCursorBy
      . WVM.samplesToScroll nsiv
      . uncurry (-)
      <$> mouseScrollData ic
    else Nothing
    where
      ScrollControl
        { boundsOf = bounds
        , inputContextOf = ic
        , numSamplesInViewOf = nsiv
        } = sc
  getProduct _ _ _ = Nothing

scrollControl :: ScrollControl
scrollControl = ScrollControl zeroRect defaultInputContext 0

