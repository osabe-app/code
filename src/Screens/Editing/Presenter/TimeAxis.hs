{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Editing.Presenter.TimeAxis where

import Control.DeepSeq
import Control.Monad.Reader
import FULE
import GHC.Generics (Generic)

import Audio hiding (sampleRateOf)
import Graphics.Context
import Graphics.Renderable
import qualified Screens.Editing.Messages as M
import Screens.Editing.Presenter.ViewModel
import Screens.Editing.Presenter.Waveform.ViewModel
import qualified Screens.Editing.View.TimeAxis as View
import qualified Screens.Editing.View.Waveform.ViewModel as View
import Screens.Shared.Presenter.Reactor


data TimeAxis
  = TimeAxis
    { indexOfFirstDeadSampleOf :: !SampleIndex
    , sampleRateOf :: !Int
    , viewOf :: !WaveformView
    , waveformViewModelOf :: !View.WaveformViewModel
    }
  deriving (Generic, NFData)

instance Reaction TimeAxis
  (RInput M.ModelToPresenter ViewModelRecord)
  (ROutput mo po ViewModelRecord)
  where
  addReactant (ModelInput mi) _ ta =
    ta
    { indexOfFirstDeadSampleOf =
        if viewOf ta == Dynamic
        then M.viewCursorPositionOf mi + numSamplesIn (M.segmentsInViewOf mi)
        else numSamplesIn (M.allSegmentsOf mi)
    , sampleRateOf = M.sampleRateOf mi
    }
  addReactant (ViewModelInput (DynamicWaveform wvm)) _ ta@(TimeAxis { viewOf = Dynamic }) =
    ta { waveformViewModelOf = wvm }
  addReactant (ViewModelInput (FullWaveform wvm)) _ ta@(TimeAxis { viewOf = Full }) =
    ta { waveformViewModelOf = wvm }
  addReactant _ _ ta = ta
  getProduct _ ViewOutput ta = Just . Rendering $
    View.TimeAxis
    { View.indexOfFirstDeadSampleOf = indexOfFirstDeadSampleOf ta
    , View.sampleRateOf = sampleRateOf ta
    , View.waveformViewModelOf = waveformViewModelOf ta
    }
  getProduct _ _ _ = Nothing

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => Component TimeAxis m where
  requiredWidth _ = requiredWidth (View.TimeAxis 0 0 View.emptyWaveformViewModel)
  requiredHeight _ = requiredHeight (View.TimeAxis 0 0 View.emptyWaveformViewModel)

timeAxis :: WaveformView -> TimeAxis
timeAxis view = TimeAxis 0 0 view View.emptyWaveformViewModel

