{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Screens.Editing.Presenter.PrecisionCursor
 ( PrecisionCursor
 , precisionCursor
 ) where

import Control.DeepSeq
import FULE hiding (boundsOf)
import GHC.Generics (Generic)

import Audio hiding (Cursor)
import Graphics.Renderable
import qualified Screens.Editing.Messages as M
import Screens.Editing.Model.State
import Screens.Editing.Presenter.PrecisionCursor.ViewModel
import Screens.Editing.Presenter.ViewModel
import Screens.Editing.Presenter.Waveform.ViewModel
import qualified Screens.Editing.View.Cursor as View
import qualified Screens.Editing.View.Waveform.ViewModel as View
import Screens.Shared.Presenter.InputContext
import Screens.Shared.Presenter.Reactor
import Primitive.Geometry


data PrecisionCursor
  = PrecisionCursor
    { boundsOf :: !Rectangle
    , mouseXOf :: !(Maybe Int)
    , sampleCountOf :: !SampleCount
    , stateOf :: !PrecisionCursorState
    , viewModelOf :: !View.WaveformViewModel
    }
  deriving (Generic, NFData)

instance Reaction PrecisionCursor
  (RInput M.ModelToPresenter ViewModelRecord)
  (ROutput M.PresenterToModel po ViewModelRecord)
  where
  addReactant (LayoutInput li bi) _ cu = cu { boundsOf = makeBoundsRect li bi }
  addReactant (ModelInput mi) _ cu = cu { sampleCountOf = M.sampleCountOf mi }
  addReactant (ViewInput vi) _ cu@(PrecisionCursor { boundsOf = b, stateOf = pt }) =
    cu { mouseXOf =
      if mouseInBounds b vi
      then fmap xOf . locationStateOf $ mouseStateOf vi
      else Nothing
    }
  addReactant (ViewModelInput (DynamicWaveform vmi)) _ cu = cu { viewModelOf = vmi }
  addReactant (ViewModelInput (PrecisionCursorState s)) _ cu = cu { stateOf = s }
  addReactant _ _ cu = cu
  getProduct _ ViewOutput cu = do
    sampleIndex <- calcSampleIndex cu
    typ <- View.PrecisionCursor <$> forView (stateOf cu)
    return $ Rendering
      View.Cursor
      { View.cursorPositionOf = sampleIndex
      , View.cursorTypeOf = typ
      , View.viewModelOf = viewModelOf cu
      }
  getProduct _ _ _ = Nothing

calcSampleIndex :: PrecisionCursor -> Maybe SampleIndex
calcSampleIndex cu = do
  offset <- (\mx -> mx - x) <$> mouseX
  sampleIndex <- View.pixelOffsetToSample offset viewModel
  if sampleIndex < sampleCount then return sampleIndex else Nothing
  where
    PrecisionCursor
      { boundsOf = (Rectangle (Coordinates x _) _)
      , mouseXOf = mouseX
      , sampleCountOf = sampleCount
      , viewModelOf = viewModel
      } = cu

forView :: PrecisionCursorState -> Maybe View.PrecisionCursorState
forView PrecisionCursorActivating = Just View.PrecisionCursorActivating
forView PrecisionCursorIdle       = Just View.PrecisionCursorIdle
forView PrecisionCursorModifying  = Just View.PrecisionCursorModifying
forView PrecisionCursorNone       = Nothing

precisionCursor :: PrecisionCursor
precisionCursor =
  PrecisionCursor
  { boundsOf = zeroRect
  , mouseXOf = Nothing
  , sampleCountOf = 0
  , stateOf = PrecisionCursorNone
  , viewModelOf = View.emptyWaveformViewModel
  }

