{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Editing.Presenter.SampleType
 ( SampleType
 , sampleType
 ) where

import Control.DeepSeq
import Control.Monad.Reader
import FULE
import GHC.Generics (Generic)

import qualified Audio
import Config
import Graphics.Context
import Graphics.Renderable
import Screens.Editing.Messages
import qualified Screens.Editing.View.SampleType as View
import Screens.Shared.Presenter.Reactor


newtype SampleType = SampleType View.SampleType
  deriving (Generic, NFData)

sampleType :: SampleType
sampleType = SampleType (View.SampleType Audio.Word8)

instance Reaction SampleType (RInput ModelToPresenter vm) (ROutput mo po vm) where
  addReactant (ModelInput status) _ _ =
    SampleType . View.SampleType $ sampleTypeOf status
  addReactant _ _ state = state
  getProduct _ ViewOutput (SampleType s) = Just (Rendering s)
  getProduct _ _ _ = Nothing

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r, HasStyle r)
  => Component SampleType m where
  requiredWidth _ = requiredWidth (View.SampleType Audio.Word8)
  requiredHeight _ = requiredHeight (View.SampleType Audio.Word8)

