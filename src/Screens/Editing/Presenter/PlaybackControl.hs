{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE MultiWayIf #-}

module Screens.Editing.Presenter.PlaybackControl
 ( PlaybackControl
 , playbackControl
 ) where

import Control.DeepSeq
import FULE
import GHC.Generics (Generic)

import Screens.Editing.Messages hiding (isPlayingOf)
import qualified Screens.Editing.Messages as Model
import Screens.Editing.Model.State
import Screens.Shared.Presenter.InputContext
import Screens.Shared.Presenter.Reactor
import Screens.Shared.UserInput


data PlaybackControl
  = PlaybackControl
    { isCtrlHeldOf :: !Bool
    , isPlayingOf :: !Bool
    , isPrimedOf :: !Bool
    , shouldReactOf :: !Bool
    }
  deriving (Generic, NFData)

-- I forget why I decided to have playback start/stop on the key-up event.
--
-- TODO register with the OS to receive Media Keys and react to those too.
instance Reaction PlaybackControl
  (RInput ModelToPresenter vm)
  (ROutput PresenterToModel po vm)
  where
  addReactant (ModelInput mi) _ pc = pc { isPlayingOf = Model.isPlayingOf mi }
  addReactant (ViewInput vi) _ pc@(PlaybackControl { isPrimedOf = primed }) = pc
    { isCtrlHeldOf = anyCtrlHeld vi
    , isPrimedOf = keyDownEvent KeySpace vi ||
        if not (inputEvent vi) then primed
        else (anyCtrlEvent vi || mouseMovementEvent vi) && primed
    , shouldReactOf = primed && keyUpEvent KeySpace vi
    }
  addReactant _ _ pc = pc
  getProduct _ ModelOutput pc =
    if not (shouldReactOf pc)
    then Nothing
    else
      if | isPlayingOf pc ->
            Just (PerformAudioAction StopPlayback)
         | isCtrlHeldOf pc ->
            Just (PerformActions maybeInitPlaybackCursor StartPlayback)
         | otherwise ->
            Just (PerformActions setPlaybackCursorToMarkingCursor StartPlayback)
  getProduct _ _ _ = Nothing

playbackControl :: PlaybackControl
playbackControl = PlaybackControl False False False False

