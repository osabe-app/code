{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}

module Screens.Editing.Presenter.PrecisionCursor.ViewModel where

import Control.DeepSeq
import GHC.Generics (Generic)


data PrecisionCursorState
  = PrecisionCursorActivating
  | PrecisionCursorIdle
  | PrecisionCursorModifying
  | PrecisionCursorNone
  deriving (Eq, Generic, NFData)

