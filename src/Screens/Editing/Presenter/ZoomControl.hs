{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE MultiWayIf #-}

module Screens.Editing.Presenter.ZoomControl
 ( ZoomControl
 , zoomControl
 ) where

import Control.DeepSeq
import FULE hiding (boundsOf)
import GHC.Generics (Generic)

import Audio
import Primitive.Geometry
import qualified Screens.Editing.Messages as M
import Screens.Editing.Model.State
import qualified Screens.Editing.View.Waveform.ViewModel as WVM
import Screens.Shared.Presenter.InputContext
import Screens.Shared.Presenter.Reactor


data ZoomControl
  = ZoomControl
    { boundsOf :: !Rectangle
    , inputContextOf :: !InputContext
    , viewSpanOf :: !SampleCount
    }
  deriving (Generic, NFData)

instance Reaction ZoomControl
  (RInput M.ModelToPresenter vm)
  (ROutput M.PresenterToModel po vm)
  where
  addReactant (LayoutInput li bi) _ ctrl = ctrl { boundsOf = makeBoundsRect li bi }
  addReactant (ModelInput mi) _ ctrl = ctrl { viewSpanOf = M.viewSpanOf mi }
  addReactant (ViewInput vi) _ ctrl = ctrl { inputContextOf = vi }
  addReactant _ _ ctrl = ctrl
  getProduct _ ModelOutput ctrl =
    if mouseInBounds bounds ic
    then do
      (sx, sy) <- mouseScrollData ic
      c <- locationStateOf $ mouseStateOf ic
      return . M.PerformAction $
        moveViewCursorBy (WVM.samplesToScroll vs sx) .
        zoomView (c `ratioX` bounds)
          if | sy  < 0 -> 10/9
             | sy == 0 -> 1.0
             | sy  > 0 -> 9/10
    else Nothing
    where
      ZoomControl
        { boundsOf = bounds
        , inputContextOf = ic
        , viewSpanOf = vs
        } = ctrl
  getProduct _ _ _ = Nothing

zoomControl :: ZoomControl
zoomControl = ZoomControl zeroRect defaultInputContext 0

