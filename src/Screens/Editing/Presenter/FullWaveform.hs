{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Screens.Editing.Presenter.FullWaveform where

import Control.DeepSeq
import FULE
import GHC.Generics (Generic)

import Audio
import Graphics.Renderable
import qualified Screens.Editing.Messages as M
import qualified Screens.Editing.Presenter.ViewModel as VM
import Screens.Editing.Presenter.Waveform.ViewModel
import qualified Screens.Editing.View.Waveform as View
import qualified Screens.Editing.View.Waveform.ViewModel as View
import Screens.Shared.Presenter.Reactor


data FullWaveform
  = FullWaveform
    { segmentsOf :: !Segments
    , waveformRepresentationOf :: !View.WaveformRepresentation
    , waveformViewModelOf :: !View.WaveformViewModel
    }
  deriving (Generic, NFData)

instance Reaction FullWaveform
  (RInput M.ModelToPresenter VM.ViewModelRecord)
  (ROutput mo po VM.ViewModelRecord)
  where
  addReactant (ModelInput mi) _ w =
    w
    { segmentsOf = M.allSegmentsOf mi
    , waveformRepresentationOf = M.waveformRepresentationOf mi
    }
  addReactant (ViewModelInput (VM.FullWaveform vmi)) _ w = w { waveformViewModelOf = vmi }
  addReactant _ _ w = w
  getProduct _ ViewOutput w =
    Just $ Rendering View.Waveform
    { View.interactivityOf = View.NoInteractivity
    , View.segmentsOf = segmentsOf w
    , View.viewStartOf = 0
    , View.waveformRepresentationOf = waveformRepresentationOf w
    , View.waveformViewModelOf = waveformViewModelOf w
    }
  getProduct _ _ _ = Nothing

fullWaveform =
  FullWaveform
  { segmentsOf = emptySegments
  , waveformRepresentationOf = View.emptyWaveformRepresentation
  , waveformViewModelOf = View.emptyWaveformViewModel
  }

