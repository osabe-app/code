{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE MultiWayIf #-}

module Screens.Editing.Presenter.Divider
 ( Divider
 , divider
 ) where

import Control.DeepSeq
import Data.Functor
import Data.Maybe
import FULE hiding (boundsOf, offsetOf)
import GHC.Generics (Generic)

import Graphics.Renderable
import Primitive.Geometry
import qualified Screens.Editing.View.Divider as View
import Screens.Shared.Presenter.InputContext
import Screens.Shared.Presenter.Reactor


-- movement of the bar goes perpendicular to the orientation

data Divider
  = Divider
    { boundsOf :: !Rectangle
    , offsetOf :: !(Maybe Int)
    , movementOf :: !(Maybe Int)
    , orientationOf :: !Orientation
    , guideOf :: !GuideID
    }
  deriving (Generic, NFData)

instance Reaction Divider (RInput mi vm) (ROutput mo po vm) where
  addReactant (LayoutInput li bi) _ di = di { boundsOf = makeBoundsRect li bi }
  addReactant (ViewInput vi) _ di@(Divider { boundsOf = b, movementOf = m }) =
    if | mouseInBounds b vi && leftPressEvent vi ->
          di { offsetOf = offsetInBar vi di, movementOf = Just 0 }
       | leftReleaseEvent vi || (isNothing . locationStateOf $ mouseStateOf vi) ->
          di { offsetOf = Nothing, movementOf = Nothing }
       | mouseMovementEvent vi ->
          di { movementOf = movementOfBar vi di }
       | otherwise ->
          di { movementOf = m $> 0 }
  addReactant _ _ di = di
  getProduct _ LayoutOutput (Divider { guideOf = guide, movementOf = movement}) =
    reactToChange guide <$> movement
  getProduct _ ViewOutput _ = Just (Rendering View.Divider)
  getProduct _ _ _ = Nothing

offsetInBar :: InputContext -> Divider -> Maybe Int
offsetInBar vi di@(Divider { boundsOf = (Rectangle c _) }) =
  retrieve . getOffset c <$> locationStateOf (mouseStateOf vi)
  where
    retrieve = case orientationOf di of
      Horizontal -> deltaYOf
      Vertical   -> deltaXOf

movementOfBar :: InputContext -> Divider -> Maybe Int
movementOfBar vi di@(Divider { boundsOf = (Rectangle c _) }) = do
  mouse <- retrieve <$> locationStateOf (mouseStateOf vi)
  offset <- offsetOf di
  let loc = retrieve c + offset
  return (mouse - loc)
  where
    retrieve = case orientationOf di of
      Horizontal -> yOf
      Vertical   -> xOf

divider :: Orientation -> GuideID -> Divider
divider = Divider zeroRect Nothing Nothing

