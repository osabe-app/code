{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}

module Screens.Editing.Messages
 ( PresenterToModel(..)
 , AudioAction(..)
 , ModelToPresenter(..)
 , SaveStatus(..)
 , currentlySaving
 ) where

import Control.DeepSeq
import Control.Exception
import GHC.Generics (Generic)

import Audio
import Screens.Editing.Model.State
import Screens.Editing.View.Waveform.Representation
import Screens.Shared.Output


data PresenterToModel
  = Monitor
  | PerformAction !(State -> State)
  | PerformActions !(State -> State) !AudioAction
  | PerformAudioAction !AudioAction
  | Save
  | Stop !Output
  deriving (Generic, NFData)

instance Semigroup PresenterToModel where
  a <> b =
    case (a, b) of
      (Save, _) -> Save
      (_, Save) -> Save
      (Stop o, _) -> Stop o
      (_, Stop o) -> Stop o
      (PerformAction a, PerformAction b) -> PerformAction (a . b)
      (PerformAction a, PerformActions b ba) -> PerformActions (a . b) ba
      (PerformAction a, PerformAudioAction aa) -> PerformActions a aa
      (PerformAction a, _) -> PerformAction a
      (PerformActions a aa, PerformAction b) -> PerformActions (a . b) aa
      (PerformActions a aa, PerformActions b ba) -> PerformActions (a . b) (aa <> ba)
      (PerformActions a aa, PerformAudioAction ba) -> PerformActions a (aa <> ba)
      (PerformActions a aa, _) -> PerformActions a aa
      (PerformAudioAction aa, PerformAction b) -> PerformActions b aa
      (PerformAudioAction aa, PerformActions b ba) -> PerformActions b (aa <> ba)
      (PerformAudioAction aa, PerformAudioAction ba) -> PerformAudioAction (aa <> ba)
      (PerformAudioAction aa, _) -> PerformAudioAction aa
      (a, Monitor) -> a
      (Monitor, b) -> b

instance Monoid PresenterToModel where
  mempty = Monitor

data AudioAction
  = StartPlayback
  | StopPlayback
  deriving (Generic, NFData, Show)

instance Semigroup AudioAction where
  StartPlayback <> _ = StartPlayback
  _ <> StartPlayback = StartPlayback
  _ <> _             = StopPlayback


data ModelToPresenter
  = Status
    { allSegmentsOf :: !Segments
    , canRedoOf :: !Bool
    , canUndoOf :: !Bool
    , errorOf :: !(Maybe SomeException)
    , filePathOf :: !FilePath
    , isPlayingOf :: !Bool
    , markingCursorPositionOf :: !SampleIndex
    , playbackCursorPositionOf :: !(Maybe SampleIndex)
    , sampleCountOf :: !SampleCount
    , sampleRateOf :: !Int
    , sampleTypeOf :: !SampleType
    , saveStatusOf :: !SaveStatus
    , segmentsInViewOf :: !Segments
    , viewCursorPositionOf :: !SampleIndex
    , viewSpanOf :: !SampleCount
    , waveformRepresentationOf :: !WaveformRepresentation
    }

data SaveStatus
  = SaveErrored
  | Saved
  | Saving !Float
  | Unsaved

currentlySaving :: SaveStatus -> Bool
currentlySaving = \case
  Saving _ -> True
  _ -> False

