{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE MonoLocalBinds #-}

module Screens.Editing.View.Waveform
 ( Waveform(..)
 , Interactivity(..)
 , module Screens.Editing.View.Waveform.Representation
 ) where

import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.List
import qualified Data.Vector.Unboxed as VU
import Data.Word
import SDL

import Audio
import Config
import Graphics.Context
import Graphics.Renderable
import Graphics.SDLShim
import Primitive.Geometry as UI
import Screens.Editing.View.Waveform.Representation
import qualified Screens.Editing.View.Waveform.ViewModel as WVM


data Waveform
  = Waveform
    { interactivityOf :: Interactivity
    , segmentsOf :: Segments
    , viewStartOf :: SampleIndex
    , waveformRepresentationOf :: WaveformRepresentation
    , waveformViewModelOf :: WVM.WaveformViewModel
    }

data Interactivity
  = NoInteractivity
  | Hovering !SampleOffset
  | Activating !SampleOffset
  | RemovingBetween !SampleOffset !SampleOffset
  | Truncating !SampleOffset

--------------------------------
--
--------------------------------

data ClipState
  = ClipActive
  | ClipHover
  | ClipNeutral
  | ClipRemoving
  deriving (Eq)

data GapState
  = GapNeutral
  | GapRemoving
  deriving (Eq)


clipStateFor interactivity segment sampleOffset =
  let pair = (segment, sampleOffset)
  in case interactivity of
    Hovering p ->
      if inSegment p pair then ClipHover else ClipNeutral
    Activating p ->
      if inSegment p pair then ClipActive else ClipNeutral
    RemovingBetween p1 p2 ->
      if inSegment p1 pair || inSpan (p1, p2) sampleOffset
      then ClipRemoving else ClipNeutral
    _ -> ClipNeutral

inSegment :: SampleOffset -> (Segment, SampleOffset) -> Bool
inSegment pos (segment, start) = start <= pos && pos < start + lengthOf segment
{-# INLINE inSegment #-}

inSpan :: (SampleOffset, SampleOffset) -> SampleOffset -> Bool
inSpan (p1, p2) start = p1 < start && start <= p2
{-# INLINE inSpan #-}


--------------------------------
--
--------------------------------

-- This code was optimized in the following ways:
-- - The loops were modified so that they run in the IO monad instead of the
--   full monad stack, as the machinery for monads and lifting to IO appear to
--   be expensive when done a lot
-- - Calls to the GPU are expensive, so the render color is set only when a
--   color change has occurred (instead of setting it each loop iteration)

instance Renderable Waveform where
  render waveform bounds = do
    displayDeadspace bounds
    displayGaps bounds waveform
    displayClipBackgrounds bounds waveform
    displayClipForegrounds bounds waveform

displayDeadspace
  :: (CanRender r m)
  => UI.Rectangle -> m ()
displayDeadspace bounds = do
  renderer <- getRenderer
  style <- asks getStyle
  let color = editScreenDeadSpaceColorOf style
  draw renderer (color, bounds)

displayGaps
  :: (CanRender r m)
  => UI.Rectangle -> Waveform -> m ()
displayGaps bounds waveform = do
  renderer <- getRenderer
  style <- asks getStyle
  highDPI <- readHighDPIFlag
  let gapBackgroundColorFor state =
        case state of
          GapNeutral  -> toV4 (editScreenGapNeutralColorOf style)
          GapRemoving -> toV4 (editScreenGapRemovingColorOf style)
  let Waveform
        { interactivityOf = interactivity
        , segmentsOf = segments
        , waveformViewModelOf = wvm
        } = waveform
  let startingIndex = WVM.getStartingIndex wvm
  -- since the gaps are going to be covered by the clips, we can draw just two
  -- rectangles, max, to cover all the gaps:
  --  - one for the regular gap background
  --  - one for the highlighted gap background, if any
  liftIO do
    -- neutral
    rendererDrawColor renderer $= gapBackgroundColorFor GapNeutral
    let segment = InsertedGap (numSamplesIn segments)
    let neutralBounds = bounds `shrinkHorizTo`
          WVM.segmentToPixelOffsets highDPI segment startingIndex wvm
    draw renderer neutralBounds
    -- highlighted
    case interactivity of
      RemovingBetween p1 p2 -> do
        let initCursor = cursorAtFirstSample segments
        let p2Cursor = advanceCursorInBounds p2 initCursor segments
        let endCursor = moveToEndOfSegment p2Cursor segments
        let segment = InsertedGap $ numSamplesBetween initCursor endCursor segments - p1
        let removalBounds = bounds `shrinkHorizTo`
              WVM.segmentToPixelOffsets highDPI segment (startingIndex+p1) wvm
        rendererDrawColor renderer $= gapBackgroundColorFor GapRemoving
        draw renderer removalBounds
      Truncating p -> do
        let initCursor = cursorAtFirstSample segments
        let midCursor = advanceCursorInBounds p initCursor segments
        let endCursor = moveToEndOfSegment midCursor segments
        let segment = InsertedGap $ numSamplesBetween initCursor endCursor segments - p
        let removalBounds = bounds `shrinkHorizTo`
              WVM.segmentToPixelOffsets highDPI segment (startingIndex+p) wvm
        rendererDrawColor renderer $= gapBackgroundColorFor GapRemoving
        draw renderer removalBounds
      _ -> return ()

displayClipBackgrounds
  :: (CanRender r m)
  => UI.Rectangle -> Waveform -> m ()
displayClipBackgrounds bounds waveform = do
  renderer <- getRenderer
  style <- asks getStyle
  highDPI <- readHighDPIFlag
  let clipBackgroundColorFor state =
        case state of
          ClipActive   -> toV4 (editScreenClipActiveBackgroundColorOf style)
          ClipHover    -> toV4 (editScreenClipHoverBackgroundColorOf style)
          ClipNeutral  -> toV4 (editScreenClipNeutralBackgroundColorOf style)
          ClipRemoving -> toV4 (editScreenClipRemovingBackgroundColorOf style)
  let Waveform
        { interactivityOf = interactivity
        , segmentsOf = segments
        , waveformViewModelOf = wvm
        } = waveform
  let startingIndex = WVM.getStartingIndex wvm
  let startingOffset = viewStartOf waveform - startingIndex
  let initClipState = ClipNeutral
  rendererDrawColor renderer $= clipBackgroundColorFor initClipState
  liftIO $ forEachWithAccum (startingOffset, initClipState) segments $
    \(sampleOffset, lastState) segment -> do
      nextState <- case segment of
        Clip{} -> do
          let state = clipStateFor interactivity segment sampleOffset
          when (state /= lastState) $
            rendererDrawColor renderer $= clipBackgroundColorFor state
          let start = startingIndex + sampleOffset
          let clipBounds = bounds `shrinkHorizTo`
                WVM.segmentToPixelOffsets highDPI segment start wvm
          draw renderer clipBounds
          return state
        _ -> return lastState
      return (sampleOffset + lengthOf segment, nextState)

displayClipForegrounds
  :: (CanRender r m)
  => UI.Rectangle -> Waveform -> m ()
displayClipForegrounds bounds waveform = do
  renderer <- getRenderer
  style <- asks getStyle
  highDPI <- readHighDPIFlag
  let clipForegroundColorFor state =
        case state of
          ClipActive   -> toV4 (editScreenClipActiveForegroundColorOf style)
          ClipHover    -> toV4 (editScreenClipHoverForegroundColorOf style)
          ClipNeutral  -> toV4 (editScreenClipNeutralForegroundColorOf style)
          ClipRemoving -> toV4 (editScreenClipRemovingForegroundColorOf style)
  let Waveform
        { interactivityOf = interactivity
        , segmentsOf = segments
        , waveformRepresentationOf = waveRep
        , waveformViewModelOf = wvm
        } = waveform
  let startingIndex = WVM.getStartingIndex wvm
  let startingOffset = viewStartOf waveform - startingIndex
  let (UI.Rectangle (UI.Coordinates x y) (UI.Dimensions _ height)) = bounds
  -- the following values were moved out of the rendering loop
  -- to help optimize it (since they're the same in each iteration)
  let halfHeight = fromIntegral height / 2.0
  let scale sample = round (halfHeight * (1 - sample)) + y
  --
  let initClipState = ClipNeutral
  rendererDrawColor renderer $= clipForegroundColorFor initClipState
  liftIO $ forEachWithAccum (startingOffset, initClipState) segments $
    \(sampleOffset, lastState) segment -> do
      nextState <- case segment of
        Clip{} -> do
          let state = clipStateFor interactivity segment sampleOffset
          when (state /= lastState) $
            rendererDrawColor renderer $= clipForegroundColorFor state
          --
          let start = startingIndex + sampleOffset
          case wvm of
            WVM.Samples{} -> do
              let samples = getRepresentationSamples segment waveRep
              let coordFor idx v = UI.Coordinates
                    (maybe x (+x) $ WVM.sampleToPixelOffset highDPI (start+idx) wvm)
                    (scale v)
              draw renderer $ UI.Polyline $
                VU.imap coordFor samples
            WVM.Extremes{ WVM.sampleRangesOf = ranges } -> do
              let repFor = getRepresentationExtremePair start segment waveRep
              let (x1, x2) = WVM.segmentToPixelOffsets highDPI segment start wvm
              forM_ [x1..x2] $ \index -> do
                let range = ranges VU.! (if highDPI then index `div` 2 else index)
                let (e1, e2) = repFor (WVM.maybeSplitRange range highDPI index)
                let x' = fromIntegral (x+index)
                let pointFrom = P . V2 x' . fromIntegral . scale
                drawLine renderer (pointFrom e1) (pointFrom e2)
          --
          return state
        _ -> return lastState
      return (sampleOffset + lengthOf segment, nextState)

shrinkHorizTo :: UI.Rectangle -> (Int, Int) -> UI.Rectangle
shrinkHorizTo (UI.Rectangle (UI.Coordinates x y) (UI.Dimensions _ height)) (x1, x2) =
  UI.Rectangle (UI.Coordinates (x+x1) y) (UI.Dimensions (x2-x1+1) height)
{-# INLINE shrinkHorizTo #-}

