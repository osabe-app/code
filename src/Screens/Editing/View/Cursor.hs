{-# LANGUAGE BlockArguments #-}

module Screens.Editing.View.Cursor
 ( Cursor(..)
 , CursorType(..)
 , PlaybackCursorState(..)
 , PrecisionCursorState(..)
 ) where

import Control.Monad.Reader
import Data.Maybe

import Audio hiding (Cursor)
import Config
import Graphics.Context
import Graphics.Renderable
import Graphics.SDLShim
import Primitive.Color
import Primitive.Geometry
import Screens.Editing.View.Waveform.ViewModel


data CursorType
  = MarkingCursor
  | PlaybackCursor PlaybackCursorState
  | PrecisionCursor PrecisionCursorState

data PlaybackCursorState
  = PlaybackCursorIdle
  | PlaybackCursorActive

data PrecisionCursorState
  = PrecisionCursorActivating
  | PrecisionCursorIdle
  | PrecisionCursorModifying

data Cursor
  = Cursor
    { cursorPositionOf :: SampleIndex
    , cursorTypeOf :: CursorType
    , viewModelOf :: WaveformViewModel
    }

instance Renderable Cursor where
  render cursor (Rectangle (Coordinates x y) (Dimensions _ height)) = do
    highDPI <- readHighDPIFlag
    let position = cursorPositionOf cursor
    let dx = fromMaybe 0 . sampleToPixelOffset highDPI position $ viewModelOf cursor
    color <- colorForCursor cursor
    renderer <- getRenderer
    if not highDPI
    then do
      let line = Line (Coordinates (x+dx) y) (Coordinates (x+dx) (y + height))
      draw renderer (color, line)
    else do
      let x1 = x + dx
      let x2 = x + dx + 1
      let line1 = Line (Coordinates x1 y) (Coordinates x1 (y + height))
      let line2 = Line (Coordinates x2 y) (Coordinates x2 (y + height))
      draw renderer (color, line1)
      draw renderer (color, line2)

colorForCursor :: (MonadReader r m, HasStyle r) => Cursor -> m Color
colorForCursor cursor = do
  style <- asks getStyle
  return case cursorTypeOf cursor of
    MarkingCursor ->
      editScreenMarkingCursorColorOf style
    PlaybackCursor PlaybackCursorActive ->
      editScreenPlaybackCursorActiveColorOf style
    PlaybackCursor PlaybackCursorIdle ->
      editScreenPlaybackCursorIdleColorOf style
    PrecisionCursor PrecisionCursorActivating ->
      editScreenPrecisionCursorActivatingColorOf style
    PrecisionCursor PrecisionCursorIdle ->
      editScreenPrecisionCursorIdleColorOf style
    PrecisionCursor PrecisionCursorModifying ->
      editScreenPrecisionCursorModifyingColorOf style

