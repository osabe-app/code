{-# LANGUAGE BlockArguments #-}

module Screens.Editing.View.ViewWindow
 ( ViewWindow(..)
 , ViewWindowState(..)
 ) where

import Control.Monad.Reader
import Data.Maybe

import Audio
import Config
import Graphics.Context
import Graphics.Renderable
import Graphics.SDLShim
import Primitive.Geometry
import Screens.Editing.View.Waveform.ViewModel


data ViewWindowState
  = ViewWindowActive
  | ViewWindowHover
  | ViewWindowNeutral

data ViewWindow
  = ViewWindow
    { viewWindowStateOf :: ViewWindowState
    , waveformViewModelOf :: WaveformViewModel
    , windowSpanOf :: SampleCount
    , windowStartOf :: SampleIndex
    }

instance Renderable ViewWindow where
  render viewWindow bounds = do
    highDPI <- readHighDPIFlag
    renderer <- getRenderer
    style <- asks getStyle
    let state = viewWindowStateOf viewWindow
    let viewPosition = windowStartOf viewWindow
    let viewSpan = windowSpanOf viewWindow
    let viewModel = waveformViewModelOf viewWindow
    --
    let Rectangle (Coordinates x y) (Dimensions w h) = bounds
    let vwx = fromMaybe 0 $ sampleToPixelOffset highDPI viewPosition viewModel
    let windowEndingSample = viewPosition + viewSpan - 1
    --                                  fromMaybe default for scrolling off the end
    let vww = max 1 . (\o -> o - vwx) . fromMaybe (w+1-x) $
          sampleToPixelOffset highDPI windowEndingSample viewModel
    let rect = Rectangle (Coordinates vwx y) (Dimensions vww h)
    --
    let backgroundColor =
          case state of
            ViewWindowActive  -> editScreenViewWindowActiveBackgroundColorOf style
            ViewWindowHover   -> editScreenViewWindowHoverBackgroundColorOf style
            ViewWindowNeutral -> editScreenViewWindowNeutralBackgroundColorOf style
    draw renderer (backgroundColor, rect)
    let borderColor =
          case state of
            ViewWindowActive  -> editScreenViewWindowActiveBorderColorOf style
            ViewWindowHover   -> editScreenViewWindowHoverBorderColorOf style
            ViewWindowNeutral -> editScreenViewWindowNeutralBorderColorOf style
    draw renderer (borderColor, rectToPoly rect)
    -- double up when we're at a high DPI to make it more visible
    when highDPI do
      let rect' = constrictBy 1 rect
      draw renderer (borderColor, rectToPoly rect')

