{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Editing.View.SampleRate where

import Control.DeepSeq
import Control.Monad.Reader
import FULE
import GHC.Generics (Generic)

import Graphics.Assets
import Graphics.Context
import Graphics.Renderable
import Primitive.Geometry


newtype SampleRate = SampleRate Int
  deriving (Generic, NFData)

instance Renderable SampleRate where
  render (SampleRate rate) (Rectangle coords _) = do
    renderer <- getRenderer
    displayTEXT renderer coords . label $ sampleRate rate

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => Component SampleRate m where
  requiredWidth _ = do
    width <- lowDPIWidthOfTEXT $ label (sampleRate 1000000)
    return (Just width)
  requiredHeight _ = do
    Dimensions _ height <- getLowDPITextureDims EditScreenStatusBarHertz
    return (Just height)

label =
  (EditScreenStatusBarSampleRateLabel :) .
  (EditScreenStatusBarNameValueSeparator :)

sampleRate rate =
  map EditScreenStatusBarChar (show rate)
  ++ [EditScreenStatusBarHertz]

