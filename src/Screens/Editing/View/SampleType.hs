{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Editing.View.SampleType where

import Control.DeepSeq
import Control.Monad.Reader
import FULE
import GHC.Generics (Generic)

import qualified Audio
import Graphics.Assets
import Graphics.Context
import Graphics.Renderable
import Primitive.Geometry


newtype SampleType = SampleType Audio.SampleType
  deriving (Generic, NFData)

instance Renderable SampleType where
  render (SampleType typ) (Rectangle coords _) = do
    renderer <- getRenderer
    displayTEXT renderer coords . label $ sampleType typ

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => Component SampleType m where
  requiredWidth _ = do
    width <- lowDPIWidthOfTEXT $ label (sampleType Audio.Word8)
    return (Just width)
  requiredHeight _ = do
    Dimensions _ height <- getLowDPITextureDims EditScreenStatusBarHertz
    return (Just height)

label =
  (EditScreenStatusBarSampleTypeLabel :) .
  (EditScreenStatusBarNameValueSeparator :)

sampleType typ = [EditScreenStatusBarSampleType typ]

