{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Editing.View.TimeAxis
 ( TimeAxis(..)
 ) where

import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.Maybe
import FULE
import Text.Printf (printf)
import SDL

import Audio hiding (sampleRateOf)
import Config
import Graphics.Assets
import Graphics.Context
import Graphics.Renderable
import Graphics.SDLShim
import Primitive.Geometry as G
import Screens.Editing.View.Waveform.ViewModel


data TimeAxis
  = TimeAxis
    { indexOfFirstDeadSampleOf :: SampleIndex
    , sampleRateOf :: Int
    , waveformViewModelOf :: WaveformViewModel
    }

tickHeight :: Int
tickHeight = 3

longer = (*2)

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => Component TimeAxis m where
  requiredHeight _ = do
    textHeight <- lowDPIHeightOfTEXT [AxisText '8']
    let height = longer tickHeight + textHeight + 2-- TODO read padding from style
    return (Just height)

instance Renderable TimeAxis where
  render timeAxis bounds = do
    highDPI <- readHighDPIFlag
    renderer <- getRenderer
    let TimeAxis { sampleRateOf = rate, waveformViewModelOf = wvm } = timeAxis
    let b = getStartingIndex wvm
    let e = indexOfFirstDeadSampleOf timeAxis
    let num = viewSpanOf wvm
    style <- asks getStyle
    let bgColor = editScreenAxisBackgroundColorOf style
    draw renderer (bgColor, bounds)
    --
    let borderColor = editScreenAxisBorderColorOf style
    let ex = calcSampleX highDPI bounds e wvm - 1
    draw renderer (borderColor, timeAxisLine bounds ex)
    --
    (textWidth, showMilli, labelInterval) <- calcLabelInterval bounds num rate
    let labelPositions = calcLabelPositions labelInterval b e
    let interstitialPositions = calcInterstitialPositions labelInterval b e
    --
    -- optimization: liftIO
    --
    let dpiMul = if highDPI then 2 else 1
    let y = yOf (coordinatesOf bounds)
    liftIO $ forM_ interstitialPositions $ \i -> do
      let x' = calcSampleX highDPI bounds i wvm
      let shortTickHeight = tickHeight * dpiMul
      let tick = tickLine x' y shortTickHeight
      draw renderer (borderColor, tick)
    textures <- getTextures
    liftIO $ forM_ labelPositions $ \i -> do
      let x' = calcSampleX highDPI bounds i wvm
      let longTickHeight = longer tickHeight * dpiMul
      let tick = tickLine x' y longTickHeight
      let textCoords = Coordinates (x' - textWidth `div` 2) (y+longTickHeight)
      let timeStamp = samplesToTimeStamp rate i
      draw renderer (borderColor, tick)
      displayTEXTures renderer textCoords textures (timeStampToTEXT showMilli timeStamp)

calcSampleX :: Bool -> G.Rectangle -> SampleIndex -> WaveformViewModel -> Int
calcSampleX highDPI (G.Rectangle (Coordinates x _) (Dimensions w _)) i wvm =
  (x'+) . fromJust $ sampleToPixelOffset highDPI i' wvm -- fromJust OK
  where
    startingIndex = getStartingIndex wvm
    viewSpan = viewSpanOf wvm
    (i', x') =
      if | i < startingIndex -> (i+viewSpan, x-w)
         | i >= startingIndex + viewSpan -> (i-viewSpan, x+w)
         | otherwise -> (i, x)

calcLabelInterval
  :: (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => G.Rectangle -> SampleCount -> Int -> m (Int, Bool, SampleCount)
calcLabelInterval (G.Rectangle _ (Dimensions w _)) num rate = do
  let spanTimeStamp = samplesToTimeStamp rate num
  textWidth <- widthOfTEXT (timeStampToTEXT False spanTimeStamp)
  let maxNumLabels = max 1 $ (w `div` textWidth) - 1
  let roughInterval = num `div` maxNumLabels
  let adjust = timeStampToSamples rate . adjustInterval . samplesToTimeStamp rate . (*2)
  if roughInterval > rate
  then return (textWidth, False, adjust roughInterval)
  else do
    textWidthMs <- widthOfTEXT (timeStampToTEXT True spanTimeStamp)
    let maxNumLabels = max 1 $ (w `div` textWidthMs) - 1
    let roughInterval = num `div` maxNumLabels
    let timeInterval = adjust roughInterval
    if timeInterval < rate
    then return (textWidthMs, True, timeInterval)
    else return (textWidth, False, timeInterval)

calcLabelPositions :: SampleCount -> SampleIndex -> SampleIndex -> [SampleIndex]
calcLabelPositions labelInterval beginningSample endingSample =
  -- start displaying before the view cursor position so we have a smooth
  -- scroll-in/out of the label text
  let firstLabelPosition = beginningSample - (beginningSample `mod` labelInterval)
  in  -- display after the end of the view so we have a smooth scroll-in/out
      -- of the label text, as well as a handy marker after the end of the
      -- audio when expanding the trailing gap
      takeWhile (< endingSample + labelInterval) -- adding the interval adds an extra label
      [ firstLabelPosition
      , firstLabelPosition + labelInterval
      ..]

{-# ANN module "HLint: ignore Use even" #-}
calcInterstitialPositions :: SampleCount -> SampleIndex -> SampleIndex -> [SampleIndex]
calcInterstitialPositions labelInterval beginningSample endingSample =
  let interstitialInterval =
        if | labelInterval `mod` 4 == 0 -> labelInterval `div` 4
           | labelInterval `mod` 2 == 0 -> labelInterval `div` 2
           | otherwise                  -> labelInterval
      firstInterstitialPosition =
        beginningSample + interstitialInterval - (beginningSample `mod` interstitialInterval)
  in  takeWhile (< endingSample)
      [ firstInterstitialPosition
      , firstInterstitialPosition + interstitialInterval
      ..]


data TimeStamp
  = TimeStamp
    { hoursOf :: !Int
    , minutesOf :: !Int
    , secondsOf :: !Int
    , millisecondsOf :: !Int
    }

defaultTimeStamp
  = TimeStamp
    { hoursOf = 0
    , minutesOf = 0
    , secondsOf = 0
    , millisecondsOf = 0
    }

samplesToTimeStamp :: Int -> Int -> TimeStamp
samplesToTimeStamp sampleRate samples =
  TimeStamp
  { hoursOf = hours
  , minutesOf = minutes
  , secondsOf = seconds
  , millisecondsOf = milliseconds
  }
  where
    milliseconds = samples * 1000 `div` sampleRate `mod` 1000
    totalSeconds = samples `div` sampleRate
    seconds = totalSeconds `mod` 60
    totalMinutes = totalSeconds `div` 60
    minutes = totalMinutes `mod` 60
    hours = totalMinutes `div` 60

timeStampToSamples :: Int -> TimeStamp -> Int
timeStampToSamples sampleRate (TimeStamp h m s l) =
  (h * 3600 + m * 60 + s) * sampleRate + (l * sampleRate `div` 1000)

adjustInterval :: TimeStamp -> TimeStamp
adjustInterval (TimeStamp 0 0 0 l) =
  defaultTimeStamp { millisecondsOf = nearestDivisionMs l }
adjustInterval (TimeStamp 0 0 s _) =
  defaultTimeStamp { secondsOf = nearestDivision s }
adjustInterval (TimeStamp 0 m _ _) =
  defaultTimeStamp { minutesOf = nearestDivision m }
adjustInterval (TimeStamp h _ _ _) =
  defaultTimeStamp { hoursOf = nearestDivision h }

nearestDivisionMs v
  | v < 5    = max 1 v
  | v < 10   = 10
  | v < 25   = 25
  | v < 50   = 50
  | v < 100  = 100
  | v < 250  = 250
  | v < 500  = 500
  | v < 1000 = 1000

nearestDivision v
  | v < 5  = v
  | v < 10 = 10
  | v < 15 = 15
  | v < 20 = 20
  | v < 30 = 30
  | v < 60 = 60

timeStampToTEXT showMilli (TimeStamp h m s l) =
  map AxisText $
  if showMilli
  then toTwoDigits h . (':':) . toTwoDigits m . (':':) . toTwoDigits s . ('.':) $ toThreeDigits l []
  else toTwoDigits h . (':':) . toTwoDigits m . (':':) $ toTwoDigits s []

toTwoDigits n = (numToDigit (n `div` 10 `mod` 10) :) . (numToDigit (mod n 10) :)

toThreeDigits n = (numToDigit (n `div` 100 `mod` 10) :) . toTwoDigits n

numToDigit = \case
  0 -> '0'
  1 -> '1'
  2 -> '2'
  3 -> '3'
  4 -> '4'
  5 -> '5'
  6 -> '6'
  7 -> '7'
  8 -> '8'
  9 -> '9'
  _ -> '?'

timeAxisLine :: G.Rectangle -> Int -> G.Line
timeAxisLine bounds@(G.Rectangle (G.Coordinates x y) _) ex =
  -- use the `x` for the ending sample instead of the bounds' width to not
  -- draw the time-axis line in the dead-space at the end
  Line (Coordinates x y) (Coordinates ex y)

tickLine :: Int -> Int -> Int -> G.Line
tickLine x y h = Line (Coordinates x y) (Coordinates x (y+h))

