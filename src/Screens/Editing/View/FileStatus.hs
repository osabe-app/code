{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Editing.View.FileStatus where

import Control.DeepSeq
import Control.Monad.Reader
import FULE
import GHC.Generics (Generic)

import Graphics.Assets
import Graphics.Context
import Graphics.Renderable
import Primitive.Geometry


data FileStatus
  = SaveErrored
  | Saved
  | Saving Float
  | Unsaved
  deriving (Generic, NFData)

instance Renderable FileStatus where
  render fileStatus (Rectangle coords _) = do
    renderer <- getRenderer
    displayTEXT renderer coords . label $
      case fileStatus of
        SaveErrored -> errored
        Saved       -> saved
        Saving p    -> saving p
        Unsaved     -> unsaved

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => Component FileStatus m where
  requiredWidth _ = do
    width <- maximum <$>
      mapM (lowDPIWidthOfTEXT . label) [errored, saved, saving 100, unsaved]
    return (Just width)
  requiredHeight _ = do
    Dimensions _ height <- getLowDPITextureDims EditScreenStatusBarSaved
    return (Just height)

label =
  (EditScreenStatusBarFileStatusLabel :) .
  (EditScreenStatusBarNameValueSeparator :)

errored = [EditScreenStatusBarSaveErrored]
saved = [EditScreenStatusBarSaved]
saving pct = 
  map EditScreenStatusBarChar (show (floor (pct*100))) ++
  [ EditScreenStatusBarChar '%'
  , EditScreenStatusBarChar ' '
  , EditScreenStatusBarSaved
  ]
unsaved = [EditScreenStatusBarUnsaved]

