{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Editing.View.PlaybackStatus where

import Control.DeepSeq
import Control.Monad.Reader
import FULE
import GHC.Generics (Generic)

import Graphics.Assets
import Graphics.Context
import Graphics.Renderable
import Primitive.Geometry


data PlaybackStatus
  = Playing
  | Stopped
  deriving (Generic, NFData)

instance Renderable PlaybackStatus where
  render fileStatus (Rectangle coords _) = do
    renderer <- getRenderer
    displayTEXT renderer coords . label $
      case fileStatus of
        Playing -> playing
        Stopped -> stopped

instance {-# OVERLAPPING #-}
  (MonadIO m, MonadReader r m, HasGraphicsContext r)
  => Component PlaybackStatus m where
  requiredWidth _ = do
    width <- maximum <$>
      mapM (lowDPIWidthOfTEXT . label) [playing, stopped]
    return (Just width)
  requiredHeight _ = do
    Dimensions _ height <- getLowDPITextureDims EditScreenStatusBarStopped
    return (Just height)

label =
  (EditScreenStatusBarPlaybackStatusLabel :) .
  (EditScreenStatusBarNameValueSeparator :)

playing = [EditScreenStatusBarPlaying]
stopped = [EditScreenStatusBarStopped]

