{-# LANGUAGE BlockArguments #-}

module Screens.Editing.View.Divider where

import Control.Monad.Reader

import Config
import Graphics.Assets
import Graphics.Context
import Graphics.Renderable
import Graphics.SDLShim
import Primitive.Geometry


data Divider = Divider

instance Renderable Divider where
  render _ bounds@(Rectangle _ (Dimensions w h)) =
    when (w > 0 && h > 0) do
      renderer <- getRenderer
      highDPI <- readHighDPIFlag
      color <- asks (editScreenResizeBarBackgroundColorOf . getStyle)
      draw renderer (color, bounds)
      texture <- getTexture EditScreenResizeBarPattern
      tilePattern renderer texture bounds

