{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}

module Screens.Editing.View.Waveform.ViewModel
 ( WaveformViewModel(..)
 , emptyWaveformViewModel
 , makeWaveformViewModel
 , getStartingIndex
 , SampleRanges
 , maybeSplitRange
 , sampleToPixelOffset
 , pixelOffsetToSample
 , pixelOffsetToSampleOffset
 , segmentToPixelOffsets
 , segmentUnderSampleOffset
 , samplesToShift
 --
 , samplesToScroll
 ) where

import Control.DeepSeq
import qualified Data.Vector.Unboxed as VU
import GHC.Generics (Generic)

import Audio hiding (numSamplesIn)
import Primitive.Geometry hiding (widthOf)


data WaveformViewModel
  = Extremes
    { sampleRangesOf :: !SampleRanges
    , viewSpanOf :: !SampleCount
    }
  | Samples
    { startingIndexOf :: !SampleIndex
    , viewSpanOf :: !SampleCount
    , widthOf :: !Int
    }
  deriving (Generic, NFData)

emptyWaveformViewModel :: WaveformViewModel
emptyWaveformViewModel = Samples 0 0 0

makeWaveformViewModel
  :: Int -> SampleIndex -> SampleCount -> Int -> WaveformViewModel
makeWaveformViewModel width startingIndex viewSpan chunkSize =
  -- multiplier of 2 means two extreme pairs per pixel to select from
  if viewSpan < 2*chunkSize*width
  then Samples startingIndex viewSpan width
  else Extremes sampleRanges viewSpan
  where
    sampleRanges = makeSampleRanges width startingIndex viewSpan

getStartingIndex :: WaveformViewModel -> SampleIndex
getStartingIndex = \case
  Extremes { sampleRangesOf = ranges } ->
    if VU.length ranges > 0 then fst $ ranges VU.! 0 else 0
  Samples { startingIndexOf = si } -> si

sampleToPixelOffset :: Bool -> SampleIndex -> WaveformViewModel -> Maybe Int
sampleToPixelOffset highDPI sampleIndex wvm =
  (if highDPI then fmap (*2) else id)
  case wvm of
    Extremes { sampleRangesOf = ranges, viewSpanOf = viewSpan } ->
      rangeContaining sampleIndex viewSpan ranges
    Samples
      { startingIndexOf = startingIndex
      , viewSpanOf = viewSpan
      , widthOf = width
      } ->
      toPixelOffset sampleIndex startingIndex viewSpan width

pixelOffsetToSample :: Int -> WaveformViewModel -> Maybe SampleIndex
pixelOffsetToSample offset = \case
  Extremes { sampleRangesOf = ranges } ->
    if offset < VU.length ranges
    then Just . fst $ ranges VU.! offset
    else Nothing
  Samples
    { startingIndexOf = startingIndex
    , viewSpanOf = viewSpan
    , widthOf = width
    } ->
    if offset < width
    then Just . (startingIndex +) . round $
      fromIntegral (viewSpan * offset) / fromIntegral width
    else Nothing

pixelOffsetToSampleOffset :: Int -> WaveformViewModel -> Maybe SampleOffset
pixelOffsetToSampleOffset pixelOffset wvm = do
  sampleIndex <- pixelOffsetToSample pixelOffset wvm
  return (sampleIndex - getStartingIndex wvm)

segmentToPixelOffsets
  :: Bool -> Segment -> SampleIndex -> WaveformViewModel -> (Int, Int)
segmentToPixelOffsets highDPI segment sampleIndex wvm =
  (if highDPI then doublePair else id)
  case wvm of
    Extremes { sampleRangesOf = ranges, viewSpanOf = viewSpan } ->
      segmentToRangeIndices segment sampleIndex viewSpan ranges
    Samples
      { startingIndexOf = startingIndex
      , viewSpanOf = viewSpan
      , widthOf = width
      } ->
      toPixelOffsets segment sampleIndex startingIndex viewSpan width

doublePair (a, b) = (a*2, b*2+1)

segmentUnderSampleOffset :: SampleCount -> Segments -> Maybe Segment
segmentUnderSampleOffset offset segments = do
  cursor <- advanceCursor offset (cursorAtFirstSample segments) segments
  return (segmentAtCursor cursor segments)

samplesToShift :: Int -> WaveformViewModel -> SampleOffset
samplesToShift delta = \case
  Extremes { sampleRangesOf = ranges } ->
    if | delta < 0 ->
          numSamplesIn $ VU.take (abs delta) ranges
       | delta > 0 ->
          (* (-1)) . numSamplesIn $ VU.drop (VU.length ranges - delta) ranges
       | otherwise -> 0
  Samples
    { viewSpanOf = viewSpan
    , widthOf = width
    } -> round $ fromIntegral (viewSpan * (-1) * delta) / fromIntegral width

numSamplesIn :: VU.Vector SampleRange -> SampleCount
numSamplesIn ranges =
  if VU.length ranges /= 0
  then snd (VU.last ranges) + 1 - fst (VU.head ranges)
  else 0


--------------------------------
-- Simple Samples
--------------------------------

toPixelOffset :: SampleIndex -> SampleIndex -> SampleCount -> Int -> Maybe Int
toPixelOffset sampleIndex startingIndex viewSpan width =
  if sampleIndex >= startingIndex && sampleIndex < startingIndex + viewSpan
  then Just pixelOffset
  else Nothing
  where
    sampleOffset = sampleIndex - startingIndex
    pixelOffset = sampleToPixel viewSpan width sampleOffset

toPixelOffsets
  :: Segment -> SampleIndex -> SampleIndex -> SampleCount -> Int -> (Int, Int)
toPixelOffsets segment sampleIndex startingIndex viewSpan width =
  (pixelOffset1, pixelOffset2)
  where
    sampleOffset1 = sampleIndex - startingIndex
    sampleOffset2 = sampleOffset1 + lengthOf segment - 1
    pixelOffset1 = sampleToPixel viewSpan width sampleOffset1
    pixelOffset2 = sampleToPixel viewSpan width sampleOffset2

sampleToPixel :: SampleCount -> Int -> SampleIndex -> Int
sampleToPixel viewSpan width sampleOffset = round
  ( fromIntegral sampleOffset
  / fromIntegral viewSpan
  * fromIntegral width
  )
{-# INLINE sampleToPixel #-}


--------------------------------
-- Sample Ranges
--------------------------------

type SampleRange = (SampleIndex, SampleIndex) -- indexes, inclusive
type SampleRanges = VU.Vector SampleRange

addToRange :: SampleCount -> SampleRange -> SampleRange
addToRange num (min, max) = (min+num, max+num)

respRange :: SampleIndex -> SampleRange -> Ordering
respRange sampleIndex (min, max)
 | sampleIndex < min = LT
 | sampleIndex > max = GT
 | otherwise = EQ

maybeSplitRange :: SampleRange -> Bool -> Int -> SampleRange
maybeSplitRange range@(s1, s2) highDPI index
 | not highDPI = range
 | even index = fst $ splitRange range
 | otherwise = snd $ splitRange range

splitRange :: SampleRange -> (SampleRange, SampleRange)
splitRange range@(s1, s2) =
  if | s1 == s2   -> (range, range)
     | s2-s1 == 1 -> ((s1, s1), (s2, s2))
     | otherwise  -> ((s1, s1+d), (s1+d+1, s2))
  where d = (s2-s1) `div` 2


--
-- generation
--

data GenState = GenState Int (Int, Int)

-- We have a certain number of pixels in which to display the samples in view.
-- The generation function below distributes the samples in view among these
-- pixels, associating with each pixel a range of samples. Since the samples
-- won't necessarily distrubte evenly among the pixels, some pixels will
-- represent more samples than others.

-- `samplesInView` must be larger than `width`
sampleRangesForPixels :: Int -> SampleCount -> SampleRanges
sampleRangesForPixels width samplesInView =
  VU.unfoldrN width generator initState
  where
    numSamplesPerPixel = samplesInView `div` width
    numSamplesToDistribute = samplesInView `rem` width
    initState = GenState numSamplesToDistribute (0, numSamplesPerPixel-1)
    generator (GenState numLeft plant@(_,s)) = Just
      ( plant
      , GenState (max 0 $ numLeft-1)
        ( s + 1
        , s + numSamplesPerPixel
            -- distribute the extra samples
            -- TODO: improve?
            + min 1 numLeft
        )
      )

-- When the view of the waveform advances, the waveform will appear to slide to
-- the left and off the screen. In order to keep the rendering of the waveform
-- smooth and consistent, the ranges associated with each pixel will shift to
-- the left as the waveform moves. Those ranges which drop off the left side of
-- the screen will be cycled over to the right, reused for the new part of the
-- waveform coming into view; this is what the rotation function below does.
rotateForViewOffset :: SampleIndex -> SampleCount -> SampleRanges -> SampleRanges
rotateForViewOffset startingIndex samplesInView ranges =
  VU.concat [rest, VU.map (addToRange samplesInView) init]
  where
    shouldRotate (_, max) = max < (startingIndex `mod` samplesInView)
    (init, rest) = VU.span shouldRotate ranges

-- The generation function only sets up one screen-width's worth of ranges;
-- the rotation function only slides into a second screen-width's worth of
-- ranges, but no further. When the view has moved into the waveform more than
-- a screen-width's worth of samples, we have to add an offset to the ranges to
-- get things to track correctly -- this is what the shift function below does.
shiftForViewOffset :: SampleIndex -> SampleCount -> SampleRanges -> SampleRanges
shiftForViewOffset startingIndex samplesInView =
  VU.map (addToRange offset)
  where
    offset = (startingIndex `div` samplesInView) * samplesInView

makeSampleRanges :: Int -> SampleIndex -> SampleCount -> SampleRanges
makeSampleRanges width startingIndex viewSpan =
     shiftForViewOffset startingIndex viewSpan
     . rotateForViewOffset startingIndex viewSpan
     $ sampleRangesForPixels width viewSpan


--
-- retrieval
--

rangeContaining :: SampleIndex -> SampleCount -> SampleRanges -> Maybe Int
rangeContaining value viewSpan ranges =
  rangeContaining' value ranges indexGuess
  where
    startingIndex = fst (ranges VU.! 0)
    indexGuess = ((value - startingIndex) * VU.length ranges) `div` viewSpan

rangeContaining' :: SampleIndex -> SampleRanges -> Int -> Maybe Int
rangeContaining' value ranges index =
  if | index < 0                 -> Nothing
     | index >= VU.length ranges -> Nothing
     | otherwise ->
        case respRange value (ranges VU.! index) of
          LT -> rangeContaining' value ranges (index-1)
          GT -> rangeContaining' value ranges (index+1)
          EQ -> Just index

segmentToRangeIndices :: Segment -> SampleIndex -> SampleCount -> SampleRanges -> (Int, Int)
segmentToRangeIndices segment startingIndex viewSpan ranges =
  case ( rangeContaining startingIndex viewSpan ranges
       , rangeContaining endingIndex viewSpan ranges) of
    (Just a,  Just b)   -> (a, b)
    (Just a,  Nothing)  -> (a, e)
    (Nothing, Just b)   -> (0, b)
    -- happens when a segment is about to enter the screen on the right, so
    -- default to the end of the waveform -- this results in a single ending
    -- teaser pixel:
    (Nothing, Nothing)  -> (e, e)
  where
    e = VU.length ranges - 1
    endingIndex = startingIndex + lengthOf segment - 1


--------------------------------
-- Scrolling
--------------------------------

-- positive `direction` scrolls forward
-- negative `direction` scrolls backward
samplesToScroll :: SampleCount -> Int -> SampleCount
samplesToScroll numSamplesInView direction =
  let viewPercent = 0.04
      dir =
        if | direction > 0 ->  1.0
           | direction < 0 -> -1.0
           | otherwise     ->  0.0
      num = fromIntegral numSamplesInView
  in floor (num * dir * viewPercent)

