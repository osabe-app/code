{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MultiWayIf #-}

module Screens.Editing.View.Waveform.Representation
 ( WaveformRepresentation
 , emptyWaveformRepresentation
 , makeWaveformRepresentation
 , chunkSize
 , getRepresentationExtremePair
 , getRepresentationSamples
 ) where

import Control.DeepSeq
import Control.Monad
import Data.List as L
import Data.Map.Strict as MS
import Data.Maybe
import qualified Data.Vector.Unboxed as VU
import GHC.Generics (Generic)

import Audio


--------------------------------
-- Clips Data
--------------------------------

type Samples = VU.Vector Float

data ClipData
  = ClipData
    { clipOf    :: !Segment
    , maxTreeOf :: !ExtremeTree
    , minTreeOf :: !ExtremeTree
    , samplesOf :: !Samples
    }
  deriving (Eq, Generic, NFData)


-- TODO make this an internal config item?
chunkSize = 32

makeClipData :: (AudioData a) => a -> Segment -> Float -> ClipData
makeClipData audio clip threshold = ClipData clip maxTree minTree samples
  where
    samples = makeDisplaySamples audio clip threshold
    (maxTree, minTree) = makeExtremes samples chunkSize

makeDisplaySamples :: AudioData a => a -> Segment -> Float -> VU.Vector Float
makeDisplaySamples audio clip threshold =
  VU.generate (lengthOf clip)
    (\i -> scaleForDisplay (dBFSAt (startOf clip + i) audio) threshold)

makeExtremes vec chunkSize =
  ( makeExtremeTree (>) $ findExtremes vec VU.maximum chunkSize
  , makeExtremeTree (<) $ findExtremes vec VU.minimum chunkSize
  )

findExtremes :: VU.Vector Float -> (VU.Vector Float -> Float) -> Int -> VU.Vector Float
findExtremes vec select chunkSize =
  VU.generate (size+extra) \i ->
    let len = if i < size then chunkSize else rem
    in select (VU.slice (i*chunkSize) len vec)
  where
    rem = VU.length vec `mod` chunkSize
    size = VU.length vec `div` chunkSize
    extra = if rem == 0 then 0 else 1


--------------------------------
-- Extremes Tree
--------------------------------

data ExtremeTree
  = Node
    { extremeOf :: !Float
    , indexOf :: !Int
    , leftOf :: !ExtremeTree
    , rightOf :: !ExtremeTree
    }
  | Null
  deriving (Eq, Generic, NFData)

type OrdFn = Float -> Float -> Bool
type Stack = [(Float, ExtremeTree -> ExtremeTree)]


-- This algorithm uses a stack of partial tree nodes for constructing an ExtremeTree:
--
-- As extremes are read in from the original list, if they are ordered from
-- greatest to least then they'll be put into nodes and pushed onto the stack
-- as read. The left-child pointer of each node will be set to Null and the
-- right-child pointer will be un-initialized.
-- When an extreme that is read in is greater than the previously read extreme,
-- the stack will be popped until the extreme at the top is again greater than
-- the one read from the list (or the stack is empty); the popped nodes will
-- be chained together as right children, with nodes higher up in the stack
-- becoming children of those lower -- the very top node will be given a right
-- child of Null. This chain will then become the left child in the node of the
-- extreme that prompted the popping, which itself will then be pushed onto the
-- stack, and the process continued.
-- Once all the values in the original list have been gone through, the nodes
-- remaining in the stack will be linked together, with those higher up in
-- the stack becoming right-children of those lower in the stack, and the tree
-- will be complete.
makeExtremeTree :: OrdFn -> VU.Vector Float -> ExtremeTree
makeExtremeTree ord = L.foldl' (flip snd) Null . VU.ifoldl (grow ord) []

grow :: OrdFn -> Stack -> Int -> Float -> Stack
grow ord stack index extreme =
  if L.null stack || ord topExtreme extreme
  then (extreme, Node extreme index Null):stack
  else (extreme, Node extreme index tree):stack'
  where
    topExtreme = fst (head stack)
    (stack', tree) = chainRight ord stack Null extreme

chainRight :: OrdFn -> Stack -> ExtremeTree -> Float -> (Stack, ExtremeTree)
chainRight _ [] tree _ = ([], tree)
chainRight ord stack tree extreme =
  if ord topExtreme extreme
  then (stack, tree)
  else chainRight ord (tail stack) (finishNode tree) extreme
  where
    (topExtreme, finishNode) = head stack

pickInRange :: Int -> Int -> ExtremeTree -> Float
pickInRange _ _ Null = 0.0
pickInRange min max (Node extreme index left right) =
  if | max < index -> pickInRange min max left
     | min > index -> pickInRange min max right
     | otherwise   -> extreme


--------------------------------
-- Representation
--------------------------------

type WaveformRepresentation = Map SampleIndex ClipData


emptyWaveformRepresentation :: WaveformRepresentation
emptyWaveformRepresentation = MS.empty

makeWaveformRepresentation
  :: (AudioData a, Monad m)
  => a -> Segments -> Float -> (Float -> m ()) -> m WaveformRepresentation
makeWaveformRepresentation audio segments threshold report = do
  report 0.0
  makeWaveformRepresentation' audio segments threshold report 1 MS.empty

makeWaveformRepresentation' audio segments threshold report index accum =
  if index < numSegments segments
  then do
    let segment = segmentAt index segments
    -- bang pattern(s) to force the computation(s) to run in the thread
    let !clipData = makeClipData audio segment threshold
    let !accum' = MS.insert (startOf segment) clipData accum
    report (fromIntegral (startOf segment + lengthOf segment) /
            fromIntegral (numberOfSamplesIn audio))
    makeWaveformRepresentation' audio segments threshold report (index+2) accum'
  else return accum

-- The extremes of the extreme trees are created from a vector containing
-- representatives from chunks of a segment; the indeices in these trees are thus
-- relative to the beginning of a segement, not where it's located in the overall
-- waveform.
-- The range we're given represents a portion of the overall waveform where the
-- segment is situated or overlaps. The range uses sample indices.
-- To get a representation of the segment for the range we're given, we have to
-- map the range of samples to the segment (going from absolute to relative),
-- transform it into chunk indices, then get the representative max/min for that
-- chunk index range.
getRepresentationExtremePair
  :: SampleIndex -> Segment -> WaveformRepresentation
  -> (SampleIndex, SampleIndex) -> (Float, Float)
getRepresentationExtremePair clipStartingIndex clip waveformRep =
  \(r1, r2) ->
    let d1 = max r1 clipStartingIndex - fullStartingIndex
        d2 = min r2 (clipStartingIndex + lengthOf clip - 1) - fullStartingIndex
        i1 = d1 `div` chunkSize
        i2 = d2 `div` chunkSize
    in (pickInRange i1 i2 minTree, pickInRange i1 i2 maxTree)
  where
    ClipData
      { clipOf = fullClip
      , maxTreeOf = maxTree
      , minTreeOf = minTree
      } = snd . fromJust $ MS.lookupLE (startOf clip) waveformRep
    fullStartingIndex = clipStartingIndex - (startOf clip - startOf fullClip)

getRepresentationSamples :: Segment -> WaveformRepresentation -> Samples
getRepresentationSamples clip waveformRep =
  VU.slice start (lengthOf clip) samples
  where
    ClipData
      { clipOf = fullClip
      , samplesOf = samples
      } = snd . fromJust $ MS.lookupLE (startOf clip) waveformRep
    start = startOf clip - startOf fullClip

