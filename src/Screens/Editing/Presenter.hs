{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Editing.Presenter
 ( presenter
 ) where

import Control.DeepSeq
import Control.Monad.Reader
import FULE
import GHC.Generics (Generic)
import Pipes
import Pipes.Core
import System.FilePath
import Text.Casing

import Config
import Graphics.Assets hiding (textOf)
import Graphics.Context
import Primitive.Geometry
import Screens.Editing.Messages
import Screens.Editing.Model.State
import Screens.Editing.Presenter.AutoscrollControl
import Screens.Editing.Presenter.Cursor
import Screens.Editing.Presenter.Divider
import Screens.Editing.Presenter.DynamicWaveform
import Screens.Editing.Presenter.FileStatus
import Screens.Editing.Presenter.FullWaveform
import Screens.Editing.Presenter.PlaybackControl
import Screens.Editing.Presenter.PlaybackStatus
import Screens.Editing.Presenter.PrecisionCursor
import Screens.Editing.Presenter.SampleRate
import Screens.Editing.Presenter.SampleType
import Screens.Editing.Presenter.ScrollControl
import Screens.Editing.Presenter.TimeAxis
import Screens.Editing.Presenter.ViewModel
import Screens.Editing.Presenter.ViewWindow
import Screens.Editing.Presenter.Waveform.ViewModel
import Screens.Editing.Presenter.ZoomControl
import Screens.Shared.DialogOption
import Screens.Shared.Messages
import Screens.Shared.Output as O
import Screens.Shared.Presenter.Button
import Screens.Shared.Presenter.Context
import Screens.Shared.Presenter.Control
import Screens.Shared.Presenter.InputContext
import Screens.Shared.Presenter.Reactor
import Screens.Shared.Presenter.Window
import Screens.Shared.UserInput
import Screens.Shared.View.Color


class
  ( HasGraphicsContext r
  , HasProgramName r
  , HasStyle r
  , MonadIO m
  , MonadReader r m
  ) => PresenterStateConstraint m r where {}
instance
  ( HasGraphicsContext r
  , HasProgramName r
  , HasStyle r
  , MonadIO m
  , MonadReader r m
  ) => PresenterStateConstraint m r where {}


type MI = ModelToPresenter
type MO = PresenterToModel
type VM = ViewModelRecord
type PO = PresenterAction
type R = Reactor' (RInput MI VM) (ROutput MO PO VM)
type I m = ItemM m (R m)

type PresenterState = Proxy PresenterToModel ModelToPresenter ViewToPresenter PresenterToView
type PresenterStateWithContext m = PresenterState (WithContext MI MO PO VM m)


--------------------------------
-- internal messages
--------------------------------

data PresenterAction
  = OutputAction !Output
  | SaveAction
  deriving (Generic, NFData)

actionToModelInput :: PresenterAction -> PresenterToModel
actionToModelInput = \case
  OutputAction o -> Stop o
  SaveAction -> Save

actionToString :: PresenterAction -> String
actionToString = \case
  SaveAction -> "save"
  OutputAction O.CloseRequested -> "quit"
  OutputAction (O.FileDropped _) -> "load"
  _ -> "perform action"


--------------------------------
-- toolbar
--------------------------------

type ToolbarButton = Button MI MO PO

makeUndoButton :: PresenterStateConstraint m r => m (R m)
makeUndoButton = return . reactor' $
  (button
    (buttonIcon EditScreenUndoIcon)
    Disabled (enableOn canUndoOf)
    (onClick (PerformAction undo))
    noReaction
    :: ToolbarButton)

makeRedoButton :: PresenterStateConstraint m r => m (R m)
makeRedoButton = return . reactor' $
  (button
    (buttonIcon EditScreenRedoIcon)
    Disabled (enableOn canRedoOf)
    (onClick (PerformAction redo))
    noReaction
    :: ToolbarButton)

makeSaveButton :: PresenterStateConstraint m r => m (R m)
makeSaveButton = return . reactor' $
  (button
    (buttonIcon EditScreenSaveIcon)
    Enabled (enableOn (not . currentlySaving . saveStatusOf))
    noReaction
    (onClick SaveAction)
    :: ToolbarButton)

makeToolbar
  :: forall m r . (PresenterStateConstraint m r)
  => m (LayeredM m (R m))
makeToolbar = do
  -- TODO add toolbar background color to styles
  toolbarBgColor :: R m <- asks (reactor' . editScreenStatusBarBackgroundColorOf . getStyle)
  undoButton <- makeUndoButton
  redoButton <- makeRedoButton
  saveButton <- makeSaveButton
  return
    (layered
      ([item toolbarBgColor
      , item
          (arrayedHoriz (padding 5 5) -- TODO get from style?
            ([item undoButton
            , item redoButton
            , item saveButton
            ]::[I m]))
      ]::[I m]))


--------------------------------
-- status bar
--------------------------------

getFileStatusText :: PresenterStateConstraint m r => m (R m)
getFileStatusText = return (reactor' fileStatus)

getPlaybackStatusText :: PresenterStateConstraint m r => m (R m)
getPlaybackStatusText = return (reactor' playbackStatus)

getSampleRateText :: PresenterStateConstraint m r => m (R m)
getSampleRateText = return (reactor' sampleRate)

getSampleTypeText :: PresenterStateConstraint m r => m (R m)
getSampleTypeText = return (reactor' sampleType)

makeStatusBar
  :: forall m r . (PresenterStateConstraint m r)
  => m (LayeredM m (R m))
makeStatusBar = do
  statusBarBgColor :: R m <- asks (reactor' . editScreenStatusBarBackgroundColorOf . getStyle)
  fileStatusText <- getFileStatusText
  playbackStatusText <- getPlaybackStatusText
  sampleRateText <- getSampleRateText
  sampleTypeText <- getSampleTypeText
  return
    (layered
      ([item statusBarBgColor
      , item
          (arrayedHoriz (padding 20 5) -- TODO get from style?
            ([item fileStatusText
            , item playbackStatusText
            , item sampleRateText
            , item sampleTypeText
            ]::[I m]))
      ]::[I m]))


--------------------------------
-- controls
--------------------------------

type AppControl = Control MO PO

attachControls
  :: PresenterStateConstraint m r
  => (Layout, [ComponentInfo (R m)]) -> m (Layout, [ComponentInfo (R m)])
attachControls (l, cs) = do
  autoscrollControl <- makeAutoscrollControl
  pagingControl <- makePagingControl
  playbackControl <- makePlaybackControl
  redoControl <- makeRedoControl
  saveControl <- makeSaveControl
  undoControl <- makeUndoControl
  let ci = head cs
  let controls = map (\c -> ci { componentOf = c })
        [ autoscrollControl
        , pagingControl
        , playbackControl
        , redoControl
        , saveControl
        , undoControl
        ]
  return (l, controls++cs)

makeAutoscrollControl :: PresenterStateConstraint m r => m (R m)
makeAutoscrollControl = return . reactor' $ autoscrollControl

makePagingControl :: PresenterStateConstraint m r => m (R m)
makePagingControl = return . reactor' $
  (control
    (\c ->
      if | not (anyCtrlHeld c) && keyDownEvent KeyHome c ->
            Just $ PerformAction seekBeginningView
         | not (anyCtrlHeld c) && keyDownEvent KeyEnd c ->
            Just $ PerformAction seekEndingView
         | not (anyCtrlHeld c) && keyDownEvent KeyPageUp c ->
            Just $ PerformAction pageBackwardView
         | not (anyCtrlHeld c) && keyDownEvent KeyPageDown c ->
            Just $ PerformAction pageForwardView
         | anyCtrlHeld c && keyDownEvent KeyHome c ->
            Just . PerformAction $ seekBeginningView . setMarkingCursorToBeginning
         | anyCtrlHeld c && keyDownEvent KeyEnd c ->
            Just . PerformAction $ seekEndingView . setMarkingCursorToEnding
         | otherwise -> Nothing
    )
    noControlReaction
    :: AppControl)

makePlaybackControl :: PresenterStateConstraint m r => m (R m)
makePlaybackControl = return . reactor' $ playbackControl

makeRedoControl :: PresenterStateConstraint m r => m (R m)
makeRedoControl = return . reactor' $
  (control
    (\c -> if isRedoCommand c then Just (PerformAction redo) else Nothing)
    noControlReaction
    :: AppControl)

makeSaveControl :: PresenterStateConstraint m r => m (R m)
makeSaveControl = return . reactor' $
  (control
    noControlReaction
    (\c -> if isSaveCommand c then Just SaveAction else Nothing)
    :: AppControl)

makeUndoControl :: PresenterStateConstraint m r => m (R m)
makeUndoControl = return . reactor' $
  (control
    (\c -> if isUndoCommand c then Just (PerformAction undo) else Nothing)
    noControlReaction
    :: AppControl)


--------------------------------
-- layout
--------------------------------

makeWaveformEditingView
  :: forall m r . (PresenterStateConstraint m r)
  => m (LayeredM m (R m))
makeWaveformEditingView = do
  let viewModel :: R m = reactor' (waveformViewModel Dynamic)
  let staticR :: Dynamics (R m) = static
  let zoomCtrl :: R m = reactor' zoomControl
  let ta :: R m = reactor' (timeAxis Dynamic)
  let wf :: R m = reactor' dynamicWaveform
  let markingCursor :: R m = reactor' (cursor MarkingCursor Dynamic)
  let playbackCursor :: R m = reactor' (cursor PlaybackCursor Dynamic)
  let precCursor :: R m = reactor' precisionCursor
  return (layered
    ([item zoomCtrl
    , item (sizedBottom sizedToContents staticR
              ta
              (layered
                ([item viewModel
                , item wf
                , item markingCursor
                , item playbackCursor
                , item precCursor
                ]::[I m])))
    ]::[I m]))


makeWaveformControlView
  :: forall m r . (PresenterStateConstraint m r)
  => m (Divided (R m) (R m) (LayeredM m (R m)))
makeWaveformControlView = do
  let viewModel :: R m = reactor' (waveformViewModel Full)
  let staticR :: Dynamics (R m) = static
  let ta :: R m = reactor' (timeAxis Full)
  let wf :: R m = reactor' fullWaveform
  let markingCursor :: R m = reactor' (cursor MarkingCursor Full)
  let playbackCursor :: R m = reactor' (cursor PlaybackCursor Full)
  let vw :: R m = reactor' viewWindow
  let sc :: R m = reactor' scrollControl
  return (sizedBottom sizedToContents staticR
    ta
    (layered
      ([item viewModel
      , item wf
      , item markingCursor
      , item playbackCursor
      , item vw
      , item sc
      ]::[I m])))

editScreenUI
  :: forall m r . (PresenterStateConstraint m r)
  => Int -> Int -> m (Layout, [ComponentInfo (R m)])
editScreenUI width height = do
  barSize :: Int <- asks (fromIntegral . editScreenResizeBarSizeOf . getStyle)
  viewControlSize :: Int <- asks (fromIntegral . editScreenNavigationBarHeightOf . getStyle)
  let staticR :: Dynamics (R m) = static
  let dynamicR :: Dynamics (R m) = dynamic (((.).(.)) reactor' divider) barSize
  toolbar <- makeToolbar
  statusBar <- makeStatusBar
  editingView <- makeWaveformEditingView
  controlView <- makeWaveformControlView
  attachControls =<< layoutM
    (window (width, height)
      (windowControl ((OutputAction <$>) . windowEventHandler))
      (sizedTop sizedToContents staticR
        toolbar
        (sizedBottom sizedToContents staticR
          statusBar
          (sizedBottom (sizedTo viewControlSize) dynamicR
            (clipped controlView)
            (clipped editingView)
            ))))


--------------------------------
-- presenter
--------------------------------

presenter
  :: PresenterStateConstraint m r
  => Dimensions -> ViewToPresenter -> PresenterState m Output
presenter (Dimensions width height) input = do
  ui <- lift $ editScreenUI width height
  runWithContext ui $ editing input

-- STUFF TODO
--
-- - stop playback when prompting?
-- - handle errors in model:
--    case errorOf status of
--      Just ex -> respond (Alert "Error!" (show ex))
--      Nothing -> respond =<< genOutput status

editing
  :: PresenterStateConstraint m r
  => ViewToPresenter -> PresenterStateWithContext m Output
editing input = do
  lift $ integrateViewInput input
  --
  forModel <- fmap mconcat . lift $ getOutputsForModel
  state <- request forModel
  lift $ integrateModelInput state
  --
  forPresenter <- lift $ getOutputsForPresenter
  unless (null forPresenter)
    (processPresenterInstruction (head forPresenter) state)
  --
  programName <- getNameOfProgram
  let path = filePathOf state
  let (_, fileName) = splitFileName path
  let decorator = toDecorator (saveStatusOf state)
  let title = concat [decorator, " ", fileName, " - ", programName, " - ", path]
  view <- lift renderViewOutput
  input' <- respond (Display view title)
  editing input'

processPresenterInstruction
  :: PresenterStateConstraint m r
  => PresenterAction -> ModelToPresenter -> PresenterStateWithContext m ()
processPresenterInstruction action modelState = do
  let actionText = actionToString action
  let actionInput = actionToModelInput action
  let react = return . selectPromptOutput actionInput Monitor
  forModel <- case action of
    OutputAction{} ->
      case saveStatusOf modelState of
        Saved       -> return actionInput
        SaveErrored -> react =<< respond (saveErroredDialog actionText)
        Saving{}    -> react =<< respond (stillSavingDialog actionText)
        Unsaved     -> react =<< respond (workUnsavedDialog actionText)
    SaveAction ->
      case saveStatusOf modelState of
        Saving{} -> do
          _ <- respond (Alert "Already Saving" "Save already in progress.")
          return Monitor
        _ ->
          return actionInput
  state <- request forModel
  lift $ integrateModelInput state

selectPromptOutput :: a -> a -> ViewToPresenter -> a
selectPromptOutput affirmative negative = \case
  PromptResponse (Just r) ->
    case toEnum r of
      Ok -> affirmative
      Cancel -> negative
  _ -> negative

saveErroredDialog :: String -> PresenterToView
saveErroredDialog action = Prompt title message options
  where
    title = "Save errored!"
    message = concat ["Your last save errored, ", action, " anyway?"]
    options = [okOption { textOf = pascal action }, cancelOption]

stillSavingDialog :: String -> PresenterToView
stillSavingDialog action = Prompt title message options
  where
    title = "Still saving!!!"
    message = concat ["Your file is STILL BEING SAVED, ", action, " anyway?"]
    options = [okOption { textOf = pascal action }, cancelOption]

workUnsavedDialog :: String -> PresenterToView
workUnsavedDialog action = Prompt title message options
  where
    title = "Work unsaved!"
    message = concat ["You have unsaved work, ", action, " anyway?"]
    options = [okOption { textOf = pascal action }, cancelOption]

toDecorator :: SaveStatus -> String
toDecorator = \case
  SaveErrored         -> "⚠" -- U+26A0
  Saved               -> "☰"
  Saving p | p > 0.83 -> "☱"
  Saving p | p > 0.67 -> "☲"
  Saving p | p > 0.50 -> "☳"
  Saving p | p > 0.33 -> "☴"
  Saving p | p > 0.17 -> "☵"
  Saving _            -> "☶"
  Unsaved             -> "☷"

