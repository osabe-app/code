{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Screens.Editing.Model
 ( model
 ) where

import Control.Monad
import Control.Monad.Reader
import Data.Functor
import Data.Maybe
import Pipes
import Pipes.Core

import qualified Audio
import Config
import Control.Concurrent.Container
import Control.Concurrent.Operation as Op
import Screens.Editing.Messages
import Screens.Editing.Model.Context
import Screens.Editing.Model.State
import Screens.Editing.View.Waveform.Representation
import Screens.Shared.Output


type ModelState = Server PresenterToModel ModelToPresenter
type ModelStateWithContext s d o m = ModelState (WithContext s d o m)

class
  ( Audio.AudioLoader m
  , Audio.AudioPlayer m a State d
  , Container m a
  , HasSettings r
  , MonadReader r m
  , Operation m o
  ) => ModelStateConstraint m r a o d where {}
instance
  ( Audio.AudioLoader m
  , Audio.AudioPlayer m a State d
  , Container m a
  , HasSettings r
  , MonadReader r m
  , Operation m o
  ) => ModelStateConstraint m r a o d where {}


model
  :: ModelStateConstraint m r a o d
  => Audio.AudioFile -> Audio.Segments -> WaveformRepresentation -> PresenterToModel
  -> ModelState m Output
model audio segments waveRep input = do
  numPlaybackSamples <- lift getPlaybackBufferSampleCount
  viewSpan <- lift getDefaultViewSpanInS <&> fmap (* Audio.sampleRateOf audio)
  let sampleCount = Audio.numSamplesIn segments
  let numViewSamples = maybe sampleCount (min sampleCount) viewSpan
  let model = newState numPlaybackSamples numViewSamples audio segments
  modelStateVar <- lift (create model)
  audioDevice <- lift (Audio.openPlaybackDevice modelStateVar)
  runWithContext audioDevice modelStateVar waveRep $ running input


--------------------------------

-- NOTE: all checks for save status will be done outside of the model;
-- the model assumes it is always safe to run an op when told to do so
running
  :: ModelStateConstraint m r a o d
  => PresenterToModel -> ModelStateWithContext a d o m Output
running = \case
  Monitor -> continueRunning
  PerformAction modelAction -> do
    lift (updateMainState modelAction)
    continueRunning
  PerformActions modelAction audioAction -> do
    lift (updateMainState modelAction)
    lift (updatePlaybackState audioAction)
    continueRunning
  PerformAudioAction audioAction -> do
    lift (updatePlaybackState audioAction)
    continueRunning
  Save -> do
    lift startSaveOp
    continueRunning
  Stop output -> do
    lift closePlaybackDevice
    return output

continueRunning
  :: ModelStateConstraint m r a o d
  => ModelStateWithContext a d o m Output
continueRunning = do
  isPlaying <- lift getPlaybackState
  error <- lift updateSaveState
  saveStatus <- lift getSaveState
  response <- lift (makeResponse isPlaying saveStatus error)
  input <- respond response
  running input

