module Screens.Editing.Model.AppState where


data Patch a
  = Patch
    { backwardOf :: !(a -> a)
    , forwardOf :: !(a -> a)
    }

data AppState a c s w
  = AppState
    { backwardsOf :: ![Patch a]
    , clipboardOf :: !(Maybe c)
    , forwardsOf :: ![Patch a]
    , saveStateOf :: !s
    , stateOf :: !a
    , workspaceOf :: !(Maybe w)
    }


newAppState s a =
  AppState
  { backwardsOf = []
  , clipboardOf = Nothing
  , forwardsOf = []
  , saveStateOf = s
  , stateOf = a
  , workspaceOf = Nothing
  }

over :: (a -> a) -> AppState a c s w -> AppState a c s w
over f s = s { stateOf = f (stateOf s) }

from :: (a -> b) -> AppState a c s w -> b
from f s = f (stateOf s)

apply :: Patch a -> AppState a c s w -> AppState a c s w
apply patch state =
  state
  { backwardsOf = patch : backwardsOf state
  , forwardsOf = []
  , stateOf = forwardOf patch (stateOf state)
  }

canUndo :: AppState a c s w -> Bool
canUndo = not . null . backwardsOf

undo :: AppState a c s w -> AppState a c s w
undo state
 | null (backwardsOf state) = state
 | otherwise =
    state
    { backwardsOf = bs
    , forwardsOf = b : forwardsOf state
    , stateOf = backwardOf b (stateOf state)
    }
    where b:bs = backwardsOf state

canRedo :: AppState a c s w -> Bool
canRedo = not . null . forwardsOf

redo :: AppState a c s w -> AppState a c s w
redo state
 | null (forwardsOf state) = state
 | otherwise =
    state
    { backwardsOf = f : backwardsOf state
    , forwardsOf = fs
    , stateOf = forwardOf f (stateOf state)
    }
    where f:fs = forwardsOf state

