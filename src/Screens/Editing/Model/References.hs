module Screens.Editing.Model.References where

import Control.Lens

import Audio


identity :: Lens' (Identity a) a
identity = lens runIdentity (\_ v -> Identity v)


data View f
  = View
    { cursorOf :: !(f Cursor)
    , spanOf :: !SampleCount
    }


cursor :: Lens' (View f) (f Cursor)
cursor = lens cursorOf (\v c -> v { cursorOf = c })

span :: Lens' (View f) Int
span = lens spanOf (\v s -> v { spanOf = s })


data References
  = References
    { markingCursorOf :: !Cursor
    , playbackViewOf :: !(View Maybe)
    , viewViewOf :: !(View Identity)
    }


markingCursorReference :: Lens' References Cursor
markingCursorReference = lens markingCursorOf (\m v -> m { markingCursorOf = v})

playbackViewReference :: Lens' References (View Maybe)
playbackViewReference = lens playbackViewOf (\m v -> m { playbackViewOf = v })

viewViewReference :: Lens' References (View Identity)
viewViewReference = lens viewViewOf (\m v -> m { viewViewOf = v })

