{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}

module Screens.Editing.Model.Context
 ( Context
 , WithContext
 , runWithContext
 --
 , updateMainState
 , makeResponse
 --
 , getPlaybackState
 , updatePlaybackState
 , closePlaybackDevice
 --
 , startSaveOp
 , getSaveState
 , updateSaveState
 ) where

import Prelude hiding (read)
import Control.Exception
import Control.Monad.State.Strict hiding (State)
import Pipes.Core
import Pipes.Lift

import qualified Audio
import Control.Concurrent.Container
import Control.Concurrent.Operation
import qualified Screens.Editing.Messages as M
import Screens.Editing.Model.State
import Screens.Editing.View.Waveform.Representation


data Context s d o
  = Context
    { audioDeviceOf :: d
    , mainStateOf :: s State
    , saveOpOf :: Maybe (o Audio.Segments)
    , waveformRepresentationOf :: WaveformRepresentation
    }

type WithContext s d o m = StateT (Context s d o) m


runWithContext
  :: (Monad m)
  => d -> s State -> WaveformRepresentation
  -> Proxy a' a b' b (WithContext s d o m) out
  -> Proxy a' a b' b m out
runWithContext device stateVar waveRep = evalStateP $
  Context
  { audioDeviceOf = device
  , mainStateOf = stateVar
  , saveOpOf = Nothing
  , waveformRepresentationOf = waveRep
  }


--------------------------------
-- Main State
--------------------------------

updateMainState
  :: (Container m s, Monad m)
  => (State -> State) -> WithContext s d o m ()
updateMainState action = do
  mainState <- gets mainStateOf
  lift (update mainState action)

makeResponse
  :: (Container m s, Monad m)
  => Bool -> M.SaveStatus -> Maybe SomeException
  -> WithContext s d o m M.ModelToPresenter
makeResponse isPlaying saveStatus error = do
  mainState <- gets mainStateOf >>= lift . read
  waveRep <- gets waveformRepresentationOf
  return M.Status
    { M.allSegmentsOf = getAllSegments mainState
    , M.canRedoOf = canRedo mainState
    , M.canUndoOf = canUndo mainState
    , M.errorOf = error
    , M.filePathOf = Audio.pathOf (getAudioFile mainState)
    , M.isPlayingOf = isPlaying
    , M.markingCursorPositionOf = getMarkingCursorPosition mainState
    , M.playbackCursorPositionOf = getPlaybackCursorPosition mainState
    , M.sampleCountOf = Audio.numSamplesIn (getAllSegments mainState)
    , M.sampleRateOf = Audio.sampleRateOf mainState
    , M.sampleTypeOf = Audio.sampleTypeOf mainState
    , M.saveStatusOf = saveStatus
    , M.segmentsInViewOf = getVisibleSegments mainState
    , M.viewCursorPositionOf = getViewCursorPosition mainState
    , M.viewSpanOf = getViewSpan mainState
    , M.waveformRepresentationOf = waveRep
    }


--------------------------------
-- Playback
--------------------------------

getPlaybackState
  :: (Audio.AudioPlayer m s State d, Monad m)
  => WithContext s d o m Bool
getPlaybackState = gets audioDeviceOf >>= lift . Audio.deviceIsPlaying

updatePlaybackState
  :: (Audio.AudioPlayer m s State d, Monad m)
  => M.AudioAction -> WithContext s d o m ()
updatePlaybackState action = do
  gets audioDeviceOf >>=
    case action of
      M.StartPlayback -> lift . Audio.startPlaybackDevice
      M.StopPlayback -> lift . Audio.stopPlaybackDevice

closePlaybackDevice
  :: (Audio.AudioPlayer m s State d, Monad m)
  => WithContext s d o m ()
closePlaybackDevice = gets audioDeviceOf >>= lift . Audio.closePlaybackDevice


--------------------------------
-- Save Operation
--------------------------------

startSaveOp
  :: (Audio.AudioLoader m, Container m s, Monad m, Operation m o)
  => WithContext s d o m ()
startSaveOp = do
  mainStateVar <- gets mainStateOf
  mainState <- lift (read mainStateVar)
  let audioFile = getAudioFile mainState
  let segments = getAllSegments mainState
  saveOp <- lift (start (Audio.saveAudio audioFile segments))
  modify' (\s -> s { saveOpOf = Just saveOp })

getSaveState
  :: (Container m s, Monad m, Operation m o)
  => WithContext s d o m M.SaveStatus
getSaveState = do
  saveOpStatus <- gets saveOpOf >>= \case
    Nothing -> return Nothing
    Just op -> Just <$> lift (poll op)
  state <- gets mainStateOf >>= lift . read
  case saveOpStatus of
    Just (Completed s)   -> return if getAllSegments state == s then M.Saved else M.Unsaved
    Just (Errored _)     -> return M.SaveErrored
    Just (Progressing p) -> return (M.Saving p)
    Nothing ->
      case getSaveStatus state of
        Just True  -> return M.Saved
        Just False -> return M.Unsaved
        Nothing    -> return M.SaveErrored

-- we have to check for errors whenever we update the state of the save op
-- because if we do so separately we might miss returning an error to the user
-- due to the thread finishing between checking for errors and clearing the op
updateSaveState
  :: (Container m s, Monad m, Operation m o)
  => WithContext s d o m (Maybe SomeException)
updateSaveState = do
  mainStateVar <- gets mainStateOf
  saveOpStatus <- gets saveOpOf >>= \case
    Nothing -> return Nothing
    Just op -> Just <$> lift (poll op)
  case saveOpStatus of
    Just (Completed s) -> do
      lift $ update mainStateVar (saved (Just s))
      modify' (\s -> s { saveOpOf = Nothing })
      return Nothing
    Just (Errored ex) -> do
      case fromException ex :: Maybe Audio.AudioFileException of
        Just Audio.ErrorRestoringBackup{} ->
          lift $ update mainStateVar (saved Nothing)
        Just Audio.ErrorWritingWithoutBackup{} ->
          lift $ update mainStateVar (saved Nothing)
        _ -> return ()
      modify' (\s -> s { saveOpOf = Nothing })
      return (Just ex)
    _ -> return Nothing

