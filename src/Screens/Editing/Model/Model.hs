{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE MultiWayIf #-}

module Screens.Editing.Model.Model
 ( Model(..)
 , newModel
 , MarkingCursorManipulation(..)
 , PlaybackManipulation(..)
 , ViewManipulation(..)
 , SegmentManipulation(..)
 ) where

import Prelude hiding (span)
import Control.Lens.Getter
import Control.Lens.Lens
import Control.Lens.Setter
import Control.Monad.Reader
import Data.Functor.Identity
import Data.List hiding (span)
import Data.Maybe

import Audio
import Screens.Editing.Model.References


data Model
  = Model
    { audioFileOf :: !AudioFile
    , referencesOf :: !References
    , segmentsOf :: !Segments
    }

instance AudioData Model where
  numberOfSamplesIn = numSamplesIn . segmentsOf
  sampleRateOf = sampleRateOf . audioFileOf
  sampleTypeOf = sampleTypeOf . audioFileOf
  fillWith = fillWith . audioFileOf
  dBFSAt i = dBFSAt i . audioFileOf
  belowThreshold = belowThreshold . audioFileOf

instance PlaybackSource Model where
  playbackSuspended = isNothing . (^.playbackView.cursor)
  getSegmentsToPlay _ = getAudibleSegments
  recordSamplesPlayed _ = pageForwardPlayback


references :: Lens' Model References
references = lens referencesOf (\m v -> m { referencesOf = v })

markingCursor :: Lens' Model Cursor
markingCursor = references.markingCursorReference

playbackView :: Lens' Model (View Maybe)
playbackView = references.playbackViewReference

viewCursor :: Lens' Model Cursor
viewCursor = references.viewViewReference.cursor.identity

viewSpan :: Lens' Model Int
viewSpan = references.viewViewReference.span


newModel :: SampleCount -> SampleCount -> AudioFile -> Segments -> Model
newModel numPlaybackSamples numViewSamples audioFile segments =
  Model
  { audioFileOf = audioFile
  , referencesOf =
      References
      { markingCursorOf = defaultCursor
      , playbackViewOf = View Nothing numPlaybackSamples
      , viewViewOf = View (Identity defaultCursor) numViewSamples
      }
  , segmentsOf = segments
  }
  where
    defaultCursor = cursorAtFirstSample segments


--------------------------------
-- cursors and views
--------------------------------

class MarkingCursorManipulation a where
  getMarkingCursor :: a -> Cursor
  getMarkingCursorPosition :: a -> SampleIndex
  setMarkingCursorInViewTo :: SampleOffset -> a -> a
  setMarkingCursorToClipInViewAt :: SampleOffset -> a -> a
  setMarkingCursorToBeginning :: a -> a
  setMarkingCursorToEnding :: a -> a

instance MarkingCursorManipulation Model where
  getMarkingCursor = (^.markingCursor)
  getMarkingCursorPosition model =
    numSamplesBetween beginningCursor cursor segments
    where
      beginningCursor = cursorAtFirstSample segments
      cursor = model^.markingCursor
      segments = segmentsOf model
  setMarkingCursorInViewTo samplesFromViewCursor model =
    -- do it this way instead of using advanceCursorInBounds so we don't go to the
    -- end when the user clicks in the trailing dead space
    -- ... but maybe that would be desired?
    case advanceCursor samplesFromViewCursor cursor segments of
      Just c -> set markingCursor c model
      Nothing -> model
    where
      segments = segmentsOf model
      cursor = model^.viewCursor
  setMarkingCursorToClipInViewAt samplesFromViewCursor model =
    set markingCursor cursor model'
    where
      model' = setMarkingCursorInViewTo samplesFromViewCursor model
      cursor = moveToBeginningOfSegment (model'^.markingCursor) (segmentsOf model')
  setMarkingCursorToBeginning model =
    set markingCursor (cursorAtFirstSample (segmentsOf model)) model
  setMarkingCursorToEnding model =
    set markingCursor (cursorAtLastSample (segmentsOf model)) model


class PlaybackManipulation a where
  getPlaybackCursorPosition :: a -> Maybe SampleIndex
  maybeInitPlaybackCursor :: a -> a
  pageForwardPlayback :: a -> a
  setPlaybackCursorToMarkingCursor :: a -> a

instance PlaybackManipulation Model where
  getPlaybackCursorPosition model =
    fmap (\c -> numSamplesBetween beginningCursor c segments) playbackCursor
    where
      beginningCursor = cursorAtFirstSample segments
      playbackCursor = model^.playbackView.cursor
      segments = segmentsOf model
  maybeInitPlaybackCursor model =
    case view (playbackView.cursor) model of
      Just _  -> model
      Nothing -> setPlaybackCursorToMarkingCursor model
  pageForwardPlayback model =
    over (playbackView.cursor) advance model
    where
      advance = (>>= (\c -> advanceCursor numSamples c (segmentsOf model)))
      numSamples = model^.playbackView.span
  setPlaybackCursorToMarkingCursor model =
    set (playbackView.cursor) (Just (model^.markingCursor)) model


class ViewManipulation a where
  cursorForSamplesFromViewStart :: SampleOffset -> a -> Maybe Cursor
  getViewCursorPosition :: a -> SampleIndex
  getViewSpan :: a -> SampleCount
  moveViewCursorBy :: SampleOffset -> a -> a
  moveViewCursorTo :: SampleIndex -> a -> a
  pageBackwardView :: a -> a
  pageForwardView :: a -> a
  seekBeginningView :: a -> a
  seekEndingView :: a -> a
  zoomView :: Float -> Float -> a -> a

instance ViewManipulation Model where
  cursorForSamplesFromViewStart samplesFromViewCursor model =
    advanceCursor samplesFromViewCursor (model^.viewCursor) (segmentsOf model)
  getViewCursorPosition model =
    numSamplesBetween beginningCursor cursor segments
    where
      beginningCursor = cursorAtFirstSample segments
      cursor = model^.viewCursor
      segments = segmentsOf model
  getViewSpan model = model^.viewSpan
  moveViewCursorBy numSamples model
    | numSamples < 0 = over viewCursor retreat model
    | numSamples > 0 = over viewCursor advance model
    | otherwise      = model
    where
      segments = segmentsOf model
      advance cursor = advanceCursorInBounds numSamples cursor segments
      retreat cursor = retreatCursorInBounds (abs numSamples) cursor segments
  moveViewCursorTo numSamples model =
    moveViewCursorBy (numSamples - getViewCursorPosition model) model
  pageBackwardView model =
    over viewCursor retreat model
    where
      numSamples = model^.viewSpan
      retreat cursor = retreatCursorInBounds numSamples cursor (segmentsOf model)
  pageForwardView model =
    if forwarded^.viewCursor < ended^.viewCursor
    then forwarded
    else ended
    where
      numSamples = model^.viewSpan
      advance cursor = advanceCursorInBounds numSamples cursor (segmentsOf model)
      forwarded = over viewCursor advance model
      ended = seekEndingView model
  seekBeginningView model =
    set viewCursor (cursorAtFirstSample (segmentsOf model)) model
  seekEndingView model =
    set viewCursor cursor model
    where
      end = cursorAtLastSample (segmentsOf model)
      numSamples = model^.viewSpan
      cursor = retreatCursorInBounds (numSamples-1) end (segmentsOf model)
  zoomView viewPercent scale model =
    moveViewCursorBy panSamples . set viewSpan spanSamples $ model
    where
      (panSamples, spanSamples) = calcAdjustment scale
      minSamples = 100
      maxSamples = numSamplesIn (getAllSegments model)
      --
      currentViewPosition = getViewCursorPosition model
      currentViewSpan = model^.viewSpan
      span = fromIntegral currentViewSpan
      cursorPosition = currentViewPosition + round (viewPercent * span)
      --
      calcAdjustment scale =
        let leftPercent =
              if | scale < 1 && cursorPosition > maxSamples -> 0
                 | scale > 1 && cursorPosition > maxSamples -> 1
                 | otherwise                                -> viewPercent
            rightPercent = 1 - leftPercent
            leftPercent' = leftPercent * scale
            rightPercent' = rightPercent * scale
            panSamples = round (span * (leftPercent - leftPercent'))
            spanSamples = round (span * (leftPercent' + rightPercent'))
        in if | spanSamples > maxSamples -> (panSamples, maxSamples)
              | spanSamples > minSamples -> (panSamples, spanSamples)
              | otherwise                -> (0, currentViewSpan)


--------------------------------
-- segments
--------------------------------

type ReferencesUpdater = Segments -> Segments -> References -> References

class SegmentManipulation a where
  getAllSegments :: a -> Segments
  getAudibleSegments :: a -> Segments
  getSegmentsSpanning :: Cursor -> Cursor -> a -> Segments
  getVisibleSegments :: a -> Segments
  insertSegmentsAfter :: Cursor -> Segments -> ReferencesUpdater -> a -> a
  removeSegmentsAfter :: Cursor -> SegmentCount -> ReferencesUpdater -> a -> a
  replaceSegmentsSpanning :: Cursor -> SegmentCount -> Segments -> ReferencesUpdater -> a -> a

instance SegmentManipulation Model where
  getAllSegments = segmentsOf
  getAudibleSegments model =
    case playbackCursor of
      Just cursor -> getExactSegments (Right viewSpan) cursor (segmentsOf model)
      Nothing -> singletonSegments (makeGap viewSpan)
    where
      playbackCursor = model^.playbackView.cursor
      viewSpan = model^.playbackView.span
  getSegmentsSpanning from to model =
    segmentsSpanning from to (segmentsOf model)
  getVisibleSegments model =
    getExactSegments (Left span) cursor (segmentsOf model)
    where
      cursor = model^.viewCursor
      span = model^.viewSpan
  insertSegmentsAfter cursor toInsert updateReferences model =
    model
    { referencesOf = references'
    , segmentsOf = segments'
    }
    where
      segments = segmentsOf model
      segments' = insertAfter cursor toInsert segments
      references' = updateReferences segments segments' (referencesOf model)
  removeSegmentsAfter cursor num updateReferences model =
    replaceSegmentsSpanning cursor' num emptySegments updateReferences model
    where cursor' = fromJust (moveToBeginningOfNextSegment cursor (segmentsOf model))
  replaceSegmentsSpanning from num replacements updateReferences model =
    model
    { referencesOf = references'
    , segmentsOf = segments'
    }
    where
      segments = segmentsOf model
      to = makeCursorSpanning num from segments
      segments' = replaceSegments from to replacements segments
      references' = updateReferences segments segments' (referencesOf model)
