{-# LANGUAGE FunctionalDependencies #-}

module Audio
 ( AudioLoader(..)
 , AudioPlayer(..)
 , module Audio.AudioData
 , module Audio.AudioFile
 , module Audio.AudioSample
 , module Audio.Cursor
 , module Audio.Decibel
 , module Audio.Playback
 , module Audio.Segment
 , module Audio.Segments
 ) where

import Audio.AudioData
import Audio.AudioFile
import Audio.AudioSample
import Audio.Cursor
import Audio.Decibel
import Audio.Playback
import Audio.Segment
import Audio.Segments
import Control.Concurrent.Container


class Monad m => AudioLoader m where
  readAudio :: FilePath -> (Float -> m ()) -> m AudioFile
  saveAudio :: AudioFile -> Segments -> (Float -> m ()) -> m Segments

class (Container m a, PlaybackSource s) => AudioPlayer m a s d | m -> a s d where
  openPlaybackDevice :: a s -> m d
  closePlaybackDevice :: d -> m ()
  startPlaybackDevice :: d -> m ()
  stopPlaybackDevice :: d -> m ()
  togglePlaybackDevice :: d -> m ()
  deviceIsPlaying :: d -> m Bool

