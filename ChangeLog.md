# Changelog for osabe

## Unreleased changes


## 0.10.0 - 2024-02-05

### Summary
Improved architecture, waveform rendering, and UI; removed GMP dependency

### Description
 - Switched to version of GHC that doesn't use GMP
 - Split off UI functionality into FULE library
 - Refactored the applicaiton architecture
 - Improved waveform rendering (to smooth it and make it more efficient)
 - Fixed an issue when using v2.0.20 of SDL -- UI events weren't being pumped
 - Misc code refactorings


## 0.9.1

### Summary
UI widget improvements/fixes

### Description
 - Added clipping to keep time axis from overflowing its bounds when displaying
 - Made the resize bar stay within bounds of its containing panel on resize


## 0.9.0

### Summary
Added 24-bit WAV file support, truncation highlighting, optimized segmentation,
added a toolbar at the top, added high DPI support, and fixed a few bugs

### Description
 - Reorganized WAV module and added 24-bit WAV file support
 - Added highlighting of the span of silence being removed during gap truncation
 - Fixed an issue where lazy exceptions were causing crashes instead of
   alerting the user to a problem e.g. when loading an incompatible WAV file
 - Made playback not start from the marking cursor when doing a precision click
   in the deadspace and refactored the code in the ViewPanel module a little
 - Made the cursor offset type adjust when a zero-length gap was expanded at the
   end of the audio (which was a bug)
 - Redid threshold comparison for clip segementation to prepare for stereo file
   support and ended up optimizing it too
 - Added support for high DPI displays
 - Added a toolbar with undo, redo, and save buttons


## 0.8.4

### Summary
Improved performance

### Description
 - Fixed a memory leak or two (by adding type declarations)
 - Enhanced performance in the segmentation algorithm and clarified it
 - Optimized display of the time axis a little
 - Misc. code clean-up


## 0.8.3

### Summary
Improved performance and UI responsiveness

### Description
Optimized timestamp display code.

Major UI code refactoring, including:
 - Changing how the waveform representation gets to the View
 - Changing the UI input types
 - Changing the UI layout type
 - Adding a layered panel module and changing the ViewPanel and ViewControlPanel to use it
 - Refactoring the split panel module to be more readable
 - Changing how things like mouse button state and position are kept track of
 - Rewriting code to avoid the use of `proc` syntax since it's slow when desugared
 - Misc code cleanup

Made a bunch of data strict -- should probably use the StrictData extension instead.


## 0.8.2

### Summary
Improved performance

### Description
Content in bounded panels had been written to different rendering targets then copied to the main output for display, but this method of doing things was expensive performance-wise. So it was removed and replaced with a hack to keep content from layering the wrong way since I was too lazy to adjust the code (mainly the time axis) to keep it from displaying outside its set bounds.

UI code was refactored to be more approchable -- the types were refactored -- and other code was moved into the appropriate place or shifted around within the UI and View areas. The idle and load screens were refactored in the Presenter to operate more like the editing screen.


## 0.8.1

### Summary
Better Mac install experience

### Description
OS X didn't like opening the application since it's not signed and the libraries it uses aren't signed either. When v0.8.0 was released the installation instructions told the user to use Homebrew to install the required libraries and then permit the application to run in the System Preferences. Since then I got the Ctrl+click method of opening an unsigned application to work; this method works with not only an unsigned application but with the unsigned libraries too, so I switched back to bundling the libraries with the application and instruct the user to use that method when installing.


## 0.8.0

Initial release
