module Args
 ( Args(..)
 , readArgs
 , printUsage
 ) where

import Control.Monad.Except
import System.Console.GetOpt
import System.Environment

import Config


data Args
  = Args
    { versionFlagOf :: Bool -- display version number and client type (mac/pc/linux)
    , helpFlagOf :: Bool
    , configFileOf :: Maybe FilePath
    --
    , argsOf :: [String]
    }
  deriving (Show)

defaultArgs
  = Args
    { versionFlagOf = False
    , helpFlagOf = False
    , configFileOf = Nothing
    --
    , argsOf = []
    }


options :: [OptDescr (Args -> Args)]
options =
 [ Option ['v'] ["version"]
     (NoArg (\args -> args { versionFlagOf = True}))
     "print version information"
 , Option ['h'] ["help"]
     (NoArg (\args -> args { helpFlagOf = True }))
     "print usage information"
 , Option ['c'] ["config"]
     (ReqArg (\c args -> args { configFileOf = Just c }) "FILE")
     "use an alternate settings file"
 ]

readArgs :: (MonadIO m) => ExceptT [String] m Args
readArgs = do
  args <- liftIO getArgs
  -- use RequiredOrder so we can set negative config values
  case getOpt RequireOrder options args of
    (f,p,[]) -> return (foldl (flip id) defaultArgs f) { argsOf = p }
    (_,_,es) -> throwError es

usageHeader = "\n\
\This program lets you edit long-form narration wave files.\n\
\\n\
\You can launch the program without arguments from the command-line and\n\
\drag-and-drop a file into the UI to load it, or you may give the path of\n\
\the file you would like to load as an argument at the command-line.\n\
\\n\
\In addition, the following command-line options are available:\n"

usageFooter = "\
\Note: the '--version' and '--help' flags cannot be used with any other flags.\n\
\\n\
\The program also accepts options to set and retrieve configuration values\n\
\via command-line:\n\
\\n\
\  osabe config list                list the configuration settings\n\
\  osabe config get SETTING         get the value of SETTING\n\
\  osabe config set SETTING VALUE   set SETTING to VALUE\n\
\  osabe config set SETTING         clear SETTING (if applicable)\n"

printUsage :: (MonadIO m) => m ()
printUsage = liftIO $ do
  putStrLn (usageInfo usageHeader options)
  putStrLn usageFooter
