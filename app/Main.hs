{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}

module Main where

import Control.Monad.Except
import Control.Monad.Reader

import App
import Args
import Config
import GlobalContext
import Graphics.Context
import Screens.Editing
import Screens.Instructions
import Screens.Loading
import Screens.Shared.Output


programName = "osabe"

main :: IO ()
main = do
  res <- runExceptT readArgs
  case res of
    Left es -> mapM_ print es
    Right args ->
      if | helpFlagOf args -> printUsage
         | versionFlagOf args -> putStrLn "beta"
         | otherwise -> run args

run args = do
  -- TODO handle IO errors
  configFilePaths <- establishConfigFiles (configFileOf args) Nothing
  -- TODO setup logging and add logging to the rest
  let settingsFilePath = settingsFilePathOf configFilePaths
  case argsOf args of
    ["config", "get", name] -> getSetting name settingsFilePath
    ["config", "list"] -> listSettings
    ["config", "set", name] -> setSetting name "" settingsFilePath
    ["config", "set", name, value] -> setSetting name value settingsFilePath
    ["help"] -> printUsage
    fs -> mainLoop configFilePaths settingsFilePath fs

mainLoop configFilePaths settingsFilePath files = do
  (config, messages) <- readConfigFiles configFilePaths
  graphics <- initializeGraphicsContext (getStyle config) programName
  let context = GlobalContext programName config graphics
  _ <- nextScreen context =<<
    if null files
    then instructionsScreen context messages
    else loadingScreen context messages (head files)
  destroyGraphicsContext graphics

nextScreen context = \case
  FileDropped path -> do
    ret <- loadingScreen context [] path
    nextScreen context ret
  FileLoadErrored{} -> do
    ret <- instructionsScreen context []
    nextScreen context ret
  FileLoaded audio segments waveRep -> do
    ret <- editingScreen context [] audio segments waveRep
    nextScreen context ret
  CloseRequested ->
    return ()

